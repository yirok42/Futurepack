package futurepack.generating.connected;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ModelConnected {
	public final String parent = "block/block";
	
	public List<ModelElement> elements;
	
	public Map<String, String> textures;
	
	public ModelConnected() {
		this.elements = new ArrayList<>();
		this.textures = new HashMap<>();
	}
	
	public void addElement(ModelElement e) {
		this.elements.add(e);
	}
	
	public void addTexture(String name, String texture) {
		this.textures.put(name, "futurepack:blocks/" + texture);
	}
}
