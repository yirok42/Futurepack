package futurepack.api.interfaces.tilentity;

import net.minecraft.core.Direction;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandlerModifiable;

public interface ITileScrollableInventory
{
	public default IItemHandlerModifiable getScrollableInventory()
	{
		return (IItemHandlerModifiable) ((BlockEntity)this).getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, Direction.DOWN).orElseThrow(NullPointerException::new);
	}
}
