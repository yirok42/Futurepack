package futurepack.api.interfaces;

import futurepack.api.ParentCoords;
import net.minecraft.core.BlockPos;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.material.Material;

/**
 * This Interface is Used to Select Big Formations of Blocks (Curently used for the {@link futurepack.common.spaceships.FPSpaceShipSelector})
*/
public interface IBlockSelector
{
	/**
	 * This checks if a Block is walid
	 * @param w is the World
	 * @param j is the x Coord
	 * @param k is the y Coord
	 * @param l is the z Coord
	 * @param m is the Material 
	 * @param dia is the boolean if the Block is Square 
	 * @return if the Block is Valid
	*/
	boolean isValidBlock(Level w,BlockPos pos, Material m, boolean diagonal, ParentCoords parent);
	
	/**
	 * @return if the Block is the End or Not 
	*/
	boolean canContinue(Level w,BlockPos pos, Material m, boolean diagonal, ParentCoords parent);
}
