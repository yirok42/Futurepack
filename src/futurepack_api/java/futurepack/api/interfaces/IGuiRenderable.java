package futurepack.api.interfaces;

import com.mojang.blaze3d.vertex.PoseStack;

/**
 * Used to render the Items/Blocks and Icons in the Research main overview and the entries.
 */
public interface IGuiRenderable
{
	/**
	 * Use GL methods to render.
	 * @param blitOffset TODO
	 */
	public void render(PoseStack matrixStack, int mouseX, int mouseY, int x, int y, int blitOffset);
}
