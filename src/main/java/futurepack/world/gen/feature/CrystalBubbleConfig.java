package futurepack.world.gen.feature;

import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.levelgen.feature.configurations.FeatureConfiguration;

public class CrystalBubbleConfig implements FeatureConfiguration 
{
	public static final Codec<CrystalBubbleConfig> CODEC = RecordCodecBuilder.create((p) -> 
	{
		return p.group(BlockState.CODEC.fieldOf("fillerblock").forGetter((c) -> {
			return c.fillerblock;
		}), BlockState.CODEC.fieldOf("crystalblock").forGetter((c) -> {
			return c.crystalblock;
		})
		).apply(p, CrystalBubbleConfig::new);
	});
	
	
	public final BlockState fillerblock, crystalblock;

	public CrystalBubbleConfig(BlockState fillerblock, BlockState crystalblock)
	{
		super();
		this.fillerblock = fillerblock;
		this.crystalblock = crystalblock;
	}

}
