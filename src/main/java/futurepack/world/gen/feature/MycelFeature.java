package futurepack.world.gen.feature;

import java.util.Arrays;
import java.util.Random;
import java.util.stream.IntStream;

import com.mojang.serialization.Codec;

import net.minecraft.core.BlockPos;
import net.minecraft.world.level.LevelAccessor;
import net.minecraft.world.level.WorldGenLevel;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.levelgen.LegacyRandomSource;
import net.minecraft.world.level.levelgen.LegacyRandomSource;
import net.minecraft.world.level.levelgen.WorldgenRandom;
import net.minecraft.world.level.levelgen.feature.Feature;
import net.minecraft.world.level.levelgen.feature.FeaturePlaceContext;
import net.minecraft.world.level.levelgen.feature.configurations.BlockStateConfiguration;
import net.minecraft.world.level.levelgen.synth.PerlinSimplexNoise;
import net.minecraft.world.level.levelgen.synth.SimplexNoise;


//muss ein map gen feature sein
public class MycelFeature extends Feature<BlockStateConfiguration>
{
	
	public MycelFeature(Codec<BlockStateConfiguration> codec) 
	{
		super(codec);
	}

	protected void generate(WorldGenLevel w, int chunkX, int chunkZ, Random rand, BlockState mycel)
	{
		PerlinSimplexNoise structure = new PerlinSimplexNoise(new WorldgenRandom(new LegacyRandomSource(w.getSeed())), Arrays.asList(3, 3));
		SimplexNoise highMap = new SimplexNoise(new LegacyRandomSource(w.getSeed()-1));
		
		double baseHigh = (20+highMap.getValue(chunkX, chunkZ))/256D;
		double heightVariation = 0.5D/256D;
		double minHeight = 1D/256;
		
//		boolean flag = rand.nextInt(100)==0;
		
		BlockPos.MutableBlockPos pos = new BlockPos.MutableBlockPos();
		
		for(int x=0;x<16;x++)
		{
			for(int z=0;z<16;z++)
			{
				int xx = chunkX*16 + x;
				int zz = chunkZ*16 + z;
				
				double dx = xx * 0.0625;
				double dz = zz * 0.0625;
				
				double val = structure.getValue(dx, dz, true);
				
				if(0.2<val && val<0.8)
				{			
					double height = minHeight + highMap.getValue(dx, dz) * heightVariation;
					double height2 = highMap.getValue(dx+400, dz) * heightVariation*9;
					if(height>0)
					{
						int minY = (int) (255 * (baseHigh-height -height2));
						int maxY = (int) (255 * (baseHigh+height -height2));
						
						minY = Math.max(2, minY);
						maxY = Math.min(254,maxY);
						
						for(int y=minY;y<maxY;y++)
						{
							fillBlock(w, xx, y, zz, pos, mycel);
						}					
					}
					
					if(x==8 && z==8)
					{
						int y = (int) (255 * (baseHigh-height -height2));
						y = Math.max(2, y);
						
						genTower(w, chunkX, y, chunkZ, rand, pos, mycel);
					}
				}
			}
		}
	}
	
	public void genTower(LevelAccessor w, int chunkX, int y, int chunkZ, Random rand, BlockPos.MutableBlockPos pos, BlockState mycel)
	{
		boolean up = rand.nextBoolean();
		int count = 2 +rand.nextInt(5);
		
		SimplexNoise[] noises = new SimplexNoise[count];
		for(int i=0;i<noises.length;i++)
		{
			noises[i] = new SimplexNoise(new LegacyRandomSource(rand.nextLong()));
		}
		
		for(;y>2 && y<254;y+=up?1:-1)
		{
			for(int i=0;i<count;i++)
			{
				double dx = 7 +noises[i].getValue(y, 0) * 2.5;
				double dz = 7 +noises[i].getValue(0, y) * 2.5;
				
				int x = chunkX * 16 + (int)dx;
				int z = chunkZ * 16 + (int) dz;
				
				if(!fillBlockNotAir(w, x, y, z, pos, mycel))
				{
					return;
				}
			}
		}
	}
	/**
	 * 
	 * @param w
	 * @param x 0-15
	 * @param y 0-15
	 * @param z 0-15
	 * @param prime
	 */
	public void fillBlock(LevelAccessor w, int x, int y, int z, BlockPos.MutableBlockPos pos, BlockState mycel)
	{
		//if(prime.getBlockState(x, y, z).getBlock()==Blocks.AIR || prime.getBlockState(x, y, z).getBlock())
		{
			pos.set(x, y, z);
			w.setBlock(pos, mycel, 2);
		}		
	}
	
	public boolean fillBlockNotAir(LevelAccessor w, int x, int y, int z, BlockPos.MutableBlockPos pos, BlockState mycel)
	{
		pos.set(x, y, z);
		if(y <= 60 || !w.isEmptyBlock(pos))
		{
			w.setBlock(pos, mycel, 2);
			return true;
		}		
		return false;
	}

	@Override
	public boolean place(FeaturePlaceContext<BlockStateConfiguration> c) 
	{
		
		generate(c.level(), c.origin().getX()>>4, c.origin().getZ()>>4, c.random(), c.config().state);
		return true;
	}
}
