package futurepack.world.gen.carver;

import java.util.Arrays;
import java.util.Locale;
import java.util.Random;

import it.unimi.dsi.fastutil.doubles.Double2DoubleFunction;

public class CubicSpline implements Double2DoubleFunction
{
	private final double a,b,c,d;
	private final double xa;
	
	public double defined_x_min = -Double.MAX_VALUE;
	public double defined_x_max = Double.MAX_VALUE;
	
	public CubicSpline(double xa, double a, double b, double c, double d)
	{
		super();
		this.xa = xa;
		this.a = a;
		this.b = b;
		this.c = c;
		this.d = d;
	}
	
	public double f(double x)
	{
		if(x<defined_x_min || x>defined_x_max)
			throw new UnsupportedOperationException(String.format("Defnied range is [%s, %s], but x=%s", defined_x_min, defined_x_max, x));
		x -= xa;
		return a*x*x*x + b*x*x + c*x + d;
	}
	
	@Override
	public double get(double key) 
	{
		return f(key);
	}
	
	@Override
	public String toString() 
	{
		return String.format(Locale.ENGLISH, "f="+a+"*(x-"+xa+")^3 + "+b+"*(x-"+xa+")^2 + "+c+"*(x-"+xa+") + "+d+"; %(,.4f < x < %(,.4f", defined_x_min, defined_x_max);
	}
	
	/**
	 * 
	 * @param xa x of Point A
	 * @param ya y of Point A
	 * @param xb x of Point B
	 * @param yb y of Point B
	 * @param ma = f'(A)
	 * @param mb = f'(B)
	 */
	public static CubicSpline fromPoints(double xa, double ya, double xb, double yb, double ma, double mb)
	{
		double a,b,c,d;
		
		d = ya;
		c = ma;
			
		double z = xa-xb;
		a = (ma*z + mb*z - 2*ya + 2*yb) / (z*z*z);
		//b = (-2*ma*z + mb*z - 3*ya + 3*yb) / (z*z);
		
		b = (-3*a*(xb-xa)*(xb-xa)-ma+mb)/(2*(xb-xa));
		
		CubicSpline spline = new CubicSpline(xa, a, b, c, d);
		spline.defined_x_min = xa;
		spline.defined_x_max = xb;
		
		return spline;
	}
	
	/**
	 * 
	 * @param points {{x,y,f'(x)}, ...}
	 * @return
	 */
	public static CubicSpline[] fromPoints(double[][] points)
	{
		if(points.length < 2)
			throw new IllegalArgumentException("neeed atleast 2 points!");
		
		CubicSpline[] splines = new CubicSpline[points.length-1];
		
		for(int i=0;i<splines.length;i++)
		{
			splines[i] = fromPoints(points[i][0], points[i][1], points[i+1][0], points[i+1][1], points[i][2], points[i+1][2]);
		}
		return splines;
	}
	
	/**
	 * 
	 * @param splines must be sorted from lower x to max x;
	 * @return
	 */
	public static Double2DoubleFunction asFunction(final CubicSpline[] splines)
	{
		return new Double2DoubleFunction()
		{	
			@Override
			public double get(double key) 
			{
				int n = splines.length;
				int min=0;
				
				while(n>0)
				{
					int j= n/2;
					CubicSpline c = splines[min+j];
					boolean flagA = key >= c.defined_x_min;
					boolean flagB = key <= c.defined_x_max;
					if(flagA && flagB)
					{
						return c.f(key);
					}
					else if(flagA && !flagB)
					{
						min = min+j+1;
						n = splines.length - min;
					}
					else if(flagB && !flagA)
					{
						n = j - min;
					}
					else
					{
						break;
					}
				}
				
				for(CubicSpline c : splines)
				{
					boolean flagA = key >= c.defined_x_min;
					boolean flagB = key <= c.defined_x_max;
					if(flagA && flagB)
					{
						return c.f(key);
					}
				}
				if(key > splines[splines.length-1].defined_x_max)
					return splines[splines.length-1].f(splines[splines.length-1].defined_x_max);//this happens to often
				if(key < splines[0].defined_x_min)
					return splines[0].f(splines[0].defined_x_min);
				
				return 0;				
			}
		};
	}
	
	public static void main(String[] args) 
	{
		double h = 4;
		double r = 5;
		Random rand = new Random();
		
		double v = rand.nextDouble();
		
		v = 0;
		double m = Math.max(h, r) * 0.2 * (1-v) + v* Math.min(h, r)* 0.8;
		System.out.println(m);
		CubicSpline s1 = fromPoints(0, h, m, m, 0, -1D);
		CubicSpline s2 = fromPoints(m, m, r, 0.0, -1, -5.6 * (v+0.01));//for 0 -1 is to much...;
		System.out.println("plot");
		System.out.println(s1);
		System.out.println(s2);
		
		System.out.println(Arrays.deepToString(fromPoints(new double[][]{{0,h,0},{m,m,-1},{r,0,-5.6 * (v+0.01)}})));
	}

	
}
