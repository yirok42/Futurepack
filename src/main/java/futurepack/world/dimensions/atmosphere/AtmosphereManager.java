package futurepack.world.dimensions.atmosphere;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import futurepack.api.Constants;
import futurepack.api.interfaces.IAirSupply;
import futurepack.api.interfaces.IChunkAtmosphere;
import futurepack.api.interfaces.IPlanet;
import futurepack.common.FPLog;
import futurepack.common.spaceships.FPPlanetRegistry;
import futurepack.common.sync.FPPacketHandler;
import futurepack.common.sync.MessagePlayerAirTanks;
import futurepack.common.sync.NetworkHandler;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.ByteTag;
import net.minecraft.nbt.IntArrayTag;
import net.minecraft.nbt.IntTag;
import net.minecraft.nbt.Tag;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.tags.FluidTags;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.chunk.LevelChunk;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityManager;
import net.minecraftforge.common.capabilities.CapabilityToken;
import net.minecraftforge.common.capabilities.ICapabilityProvider;
import net.minecraftforge.common.util.INBTSerializable;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.event.AttachCapabilitiesEvent;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.event.TickEvent.Phase;
import net.minecraftforge.event.entity.living.LivingEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.LogicalSide;
import net.minecraftforge.network.PacketDistributor;

public class AtmosphereManager implements Runnable
{
	public static final AtmosphereManager INSTANCE = new AtmosphereManager();

	public static final Capability<IAirSupply> cap_AIR = CapabilityManager.get(new CapabilityToken<>(){});
	public static final Capability<IChunkAtmosphere> cap_ATMOSPHERE = CapabilityManager.get(new CapabilityToken<>(){});

	private Thread thread;

	public static void init()
	{

		MinecraftForge.EVENT_BUS.register(INSTANCE);
	}

	private Set<ServerLevel> quee = new HashSet<ServerLevel>();
	private final Object lock = new Object();

	@SubscribeEvent
	public void addAirCapsToEntity(AttachCapabilitiesEvent<Entity> e)
	{
		if(e.getGenericType()==Entity.class)
		{
			if(e.getObject() instanceof LivingEntity)
			{
				e.addCapability(new ResourceLocation(Constants.MOD_ID, "air"), new AirProvider());
			}
		}
	}

	@SubscribeEvent
	public void addAirCapsToChunk(AttachCapabilitiesEvent<LevelChunk> e)
	{
		if(e.getGenericType()==LevelChunk.class)
		{
			LevelChunk c = e.getObject();
			if(c.getLevel() == null)
			{
				return; //most likely optifine in init
			}
			else if(!c.getLevel().isClientSide && !hasWorldOxygen(c.getLevel()))
			{
				e.addCapability(new ResourceLocation(Constants.MOD_ID, "atmosphere"), new AtmosphereProvider(c.getMinBuildHeight(), c.getMaxBuildHeight()));
			}

		}
	}

	private static class AirProvider implements ICapabilityProvider, INBTSerializable<IntTag>
	{
		LazyOptional<IAirSupply> air = LazyOptional.of(CapabilityAirSupply::new);

		@Override
		public <T> LazyOptional<T> getCapability(Capability<T> capability, Direction facing)
		{
			if(capability==cap_AIR)
			{
				return air.cast();
			}
			return LazyOptional.empty();
		}

		@Override
		public IntTag serializeNBT()
		{
			return IntTag.valueOf(air.orElse(null).getAir());
		}

		@Override
		public void deserializeNBT(IntTag nbt)
		{
			int air = nbt.getAsInt();
			IAirSupply iair = this.air.orElse(null);
			iair.addAir(air-iair.getAir());
		}

	}

	private static class AtmosphereProvider implements ICapabilityProvider, INBTSerializable<Tag>
	{
		LazyOptional<CapabilityAtmosphere> cap;

		public AtmosphereProvider(int minBuildHeight, int maxBuildHeight)
		{
			cap = LazyOptional.of(() -> new CapabilityAtmosphere(minBuildHeight, maxBuildHeight));
		}

		@Override
		public Tag serializeNBT()
		{
			return writeNBT(cap.orElseThrow(NullPointerException::new), null);
		}

		@Override
		public void deserializeNBT(Tag nbt)
		{
			readNBT(cap.orElseThrow(NullPointerException::new), null, nbt);
		}

		@Override
		public <T> LazyOptional<T> getCapability(Capability<T> capability, Direction facing)
		{
			if(capability == cap_ATMOSPHERE)
			{
				return cap.cast();
			}
			return LazyOptional.empty();
		}

		public Tag writeNBT(IChunkAtmosphere instance, Direction side)
		{
			int[] air = new int[16 * 16 * 256];
			boolean zero = true;
			for(int x=0;x<16;x++)
				for(int z=0;z<16;z++)
					for(int y=0;y<256;y++)
					{
						air[x<<12|z<<8|y] = instance.getAirAt(x, y, z);
						if(air[x<<12|z<<8|y] != 0)
							zero = false;
					}
			if(zero)
			{
				return ByteTag.valueOf((byte)10);
			}

			return new IntArrayTag(air);
		}

		public void readNBT(IChunkAtmosphere instance, Direction side, Tag nbt)
		{
			if(nbt instanceof IntArrayTag)
			{
				int[] air = ((IntArrayTag)nbt).getAsIntArray();
				for(int x=0;x<16;x++)
					for(int z=0;z<16;z++)
						for(int y=0;y<256;y++)
							instance.setAir(x, y, z, air[x<<12|z<<8|y]);
			}
			else if(nbt instanceof ByteTag)
			{
				if(((ByteTag) nbt).getAsByte() == 10)
				{
					for(int x=0;x<16;x++)
						for(int z=0;z<16;z++)
							for(int y=0;y<256;y++)
								instance.setAir(x, y, z, 0);
				}
			}
		}

	}

	@SubscribeEvent
	public void onEntityTick(LivingEvent.LivingUpdateEvent event)
	{
		if(!hasEntityOxygen(event.getEntityLiving()))
		{
			event.getEntity().getCapability(cap_AIR, null).ifPresent(air ->
			{
				air.reduceAir(1);

				Level w = event.getEntity().getCommandSenderWorld();
				if(!w.isClientSide)
				{
					double d0 = event.getEntity().getY() + event.getEntity().getEyeHeight();
		            BlockPos blockpos = new BlockPos(event.getEntity().getX(), d0, event.getEntity().getZ());
		            LevelChunk c = w.getChunkAt(blockpos);
		            c.getCapability(cap_ATMOSPHERE, null).ifPresent( atm ->
		            {
		            	int d = air.getMaxAir() - air.getAir();
		            	int added = atm.removeAir(blockpos.getX()&15, blockpos.getY()&255, blockpos.getZ()&15, d);
		            	air.addAir(added);
		            });
				}

				if(air.getAir()<=-20)
				{
					event.getEntityLiving().hurt(DamageSource.DROWN, 2F);
					air.addAir(20);
				}
				if(air.getAir()%20 == 0 && event.getEntityLiving() instanceof ServerPlayer)
				{
					ServerPlayer mp = (ServerPlayer) event.getEntityLiving();
					if(mp==null)
						throw new NullPointerException("Player is null");
					NetworkHandler.sendPlayerAir(mp);
				}
				event.getEntityLiving().setAirSupply(air.getAir());
			});
		}
	}

	public static boolean hasWorldOxygen(Level w)
	{
		IPlanet planet = FPPlanetRegistry.instance.getPlanetSafe(w);
		return planet.hasBreathableAtmosphere();
	}

	public static boolean hasEntityOxygen(Entity e)
	{
		return hasWorldOxygen(e.getCommandSenderWorld());
	}

	public int getAir(Entity e)
	{
		if(hasEntityOxygen(e))
		{
			return e.getAirSupply();
		}
		else
		{
			IAirSupply supp = e.getCapability(cap_AIR, null).orElseThrow(NullPointerException::new);
			return supp.getAir();
		}
	}

	@SubscribeEvent
	public void onWorldTick(TickEvent.WorldTickEvent event)
	{
		if(event.side == LogicalSide.SERVER && event.phase == Phase.END)
		{
			if(!hasWorldOxygen(event.world))
			{
				ServerLevel serv = (ServerLevel) event.world;
				addWorldToUpdateQuee(serv);
			}
		}
	}

	private void addWorldToUpdateQuee(ServerLevel s)
	{
		try
		{
			synchronized (lock)
			{
				quee.add(s);
			}

			if(thread==null || !thread.isAlive())
			{
				thread = new Thread(this, "FP-Atmosphere");
				thread.setDaemon(true);
				thread.start();
			}
		}
		catch(NullPointerException e)
		{
			//see github issue #506
			//it can crash inside the HashSet
			//I think this is a synchronized issue, as this also happened with the ArrayList
			FPLog.logger.warn("NPE in AtmoshphereManager#addWorldToUpdateQuee");
			FPLog.logger.catching(e);
		}
	}

	@Override
	public void run()
	{
		int empty = 0;

		while(empty < 1200)
		{
			if(quee.isEmpty())
			{
				empty++;
				try
				{
					Thread.sleep(50);
				}
				catch (InterruptedException e)
				{
					e.printStackTrace();
				}
			}
			else
			{
				empty=0;
				Collection<ServerLevel> list = null;
				synchronized (lock)
				{
					list = quee;
					quee = new HashSet<>(list.size());
				}
				list.forEach(CapabilityAtmosphere::updateAthmosphere);
				list.clear();
			}
		}
	}

	public static IAirSupply getAirSupplyFromEntity(final LivingEntity base)
	{
		if(hasEntityOxygen(base))
		{
			return new IAirSupply()
			{
				@Override
				public void reduceAir(int amount)
				{
					if (base.isEyeInFluid(FluidTags.WATER))
					{
						addAir(-amount);
					}
				}

				@Override
				public int getMaxAir()
				{
					return 300;
				}

				@Override
				public int getAir()
				{
					return base.getAirSupply();
				}

				@Override
				public void addAir(int amount)
				{
					base.setAirSupply(getAir() + amount);
				}
			};
		}
		else
		{
			final IAirSupply supp = base.getCapability(cap_AIR, null).orElse(null);
			if(supp!=null)
			{
				return new IAirSupply()
				{

					@Override
					public void reduceAir(int amount)
					{
						Level w = base.getCommandSenderWorld();
						double d0 = base.getY() + base.getEyeHeight();
			            BlockPos pos = new BlockPos(base.getX(), d0, base.getZ());
			            LazyOptional<IChunkAtmosphere> c = w.getChunkAt(pos).getCapability(cap_ATMOSPHERE);
			            if(c.isPresent())
			            {
			            	IChunkAtmosphere atm = c.orElseThrow(NullPointerException::new);
			            	amount -= atm.removeAir(pos.getX()&15, pos.getY(), pos.getZ()&15, amount);
			            }
			            if(amount > 0)
			            {
			            	supp.reduceAir(amount);
			            }
					}

					@Override
					public int getMaxAir()
					{
						return supp.getMaxAir();
					}

					@Override
					public int getAir()
					{
						return supp.getAir();
					}

					@Override
					public void addAir(int amount)
					{
						supp.addAir(amount);
					}
				};
			}
			return null;
		}
	}

	public static void setAirTanks(float full, Player player)
	{
		if(!player.getCommandSenderWorld().isClientSide())
		{
			FPPacketHandler.CHANNEL_FUTUREPACK.send(PacketDistributor.PLAYER.with(() -> (ServerPlayer)player), new MessagePlayerAirTanks(full));
		}
	}
}
