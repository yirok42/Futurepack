package futurepack.common;

import futurepack.api.Constants;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.tags.BlockTags;
import net.minecraft.tags.FluidTags;
import net.minecraft.tags.ItemTags;
import net.minecraft.tags.TagKey;
import net.minecraft.world.item.Item;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.material.Fluid;

public class FuturepackTags
{

	public static final TagKey<Block> force_field = bockWrapper(new ResourceLocation(Constants.MOD_ID, "dungeon/force_field"));
	public static final TagKey<Block> entity_laser = bockWrapper(new ResourceLocation(Constants.MOD_ID, "dungeon/entity_laser"));
	public static final TagKey<Block> wardrobe = bockWrapper(new ResourceLocation(Constants.MOD_ID, "wardrobe"));
	public static final TagKey<Block> METAL_FENCE = bockWrapper(new ResourceLocation(Constants.MOD_ID, "metal_fence"));
	public static final TagKey<Block> METAL_BLOCK = bockWrapper(new ResourceLocation(Constants.MOD_ID, "metal_block"));
	public static final TagKey<Block> quantanium_connecting = bockWrapper(new ResourceLocation(Constants.MOD_ID, "quantanium_connecting"));
	public static final TagKey<Block> tag_crystal_ground_neon = bockWrapper(new ResourceLocation(Constants.MOD_ID, "crystal_ground/neon"));//done
	public static final TagKey<Block> tag_crystal_ground_alutin = bockWrapper(new ResourceLocation(Constants.MOD_ID, "crystal_ground/alutin"));//done
	public static final TagKey<Block> tag_crystal_ground_retium = bockWrapper(new ResourceLocation(Constants.MOD_ID, "crystal_ground/retium"));//done
	public static final TagKey<Block> tag_crystal_ground_glowtite = bockWrapper(new ResourceLocation(Constants.MOD_ID, "crystal_ground/glowtite"));//done
	public static final TagKey<Block> tag_crystal_ground_bioterium = bockWrapper(new ResourceLocation(Constants.MOD_ID, "crystal_ground/bioterium"));//Done
	public static final TagKey<Block> SAPLING_HOLDER = bockWrapper(new ResourceLocation(Constants.MOD_ID, "sapling_holder"));
	public static final TagKey<Block> not_miner_breakable = bockWrapper(new ResourceLocation(Constants.MOD_ID, "not_miner_breakable"));
	public static final TagKey<Block> MYCEL = bockWrapper(new ResourceLocation(Constants.MOD_ID, "mycel"));
	public static final TagKey<Block> thruster = bockWrapper(new ResourceLocation(Constants.MOD_ID, "spaceship/thrusters"));
	public static final TagKey<Block> teleporter = bockWrapper(new ResourceLocation(Constants.MOD_ID, "spaceship/teleporter"));
	public static final TagKey<Block> neon_producer = bockWrapper(new ResourceLocation(Constants.MOD_ID, "spaceship/neon_producer"));
	public static final TagKey<Block> ORE_IRON = bockWrapper(new ResourceLocation("forge:ores/iron"));
	public static final TagKey<Block> ORE_MAGNETITE = bockWrapper(new ResourceLocation("forge:ores/magnetite"));
	public static final TagKey<Block> block_crystals = bockWrapper(new ResourceLocation(Constants.MOD_ID, "crystals"));
	public static final TagKey<Block> neon_sand = bockWrapper(new ResourceLocation(Constants.MOD_ID, "neonsand"));
	public static final TagKey<Block> plasmatank_wall = bockWrapper(new ResourceLocation(Constants.MOD_ID, "plasmatank_wall")); //TODO
	public static final TagKey<Block> stone_menelaus = bockWrapper(new ResourceLocation(Constants.MOD_ID, "stone_menelaus"));
	public static final TagKey<Block> erse_spawn_able = bockWrapper(new ResourceLocation(Constants.MOD_ID, "erse_spawn_able"));
	public static final TagKey<Block> radioactive_light = bockWrapper(new ResourceLocation(Constants.MOD_ID, "radioactive_light"));
	public static final TagKey<Block> BLOCK_ORES = bockWrapper(new ResourceLocation("forge:ores"));

	public static final TagKey<Block> TOOLTYPE_AXE = bockWrapper(new ResourceLocation("minecraft:mineable/axe"));
	public static final TagKey<Block> TOOLTYPE_PICKAXE = bockWrapper(new ResourceLocation("minecraft:mineable/pickaxe"));
	public static final TagKey<Block> TOOLTYPE_SHOVEL = bockWrapper(new ResourceLocation("minecraft:mineable/shovel"));
	public static final TagKey<Block> TOOLTYPE_HOE = bockWrapper(new ResourceLocation("minecraft:mineable/hoe"));
	public static final TagKey<Block> LEVEL_WOOD = bockWrapper(new ResourceLocation("forge:needs_wood_tool"));
	public static final TagKey<Block> LEVEL_GOLD = bockWrapper(new ResourceLocation("forge:needs_gold_tool"));
	public static final TagKey<Block> LEVEL_STONE = bockWrapper(new ResourceLocation("minecraft:needs_stone_tool"));
	public static final TagKey<Block> LEVEL_IRON = bockWrapper(new ResourceLocation("minecraft:needs_iron_tool"));
	public static final TagKey<Block> LEVEL_DIAMOND = bockWrapper(new ResourceLocation("minecraft:needs_diamond_tool"));
	public static final TagKey<Block> LEVEL_NETHERITE = bockWrapper(new ResourceLocation("forge:needs_netherite_tool"));

	public static final TagKey<Item> MAGNETIC = itemWrapper(new ResourceLocation(Constants.MOD_ID, "magnetic"));
	public static final TagKey<Item> MAGNET = itemWrapper(new ResourceLocation(Constants.MOD_ID, "magnet"));
	public static final TagKey<Item> item_crystals = itemWrapper(new ResourceLocation(Constants.MOD_ID, "crystals"));
	public static final TagKey<Item> gemDiamond = itemWrapper(new ResourceLocation("forge:gems/diamond"));
	public static final TagKey<Item> gemQuartz = itemWrapper(new ResourceLocation("forge:gems/quartz"));
	public static final TagKey<Item> ingotIron = itemWrapper(new ResourceLocation("forge:ingots/iron"));
	public static final TagKey<Item> ingotNeon = itemWrapper(new ResourceLocation("forge:ingots/neon"));
	public static final TagKey<Item> ingotCopper = itemWrapper(new ResourceLocation("forge:ingots/copper"));
	public static final TagKey<Item> ingotGold = itemWrapper(new ResourceLocation("forge:ingots/gold"));
	public static final TagKey<Item> COMPOST = itemWrapper(new ResourceLocation(Constants.MOD_ID, "compost"));//done
	public static final TagKey<Item> INGOTS = itemWrapper(new ResourceLocation("forge:ingots"));
	public static final TagKey<Item> GEMS = itemWrapper(new ResourceLocation("forge:gems"));
	public static final TagKey<Item> ORES = itemWrapper(new ResourceLocation("forge:ores"));
	public static final TagKey<Item> RAW_MATERIALS = itemWrapper(new ResourceLocation("forge:raw_materials"));
	public static final TagKey<Item> UNCOLOR = itemWrapper(new ResourceLocation(Constants.MOD_ID, "uncolor"));
	public static final TagKey<Item> MINING_MINDCONTROL = itemWrapper(new ResourceLocation(Constants.MOD_ID, "mining_mindcontrol"));
	public static final TagKey<Item> BLACKLIST_CRUSHER = itemWrapper(new ResourceLocation(Constants.MOD_ID, "blacklist/crusher"));


	public static final TagKey<Fluid> PARALYZING = fluidWrapper(new ResourceLocation(Constants.MOD_ID, "paralyzing"));
	public static final TagKey<Fluid> OXYGEN = fluidWrapper(new ResourceLocation(Constants.MOD_ID, "oxygen"));


	public static TagKey<Block> bockWrapper(ResourceLocation res)
	{
		return BlockTags.create(res);
	}

	public static TagKey<Item> itemWrapper(ResourceLocation res)
	{
		return ItemTags.create(res);
	}

	public static TagKey<Fluid> fluidWrapper(ResourceLocation res)
	{
		return FluidTags.create(res);
	}
}
