package futurepack.common.gui;

import java.lang.reflect.Field;

import futurepack.common.block.inventory.TileEntityTechtable;
import net.minecraft.core.NonNullList;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.CraftingContainer;
import net.minecraft.world.inventory.RecipeHolder;
import net.minecraft.world.inventory.ResultSlot;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.crafting.RecipeType;
import net.minecraftforge.event.ForgeEventFactory;

public class SlotTechtableCrafting extends ResultSlot
{
	private Player thePlayer;
	private CraftingContainer craftMatrix;
	private TileEntityTechtable tech;
	
	public SlotTechtableCrafting(Player player, CraftingContainer craftingInventory, TileEntityTechtable tech, int slotIndex, int xPosition, int yPosition)
	{
		super(player, craftingInventory, tech, slotIndex, xPosition, yPosition);
		thePlayer = player;
		craftMatrix = craftingInventory;
		this.tech = tech;
	}

	
	@Override
	public void onTake(Player playerIn, ItemStack stack)
	{
		this.checkTakeAchievements(stack);
		net.minecraftforge.common.ForgeHooks.setCraftingPlayer(playerIn);
		NonNullList<ItemStack> aitemstack = thePlayer.level.getRecipeManager().getRemainingItemsFor(RecipeType.CRAFTING, this.craftMatrix, thePlayer.level);
		net.minecraftforge.common.ForgeHooks.setCraftingPlayer(null);
	
		for (int i = 0; i < aitemstack.size(); ++i)
		{
			ItemStack itemstack1 = this.craftMatrix.getItem(i);
			ItemStack itemstack2 = aitemstack.get(i);
			
			if (!itemstack1.isEmpty())
			{
				boolean b = false;
				for(int j=10;j<tech.getContainerSize();j++)
				{
					ItemStack st1 = tech.getItem(j);
					if(!st1.isEmpty())
					{
						if(st1.sameItem(itemstack1) && ItemStack.tagMatches(st1, itemstack1))
						{
							tech.removeItem(j, 1);
							b = true;
							break;
						}
					}
				}
				this.craftMatrix.setItem(i, itemstack1);
				if(!b)
				{
					this.craftMatrix.removeItem(i, 1);
				}
			}
	
			if (!itemstack2.isEmpty()) 
			{
				if (itemstack1.isEmpty()) 
				{
					this.craftMatrix.setItem(i, itemstack2);
				} 
				else if (ItemStack.isSame(itemstack1, itemstack2) && ItemStack.tagMatches(itemstack1, itemstack2)) 
				{
					itemstack2.grow(itemstack1.getCount());
					this.craftMatrix.setItem(i, itemstack2);
				}
				else if (!thePlayer.getInventory().add(itemstack2)) 
				{
					thePlayer.drop(itemstack2, false);
				}
			}
		}
//		return stack;
	}
	
	@Override
	protected void checkTakeAchievements(ItemStack stack)
	{
		if (this.getAmount() > 0)
		{
			stack.onCraftedBy(this.thePlayer.level, this.thePlayer, this.getAmount());
			ForgeEventFactory.firePlayerCraftingEvent(this.thePlayer, stack, craftMatrix);//EXACT COPY from vanilla (super), but with craftMatrix instead of this.inventory
		}

		setAmount(0);
		((RecipeHolder)this.container).awardUsedRecipes(thePlayer);
	}
	
	private final static Field f_amound;
	static
	{
		Class<ResultSlot> cls = ResultSlot.class;
		
		f_amound = cls.getDeclaredFields()[2];
		f_amound.setAccessible(true);
	}
	
	private int getAmount()
	{
		try {
			return (int) f_amound.get(this);
		} catch (IllegalArgumentException | IllegalAccessException e) {
			e.printStackTrace();
		}
		return 0;
	}
	
	private void setAmount(int amount)
	{
		try {
			f_amound.set(this, amount);
		} catch (IllegalArgumentException | IllegalAccessException e) {
			e.printStackTrace();
		}
	}
}
