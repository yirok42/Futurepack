package futurepack.common.gui.inventory;

import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.PoseStack;

import futurepack.common.block.inventory.TileEntityForscher;
import futurepack.depend.api.helper.HelperContainerSync;
import futurepack.depend.api.helper.HelperRendering;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.Container;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.Slot;
import net.minecraft.world.item.ItemStack;

public class GuiResearchReward extends ActuallyUseableContainerScreen<GuiResearchReward.ContainerResearchReward> 
{
	private static final ResourceLocation CHEST_GUI_TEXTURE = new ResourceLocation("textures/gui/container/generic_54.png");
	
	public GuiResearchReward(Player pl, TileEntityForscher.RewardInventory ri)
	{
		
		super(new ContainerResearchReward(pl.getInventory(), ri), pl.getInventory(), "Rewards");

		imageHeight = 97 + 17 + 8 * 18;

	}
	
	@Override
	protected void renderBg(PoseStack matrixStack, float partialTicks, int mouseX, int mouseY) {
		HelperRendering.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		
		RenderSystem.setShaderTexture(0, CHEST_GUI_TEXTURE);
		
		int x = (this.width - this.imageWidth) / 2;
		int y = (this.height - this.imageHeight) / 2;
		
		int invY = 0;

		this.blit(matrixStack, x, y, 0, 0, this.imageWidth, 6 * 18 + 17);
		this.blit(matrixStack, x, y + 6*18+17, 0, 17, this.imageWidth, (8 - 6) * 18);

		invY = 8 * 18 + 17;
			
		this.blit(matrixStack, x, y + invY, 0, 125, this.imageWidth, 97);
		
	}
	
	
	public static class ContainerResearchReward extends ActuallyUseableContainer 
	{

		TileEntityForscher.RewardInventory inv;
		
		public ContainerResearchReward(Inventory pl, TileEntityForscher.RewardInventory ri)
		{
			super();
			
			inv = ri;
			
			int x, y;
			x = (178 - 18 * 9) / 2; 
			y = 18;

			for (int l = 0; l < 8; ++l)
	        {
	            for (int i1 = 0; i1 < 9; ++i1)
	            {
	            	int id = i1 + l * 9;
	                this.addSlot(new SlotTakeOnly(ri, id, x + i1 * 18, y + l * 18));
	            }
	        }
			
			y += 8 * 18;
			
			y+=14;
			
			HelperContainerSync.addInventorySlots(8, y, pl, this::addSlot);
			
		}

		@Override
		public boolean stillValid(Player playerIn) 
		{
			return true;
		}

		
		public static class SlotTakeOnly extends Slot 
		{

			public SlotTakeOnly(Container inventoryIn, int index, int xPosition, int yPosition) 
			{
				super(inventoryIn, index, xPosition, yPosition);
			}

			public boolean mayPlace(ItemStack stack) 
			{
				return false;
			}

		}
		
		@Override
		public void removed(Player playerIn) 
		{
			if(!playerIn.getCommandSenderWorld().isClientSide)
				inv.compress();
			
			super.removed(playerIn);
		}
		
		@Override
		public ItemStack quickMoveStack(Player pl, int sl)
		{
			if(!pl.level.isClientSide)
			{
				Slot slot= getSlot(sl);
				if(!slot.hasItem())		
					return ItemStack.EMPTY;
				
				if(slot.container == pl.getInventory())
				{
					//this.mergeItemStack(slot.getStack(), 0, slots, false);
				}
				else
				{
					this.moveItemStackTo(slot.getItem(), 9*8, this.slots.size(), false);
				}
			
				if(slot.getItem().getCount()<=0)
				{
					slot.set(ItemStack.EMPTY);
				}
			}
			this.broadcastChanges();
			return ItemStack.EMPTY;
		}

	}



	

}
