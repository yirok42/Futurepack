package futurepack.common.gui.inventory;

import com.mojang.blaze3d.platform.GlStateManager;
import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.PoseStack;

import futurepack.api.Constants;
import futurepack.common.block.inventory.TileEntityWaterCooler;
import futurepack.common.gui.ContainerSyncBase;
import futurepack.depend.api.helper.HelperGui;
import futurepack.depend.api.helper.HelperRendering;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.Slot;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.items.SlotItemHandler;

public class GuiWaterCooler extends ActuallyUseableContainerScreen<GuiWaterCooler.ContainerWaterCooler>
{
	private TileEntityWaterCooler tile;
	//private EntityPlayer pl;
	
	
	private ResourceLocation res = new ResourceLocation(Constants.MOD_ID,"textures/gui/water_cooler.png");
	
	public GuiWaterCooler(Player pl, TileEntityWaterCooler tile)
	{
		super(new ContainerWaterCooler(pl.getInventory(), tile), pl.getInventory(), "gui.water_cooler");
		this.tile = tile;
		imageHeight+=20;
	}
    
	@Override
	public void render(PoseStack matrixStack, int mouseX, int mouseY, float partialTicks)
    {
        this.renderBackground(matrixStack);
        super.render(matrixStack, mouseX, mouseY, partialTicks);
        this.renderTooltip(matrixStack, mouseX, mouseY);
        
		int k = (this.width - this.imageWidth) / 2;
		int l = (this.height - this.imageHeight) / 2;
        
		if(!tile.getWater().isEmpty())
		{
			HelperGui.renderFluidTankTooltip(matrixStack, k+44, l+28, 16, 52, tile.getWater(), mouseX, mouseY);
		}
		if(!tile.getSteam().isEmpty())
		{
			HelperGui.renderFluidTankTooltip(matrixStack, k+116, l+28, 16, 52, tile.getSteam(), mouseX, mouseY);
		}
    }
	
	@Override
	protected void renderLabels(PoseStack matrixStack, int p_146979_1_, int p_146979_2_)
	{
		//this.font.drawString(matrixStack, I18n.format("container.watercooler", new Object[0]), 20, 6, 4210752);
		//this.font.drawString(matrixStack, I18n.format("container.inventory", new Object[0]), 20, this.ySize - 96 + 2, 4210752);
	}
	
    
	
    
	@Override
	protected void renderBg(PoseStack matrixStack, float var1, int mouseX, int mouseY) 
	{
		HelperRendering.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		RenderSystem.setShaderTexture(0, res);
		int k = (this.width - this.imageWidth) / 2;
		int l = (this.height - this.imageHeight) / 2;
		this.blit(matrixStack, k, l, 0, 0, this.imageWidth, this.imageHeight);
		
		if(!tile.getWater().isEmpty())
		{
			HelperGui.renderFluidTank(k+44, l+28, 16, 52, tile.getWater(), mouseX, mouseY, 10);
		}
		if(!tile.getSteam().isEmpty())
		{
			HelperGui.renderFluidTank(k+116, l+28, 16, 52, tile.getSteam(), mouseX, mouseY, 10);
		}
		
		RenderSystem.setShaderTexture(0, res);
		GlStateManager._enableBlend();
		this.setBlitOffset(20);
		this.blit(matrixStack, leftPos+43, topPos+27, 176, 0, 18, 54);
		this.blit(matrixStack, leftPos+115, topPos+27, 176, 0, 18, 54);
		this.setBlitOffset(0);
	}
	
	public static class ContainerWaterCooler extends ContainerSyncBase
	{
		private TileEntityWaterCooler tile;
		private SlotItemHandler slotI;
		
		public ContainerWaterCooler(Inventory pl, TileEntityWaterCooler tile)
		{
			super(tile, tile.getLevel().isClientSide());
			this.tile = tile;
			
			slotI = new SlotItemHandler(tile.getGui(), 0, 44, 6);
			this.addSlot(slotI);
			
			for (int l = 0; l < 3; ++l)
			{
				for (int i1 = 0; i1 < 9; ++i1)
				{
					this.addSlot(new Slot(pl, i1 + l * 9 + 9, 8 + i1 * 18, 104 + l * 18));
				}
			}
			
			for (int l = 0; l < 9; ++l)
			{
				this.addSlot(new Slot(pl, l, 8 + l * 18, 162));
			}
		}
		
		@Override
		public ItemStack quickMoveStack(Player pl, int par2)
		{
			Slot slot = this.getSlot(par2);
			if(!pl.level.isClientSide && slot.hasItem())
			{
				if(slot.container == pl.getInventory())
				{
					if(slotI.mayPlace(slot.getItem()))
					{
						this.moveItemStackTo(slot.getItem(), 0, 1, false);
					}
					else
					{
						this.moveItemStackTo(slot.getItem(), 1, 4, false);
					}					
				}
				else
				{
					this.moveItemStackTo(slot.getItem(), 8, this.slots.size(), false);				
				}
				
				if(slot.getItem().getCount()<=0)
				{
					slot.set(ItemStack.EMPTY);
				}
			}
			this.broadcastChanges();
			return ItemStack.EMPTY;
		}
		
		@Override
		public boolean stillValid(Player var1)
		{
			return true;
		}
	}
}
