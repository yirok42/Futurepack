package futurepack.common.gui;

import com.mojang.blaze3d.platform.GlStateManager;
import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.PoseStack;

import futurepack.api.Constants;
import futurepack.api.interfaces.IGuiRenderable;
import futurepack.common.block.inventory.TileEntityScannerBlock;
import futurepack.depend.api.EnumAspects;
import futurepack.depend.api.helper.HelperRendering;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.components.AbstractButton;
import net.minecraft.client.gui.narration.NarrationElementOutput;
import net.minecraft.network.chat.TextComponent;
import net.minecraft.resources.ResourceLocation;

public class ButtonAspect extends AbstractButton
{
	private IGuiRenderable aspect;
	public boolean activated;
	protected static final ResourceLocation buttonTextures = new ResourceLocation(Constants.MOD_ID, "textures/gui/energie_bar.png");
	private TileEntityScannerBlock tile;
	
	public Runnable click;
	
	public ButtonAspect(int buttonId, int x, int y, EnumAspects aspect, TileEntityScannerBlock tile)
	{
		super(x, y, 18, 18, new TextComponent(""));
		this.aspect = new RenderableAspect(aspect);
		this.tile = tile;
	}
	
	public ButtonAspect(int buttonId, int x, int y, IGuiRenderable aspect, TileEntityScannerBlock tile)
	{
		super(x, y, 18, 18, new TextComponent(""));
		this.aspect = aspect;
		this.tile = tile;
	}

	public EnumAspects getAspect()
	{
		return ((RenderableAspect)aspect).icon;
	}
	
	protected int getHoverState(boolean mouseOver)
    {
		int b0 = activated ? 2 : 0;	
		if(mouseOver)
		{
			b0++;
		}
		return b0;
    }
	
	@Override
	public void render(PoseStack matrixStack, int mouseX, int mouseY, float partialTicks)
    {
		if (this.visible)
		{
			Minecraft mc = Minecraft.getInstance();
			RenderSystem.setShaderTexture(0, buttonTextures);
			HelperRendering.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
			this.isHovered = mouseX >= this.x && mouseY >= this.y && mouseX < this.x + this.width && mouseY < this.y + this.height;
			int k = this.getHoverState(this.isHovered);
			GlStateManager._enableBlend();
			GlStateManager._blendFuncSeparate(770, 771, 1, 0);
			GlStateManager._blendFunc(770, 771);
			this.blit(matrixStack, this.x, this.y, 0 + 18 * k, 72, 18, 18);
			
			aspect.render(matrixStack, mouseX, mouseY, this.x+1, this.y+1, 0);
		}
    }
	
	@Override
	public boolean mouseReleased(double mouseX, double mouseY, int button) 
	{
		return super.mouseReleased(mouseX, mouseY, button);
	}
	
	public void click()
	{
		activated = !activated;
		if(click!=null)
		{
			click.run();
		}
	}

	@Override
	public void onPress() 
	{
		click();
		if(tile!=null)
			tile.setAspects(getAspect(), activated);
	}

	@Override
	public void updateNarration(NarrationElementOutput pNarrationElementOutput)
	{
		
	}
}
