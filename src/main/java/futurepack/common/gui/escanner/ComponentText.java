package futurepack.common.gui.escanner;

import java.util.List;

import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.PoseStack;

import futurepack.common.FPConfig;
import futurepack.depend.api.helper.HelperComponent;
import futurepack.depend.api.interfaces.IGuiComponent;
import net.minecraft.client.gui.Font;
import net.minecraft.client.gui.screens.Screen;
import net.minecraft.network.chat.MutableComponent;
import net.minecraft.network.chat.Style;
import net.minecraft.util.FormattedCharSequence;

public class ComponentText implements IGuiComponent
{
	private int ah;
	
	protected int height;

	protected int width;
	
	protected MutableComponent rawText;
	protected List<FormattedCharSequence> parts;
	
	protected Font font;
	
	public ComponentText(MutableComponent text)
	{
		rawText = text;
		if(rawText instanceof MutableComponent)
		{
			Style s = text.getStyle();
			if(s==null)
			{
				s = Style.EMPTY;
			}
			if(FPConfig.CLIENT.allowUnicodeFont.get())
				s = s.withFont(HelperComponent.getUnicodeFont());
			
			((MutableComponent) rawText).setStyle(s);
		}
		
	}
	
	@Override
	public void init(int maxWidth, Screen gui)
	{
		width = maxWidth;
		font = gui.getMinecraft().font;
		parts = font.split(rawText, width);
		height = font.lineHeight * parts.size();
	}

	@Override
	public int getAdditionHeight()
	{
		return ah;
	}

	@Override
	public int getWidth()
	{
		return width;
	}

	@Override
	public int getHeight()
	{
		return height;
	}

	@Override
	public void render(PoseStack matrixStack, int x, int y, int blitOffset, int mouseX, int mouseY, GuiScannerBase gui)
	{
		RenderSystem.setShaderColor(1f, 1f, 1f, 1f);

		int start = 0;
		if(y<0)
		{
			start = (-y) / font.lineHeight;
		}
		for(int i=start;i<parts.size();i++)
		{
			int yy = y + i*font.lineHeight;	
			if(yy > gui.height)
				break;
			font.draw(matrixStack, parts.get(i) , x, yy, 0x000000);	
		}	
	}
	
	@Override
	public void setAdditionHeight(int additionHight)
	{
		ah = additionHight;
	}

	@Override
	public void postRendering(PoseStack matrixStack, int x, int y, int blitOffset, int mouseX, int mouseY, boolean hover, GuiScannerBase gui)
	{
		
	}

	@Override
	public void onClicked(int x, int y, int mouseButton, double mouseX, double mouseY, GuiScannerBase gui)
	{
		
	}

}
