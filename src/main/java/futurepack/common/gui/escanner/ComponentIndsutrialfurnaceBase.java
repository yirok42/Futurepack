package futurepack.common.gui.escanner;

import java.util.List;

import com.mojang.blaze3d.platform.Lighting;
import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.PoseStack;

import futurepack.depend.api.helper.HelperComponent;
import net.minecraft.client.gui.screens.Screen;
import net.minecraft.world.item.ItemStack;

public class ComponentIndsutrialfurnaceBase extends ComponentBase
{

	protected List<ItemStack>[] in;
	protected List<ItemStack>[] out;

	public ComponentIndsutrialfurnaceBase(List<ItemStack>[] in, List<ItemStack>[] out)
	{
		super(null);
		this.in =  in;
		this.out = out;
	}
	
	@Override
	public void init(int maxWidth, Screen gui)
	{
		
	}

	@Override
	public int getWidth() 
	{
		
		return 100;
	}

	@Override
	public int getHeight()
	{
		
		return 64;
	}

	@Override
	public void render(PoseStack matrixStack, int x, int y, int blitOffset, int mouseX, int mouseY, GuiScannerBase gui)
	{
		RenderSystem.enableDepthTest();
		Lighting.setupFor3DItems();
		
		HelperComponent.drawBackground(matrixStack, x, y, this);
		
		Lighting.setupForFlatItems();
		
		HelperComponent.renderItemStackWithSlot(matrixStack, HelperComponent.getStack(in[0]), x+5, y+23, blitOffset);
		HelperComponent.renderItemStackWithSlot(matrixStack, HelperComponent.getStack(in[1]), x+23, y+23, blitOffset);
		HelperComponent.renderItemStackWithSlot(matrixStack, HelperComponent.getStack(in[2]), x+41, y+23, blitOffset);
		
		HelperComponent.renderIndFurn(matrixStack, x+5, y+23+16, blitOffset);
		HelperComponent.renderIndFurn(matrixStack, x+23, y+23+16, blitOffset);
		HelperComponent.renderIndFurn(matrixStack, x+41, y+23+16, blitOffset);
		
		HelperComponent.renderArrow(matrixStack, x+59, y+23, blitOffset);
		
		HelperComponent.renderItemStackWithSlot(matrixStack, HelperComponent.getStack(out[0]), x+77, y+5, blitOffset);
		HelperComponent.renderItemStackWithSlot(matrixStack, HelperComponent.getStack(out[1]), x+77, y+23, blitOffset);
		HelperComponent.renderItemStackWithSlot(matrixStack, HelperComponent.getStack(out[2]), x+77, y+41, blitOffset);
		
		Lighting.setupFor3DItems();
	}

	@Override
	public void postRendering(PoseStack matrixStack, int x, int y, int blitOffset, int mouseX, int mouseY, boolean hover, GuiScannerBase gui)
	{
		HelperComponent.renderItemText(matrixStack, HelperComponent.getStack(in[0]), x+5,y+23,mouseX, mouseY, gui);
		HelperComponent.renderItemText(matrixStack, HelperComponent.getStack(in[1]), x+23,y+23,mouseX, mouseY, gui);
		HelperComponent.renderItemText(matrixStack, HelperComponent.getStack(in[2]), x+41,y+23,mouseX, mouseY, gui);
		
		HelperComponent.renderItemText(matrixStack, HelperComponent.getStack(out[0]), x+77,y+5,mouseX, mouseY, gui);
		HelperComponent.renderItemText(matrixStack, HelperComponent.getStack(out[1]), x+77,y+23,mouseX, mouseY, gui);
		HelperComponent.renderItemText(matrixStack, HelperComponent.getStack(out[2]), x+77,y+41,mouseX, mouseY, gui);
	}

	@Override
	public void onClicked(int x, int y, int mouseButton, double mouseX, double mouseY, GuiScannerBase gui)
	{
		if(mouseButton==0)
		{
			if(		HelperComponent.isInBox(mouseX, mouseY, x+6, y+24, x+6+16, y+24+16))
			{
				HelperComponent.researchItem(HelperComponent.getStack(this.in[0]), gui.getResearchGui());
			}
			else if(HelperComponent.isInBox(mouseX, mouseY, x+24, y+24, x+24+16, y+24+16))
			{
				HelperComponent.researchItem(HelperComponent.getStack(this.in[1]), gui.getResearchGui());
			}
			else if(HelperComponent.isInBox(mouseX, mouseY, x+42, y+24, x+42+16, y+24+16))
			{
				HelperComponent.researchItem(HelperComponent.getStack(this.in[2]), gui.getResearchGui());
			}
			else if(HelperComponent.isInBox(mouseX, mouseY, x+78, y+6, x+78+16, y+6+16))
			{
				HelperComponent.researchItem(HelperComponent.getStack(this.out[0]), gui.getResearchGui());
			}
			else if(HelperComponent.isInBox(mouseX, mouseY, x+78, y+24, x+78+16, y+24+16))
			{
				HelperComponent.researchItem(HelperComponent.getStack(this.out[1]), gui.getResearchGui());
			}
			else if(HelperComponent.isInBox(mouseX, mouseY, x+78, y+42, x+78+16, y+42+16))
			{
				HelperComponent.researchItem(HelperComponent.getStack(this.out[2]), gui.getResearchGui());
			}
		}
	}

}
