package futurepack.common.gui.escanner;

import com.google.gson.JsonObject;
import com.mojang.blaze3d.platform.Lighting;
import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.PoseStack;

import futurepack.depend.api.helper.HelperGui;
import net.minecraft.client.gui.screens.Screen;
import net.minecraft.resources.ResourceLocation;

public class ComponentPicture extends ComponentBase
{
	int width;
	int height;
	ResourceLocation res;
	
	public ComponentPicture(JsonObject obj)
	{
		super(obj);
		/*
		 * {
		 * 	type:"image",
		 *  width:100,
		 *  height:50,
		 *  resource:"futurepack:textures/gui/Symboles.png"
		 * }
		 */
		width = obj.getAsJsonPrimitive("width").getAsInt();
		height = obj.getAsJsonPrimitive("height").getAsInt();
		res = new ResourceLocation(obj.getAsJsonPrimitive("resource").getAsString());
	}

	@Override
	public void init(int maxWidth, Screen gui)
	{
		if(width > maxWidth)
		{
			double dis = (double)height / (double)width;
			width = maxWidth;
			height = (int) (dis * maxWidth);
		}
	}

	@Override
	public int getWidth()
	{
		return width;
	}

	@Override
	public int getHeight()
	{
		return height;
	}

	@Override
	public void render(PoseStack matrixStack, int x, int y, int blitOffset, int mouseX, int mouseY, GuiScannerBase gui)
	{
		RenderSystem.setShaderColor(1F, 1F, 1F, 1F);
		Lighting.setupFor3DItems();
		RenderSystem.enableDepthTest();
		
		HelperGui.drawQuadWithTexture(matrixStack, res, x, y, 0.0f, 0.0f, getWidth(), getHeight(), 1, 1, 1, 1, blitOffset);	
	}

	@Override
	public void postRendering(PoseStack matrixStack, int x, int y, int blitOffset, int mouseX, int mouseY, boolean hover, GuiScannerBase gui)
	{
		
	}

}
