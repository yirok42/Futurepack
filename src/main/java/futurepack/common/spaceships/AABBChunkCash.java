package futurepack.common.spaceships;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Vec3i;
import net.minecraft.util.Mth;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.level.Level;
import net.minecraft.world.phys.AABB;
import net.minecraft.world.phys.shapes.VoxelShape;

public class AABBChunkCash
{
	public enum EnumAABB
	{
		UNDEFNINED,
		EMPTY,
		FULL,
		HALF_SLAP;
		
		public static EnumAABB VALUES[] = new EnumAABB[] {UNDEFNINED, EMPTY, FULL, HALF_SLAP};
		
		
		public boolean isBigger(EnumAABB b)
		{
			switch (this)
			{
			case UNDEFNINED:
			case EMPTY:
				return true;
			case FULL:
				return false;
			case HALF_SLAP:
				return b == FULL;
			default:
				return false;
			}
		}
		
		public static EnumAABB getEnumFromAABB(List<AABB> bbs)
		{
			if(bbs.isEmpty())
				return EMPTY;
			else if(bbs.size()==1)
			{
				return getEnumFromAABB(bbs.get(0));
			}
			
			EnumAABB en = EnumAABB.EMPTY;
			for(AABB bb : bbs)
			{
				EnumAABB b = getEnumFromAABB(bb);
				if(en.isBigger(b))
					en = b;
			}
			return en;
		}
		
		

		public static EnumAABB getEnumFromAABB(AABB bbs)
		{
			double w = bbs.maxX - bbs.minX;
			double h = bbs.maxY - bbs.minY;
			double d = bbs.maxZ - bbs.minZ;
			
			if(w == 0 && d == 0 && h == 0)
				return EnumAABB.EMPTY;
			
			boolean fx = w >= 0.75;
			boolean fz = d >= 0.75;
			
			//if(fx && fz)
			{
				if(h > 0.5)
					return FULL;
				else
					return HALF_SLAP;
			}
			
			//return UNDEFNINED;
		}
	}
	
	
	private Vec3i chunk_pos;
	int[] aabb;

	private boolean isEmpty;
	private boolean isFull;
	
	public AABBChunkCash(Vec3i pos)
	{
		chunk_pos = new Vec3i(pos.getX(), pos.getY(), pos.getZ());
		aabb = new int[256];
		isEmpty = false;
		isFull = false;
	}
	
	
	public void setAABB(int x, int y, int z, EnumAABB aabb)
	{
		int e = aabb.ordinal() & 3;
		
		x %= 16;
		y %= 16;
		z %= 16;
		int a = this.aabb[x * 16 + z];
		int b = a  & ~(3<<2*y);
		int c = b | (e<<y*2);
		
		this.aabb[x * 16 + z] = c;
	}
	
	public EnumAABB getAABB(int x, int y, int z)
	{
		x %= 16;
		y %= 16;
		z %= 16;
		
		int e = (aabb[x * 16 + z] >> (y*2) ) & 3;
		return EnumAABB.VALUES[e];
	}
	
	public Vec3i getPos()
	{
		return chunk_pos;
	}


	public void addCollisionBoxToList(Level w, int x, int y, int z, AABB entityBox, Entity entityIn, List<AABB> outList)
	{
		if(isEmpty)
			return;
		
		BlockPos pos = new BlockPos(chunk_pos.getX() * 16 + x, chunk_pos.getY()*16 + y, chunk_pos.getZ()*16 + z);
		if(isFull)
		{
			outList.add(new AABB(pos));
			return;
		}
		
		switch(getAABB(x, y, z))
		{
		case EMPTY:
			break;
		case FULL:
			outList.add(new AABB(pos));
			break;
		case HALF_SLAP:	
			outList.add(new AABB(pos.getX(), pos.getY(), pos.getZ(), pos.getX()+1, pos.getY()+0.5, pos.getZ()+1));
			break;
		case UNDEFNINED:
			Stream<VoxelShape> s = StreamSupport.stream(w.getBlockCollisions(null, new AABB(pos)).spliterator(), false);//getCollisionShapes
			//List<AxisAlignedBB> bbs = w.getCollisionBoxes(null, new AxisAlignedBB(pos));
			List<AABB> bbs = s.map(VoxelShape::toAabbs).flatMap(List::stream).collect(Collectors.toList());
			setAABBFromBlock(bbs, x, y, z);
			outList.addAll(bbs);
		}
	}


	private void setAABBFromBlock(List<AABB> bbs, int x, int y, int z)
	{
		EnumAABB bb = EnumAABB.getEnumFromAABB(bbs);
		setAABB(x, y, z, bb);
	}


	public void init(Level world)
	{
		for(int x=0;x<16;x++)
		{
			for(int z=0;z<16;z++)
			{
				for(int y=0;y<16;y++)
				{
					addCollisionBoxToList(world, x, y, z, null, null, new ArrayList<AABB>());
				}
			}
		}
		
		if(aabb[0] == 0xAAAAAAAA)
		{
			isFull = true;
			for(int aa : aabb)
			{
				if(aa != 0xAAAAAAAA)
				{
					isFull = false;
					break;
				}
			}
		}
		else if(aabb[0] == 0x55555555)
		{
			isEmpty = true;
			for(int aa : aabb)
			{
				if(aa != 0x55555555)
				{
					isEmpty = false;
					break;
				}
			}
		}
		return;
	}


	public void addCollisionBoxes(Level w, AABB aabb, Entity entityIn, List<AABB> outList)
	{
		if(isEmpty)
			return;
		
		AABB aabb2 = getIntersecting(aabb);
		if(aabb2 == null)
			return;
		
		if(isFull)
		{
			outList.add(aabb2);
			return;
		}
		
		int x1 = (Mth.floor(aabb2.minX) - 1);
		int x2 = (Mth.ceil(aabb2.maxX) + 1);
		int y1 = (Mth.floor(aabb2.minY) - 1);
		int y2 = (Mth.ceil(aabb2.maxY) + 1);
		int z1 = (Mth.floor(aabb2.minZ) - 1);
		int z2 = (Mth.ceil(aabb2.maxZ) + 1);
		
		if(x1 < 0)
			x1 = 0;
		if(x2 > 16)
			x2 = 16;
		if(y1 < 0)
			y1 = 0;
		if(y2 > 16)
			y2 = 16;
		if(z1 < 0)
			z1 = 0;
		if(z2 > 16)
			z2 = 16;

		for (int x = x1; x < x2; ++x)
		{
			for (int z = z1; z < z2; ++z)
			{
				for (int y = y1; y < y2; ++y)
				{	
					this.addCollisionBoxToList(w, x, y, z, aabb, entityIn, outList);
				}
			}		
		}
	}
	
	
	private AABB getIntersecting(AABB aabb)
	{
		int x = chunk_pos.getX() * 16;
		int y = chunk_pos.getY() * 16;
		int z = chunk_pos.getZ() * 16;
		
		double d0 = Math.max(x, aabb.minX);
        double d1 = Math.max(y, aabb.minY);
        double d2 = Math.max(z, aabb.minZ);
        double d3 = Math.min(x+16, aabb.maxX);
        double d4 = Math.min(y+16, aabb.maxY);
        double d5 = Math.min(z+16, aabb.maxZ);
        
        if(d0 > d3 || d1 > d4 || d2 > d5)
        {
        	return null;
        }
        return new AABB(d0 -x, d1 -y, d2 -z, d3 -x, d4 -y, d5 -z);
	}
	
}
