package futurepack.common.spaceships;

import futurepack.api.Constants;
import futurepack.api.interfaces.IPlanet;
import net.minecraft.resources.ResourceLocation;

public class PlanetUNKNOWN extends PlanetBase
{
	private ResourceLocation dim=new ResourceLocation("overworld");
	
	public PlanetUNKNOWN()
	{
		super(new ResourceLocation("overworld"), new ResourceLocation(Constants.MOD_ID, "textures/gui/undefinierbarer_datensatz.png"), "UNKNOWN", new String[0]);
	}

	protected void initDim(ResourceLocation id)
	{
		this.dim = id;
	}
	
	@Override
	public ResourceLocation getDimenionId()
	{
		return dim;
	}

	public PlanetUNKNOWN copy()
	{
		PlanetUNKNOWN unknown = new PlanetUNKNOWN();
		unknown.initDim(dim);
		unknown.setBreathableAtmosphere(this.hasBreathableAtmosphere());
		unknown.setCanFlyInDimension(this.canFlyInDimension());
		unknown.setOxygenProprties(getSpreadVelocity(), getGravityVelocity());
		
		return unknown;
	}
}
