package futurepack.common.spaceships;

import java.lang.ref.WeakReference;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import com.google.common.collect.ImmutableMap;

import futurepack.common.entity.EntityMovingShipBase;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.util.Mth;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.TorchBlock;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.properties.Property;
import net.minecraftforge.registries.ForgeRegistries;

public class MovingShip
{
	public final UUID shipID;
	private BlockState[][][] blocks;
	private CompoundTag[][][] tiles;
	private WeakReference<EntityMovingShipBase> shipEntity;
	
	private ListTag notliving;
	
	public MovingShip(BlockState[][][] blocks, CompoundTag[][][] tiles, EntityMovingShipBase shipEntity)
	{
		this.shipID = SpaceshipHasher.hash(blocks);
		
		if(shipEntity == null)
			throw new IllegalArgumentException("Entity cant be null!");
		if(shipEntity.level.isClientSide)
			throw new IllegalArgumentException("World must be Server side!");
		
		this.shipEntity = new WeakReference<EntityMovingShipBase>(shipEntity);
		
		if(blocks.length != tiles.length)
			throw new IllegalArgumentException("Array width does not match");
		if(blocks[0].length != tiles[0].length)
			throw new IllegalArgumentException("Array height does not match");
		if(blocks[0][0].length != tiles[0][0].length)
			throw new IllegalArgumentException("Array depth does not match");
		
		this.blocks = blocks;
		this.tiles = tiles;
		
		SpaceshipCashServer.register(shipID, this);
		shipEntity.setShipID(shipID);
	}
	
	public MovingShip(CompoundTag nbt, EntityMovingShipBase shipEntity)
	{
		this(loadBlocks(nbt), loadTiles(nbt) , shipEntity);
	}
	
	public BlockState[][][] getBlocks()
	{
		return blocks;
	}
	
	public CompoundTag[][][] getSavedTiles()
	{
		return tiles;
	}
	
	public int getWidth()
	{
		return blocks.length;
	}
	
	public int getHeight()
	{
		return blocks[0].length;
	}
	
	public int getDepth()
	{
		return blocks[0][0].length;
	}
	
	public EntityMovingShipBase getEntity()
	{
		return shipEntity != null ? shipEntity.get() : null;
	}
	
	public boolean isValid()
	{
		return shipEntity != null && shipEntity.get() != null && shipEntity.get().isAlive();
	}
	
	public CompoundTag write()
	{
		CompoundTag nbt = new CompoundTag();
		
		nbt.putByteArray("blocks", SpaceshipHasher.asBytes(blocks));
		nbt.putInt("width", tiles.length);
		nbt.putInt("height", tiles[0].length);
		nbt.putInt("depth", tiles[0][0].length);
		
		for(int x=0;x<tiles.length;x++)
		{
			for(int y=0;y<tiles[x].length;y++)
			{
				for(int z=0;z<tiles[x][y].length;z++)
				{
					if(tiles[x][y][z]!=null)
					{
						nbt.put(String.format("tile-%s-%s-%s", x,y,z), tiles[x][y][z]);
					}
				}
			}
		}
		
		return nbt;
	}
	
	private static BlockState[][][] loadBlocks(CompoundTag nbt)
	{
		byte[] data = nbt.getByteArray("blocks");
		return SpaceshipHasher.fromBytes(data);
	}
	
	private static CompoundTag[][][] loadTiles(CompoundTag nbt)
	{
		int w = nbt.getInt("width");
		int h = nbt.getInt("height");
		int d = nbt.getInt("depth");
		
		CompoundTag[][][] saved = new CompoundTag[w][h][d];
		
		for(String key : nbt.getAllKeys())
		{
			if(key.startsWith("tile-"))
			{
				String[] parts = key.split("-");
				int x = Integer.valueOf(parts[1]);
				int y = Integer.valueOf(parts[2]);
				int z = Integer.valueOf(parts[3]);
				
				saved[x][y][z] = nbt.getCompound(key);
			}
		}
		
		return saved;
	}
	
	public void startMoving(SafeBlockMover creator, List<LivingEntity> livingPassengers, List<Entity> objects)
	{
		if(objects!=null && !objects.isEmpty())
		{
			if(notliving==null)
				notliving = new ListTag();
			
			BlockPos rel = creator.getStartCoords();
			
			for(Entity e : objects)
			{
				e.setPos(e.getX() - rel.getX(), e.getY() - rel.getY(), e.getZ() - rel.getZ());
				CompoundTag nbt = new CompoundTag();
				if(e.saveAsPassenger(nbt))
				{
					notliving.add(nbt);
				}
				e.discard();
			}
			objects.clear();
			objects = null;
		}
		
		EntityMovingShipBase ship = shipEntity.get();
		
		if(livingPassengers!=null && !livingPassengers.isEmpty())
		{
			for(LivingEntity liv : livingPassengers)
			{
				ship.addChair(liv);
			}
		}
		
		ship.setDestination(creator.getEndCoords());
		
		creator.delete();
	}
	
	private Map<Block, Boolean> abort = new IdentityHashMap<Block, Boolean>();
	
	private void fixTorches(Level target, BlockPos pos, BlockState state)
	{
		Boolean bool = abort.get(state.getBlock());
		if(bool==null)
		{
			bool = !(state.getBlock() instanceof TorchBlock);
			abort.put(state.getBlock(), bool);
		}
		if(bool)	
		{
			return;
		}
			
		ImmutableMap<Property<?>, Comparable<? >> map = state.getValues();
		for(Entry<Property<?>, Comparable<?>> e : map.entrySet())
		{	
			if(e.getKey().getValueClass()== Direction.class)
			{
				if(e.getValue()== Direction.WEST || e.getValue()== Direction.NORTH || e.getValue()== Direction.DOWN)
				{
					BlockPos hanging = pos.relative((Direction) e.getValue(), -1);
					target.setBlockAndUpdate(hanging, Blocks.BEDROCK.defaultBlockState());
				}
				return;
			}
		}
		abort.put(state.getBlock(), true);
	}
	
	public void placeBlocks(Level target, BlockPos end)
	{
		for(int x=0;x<blocks.length;x++)
		{
			for(int y=0;y<blocks[x].length;y++)
			{
				for(int z=0;z<blocks[x][y].length;z++)
				{
					if(blocks[x][y][z]!=null)
					{
						BlockPos ne = end.offset(x,y,z);
							
						fixTorches(target, ne, blocks[x][y][z]);
							
						target.setBlock(ne, blocks[x][y][z], 2);
						BlockEntity t = target.getBlockEntity(ne);
						if(t!=null && tiles[x][y][z]!=null) //yes this has happened 0_o
						{
							tiles[x][y][z].putInt("x", ne.getX());
							tiles[x][y][z].putInt("y", ne.getY());
							tiles[x][y][z].putInt("z", ne.getZ());
							t.load(tiles[x][y][z]);
						}			
					}
				}
			}
		}
		abort.clear();
	}
	
	public void placeObjects(Level target, BlockPos end)
	{
		if(notliving!=null)
		{
			for(int i=0;i<notliving.size();i++)
			{
				CompoundTag nbt = notliving.getCompound(i);
				ResourceLocation res = new ResourceLocation(nbt.getString("id"));
				EntityType<?> type = ForgeRegistries.ENTITIES.getValue(res);
				if(type!=null)
				{
					Entity e = type.create(target);
					if(e != null)
					{
						e.load(nbt);
						e.setPos(e.getX() + end.getX(), e.getY() + end.getY(), e.getZ() + end.getZ());
						e.setUUID(Mth.createInsecureUUID(target.random));
						target.addFreshEntity(e);
					}
					
				}
			}
		}
	}
}
