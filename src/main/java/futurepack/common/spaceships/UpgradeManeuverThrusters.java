package futurepack.common.spaceships;

import java.util.Collection;

import futurepack.api.ParentCoords;
import futurepack.api.interfaces.IBlockValidator;
import futurepack.api.interfaces.ISpaceshipSelector;
import futurepack.api.interfaces.ISpaceshipUpgrade;
import futurepack.api.interfaces.tilentity.ITileBoardComputer;
import futurepack.common.block.BlockRotateableTile;
import futurepack.common.block.logistic.frames.BlockManeuverThruster;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TextComponent;
import net.minecraft.world.level.Level;

public class UpgradeManeuverThrusters implements ISpaceshipUpgrade, IBlockValidator
{
	
	private Component error = null;
	
	@Override
	public boolean isValidBlock(Level w, ParentCoords pos) 
	{
		return w.getBlockState(pos).getBlock() instanceof BlockManeuverThruster;
	}

	@Override
	public String getTranslationKey() 
	{
		return "thrusters.maneuver";
	}

	@Override
	public boolean isUpgradeInstalled(ISpaceshipSelector sel) 
	{
		error = null;
		int directions = 0;
		Collection<ParentCoords> posses = sel.getSelector().getValidBlocks(this);
		for(ParentCoords pos : posses)
		{
			directions |= (1<<sel.getWorld().getBlockState(pos).getValue(BlockRotateableTile.FACING).ordinal());
		}
		if(directions == 0b111111)
		{
			return true;
		}
		else
		{
			error = new TextComponent("Missing Directions: DUNSWE " + Integer.toBinaryString(directions));
			
			return false;
		}
	}

	@Override
	public boolean isBoardComputerValid(ITileBoardComputer tile) 
	{
		return true;
	}

	@Override
	public Component getErrorMessage() 
	{
		return error;
	}
}
