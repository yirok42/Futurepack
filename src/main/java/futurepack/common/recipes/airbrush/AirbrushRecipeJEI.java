package futurepack.common.recipes.airbrush;

import net.minecraft.world.item.ItemStack;

public class AirbrushRecipeJEI
{
	private final ItemStack colorItem, input, output;

	public AirbrushRecipeJEI(ItemStack colorItem, ItemStack input, ItemStack output)
	{
		super();
		this.colorItem = colorItem;
		this.input = input;
		this.output = output;
	}

	public ItemStack getColorItem()
	{
		return colorItem;
	}

	public ItemStack getInput()
	{
		return input;
	}

	public ItemStack getOutput()
	{
		return output;
	}		
	
	@Override
	public boolean equals(Object obj)
	{
		if(obj==null)
			return false;
		else if(this==obj)
			return true;
		else if(obj instanceof AirbrushRecipeJEI)	
		{
			return ItemStack.matches(colorItem, ((AirbrushRecipeJEI) obj).colorItem) && ItemStack.matches(input, ((AirbrushRecipeJEI) obj).input) && ItemStack.matches(output, ((AirbrushRecipeJEI) obj).output);
		}
		return false;
	}
}
