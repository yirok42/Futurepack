package futurepack.common.recipes;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.function.Consumer;

import org.apache.logging.log4j.Level;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import futurepack.common.AsyncTaskManager;
import futurepack.common.FPLog;
import futurepack.common.recipes.assembly.FPAssemblyManager;
import futurepack.common.recipes.centrifuge.FPZentrifugeManager;
import futurepack.common.recipes.crushing.FPCrushingManager;
import futurepack.common.recipes.industrialfurnace.FPIndustrialFurnaceManager;
import futurepack.common.recipes.industrialfurnace.FPIndustrialNeonFurnaceManager;
import futurepack.common.recipes.recycler.FPRecyclerLaserCutterManager;
import futurepack.common.recipes.recycler.FPRecyclerShredderManager;
import futurepack.common.recipes.recycler.FPRecyclerTimeManipulatorManager;
import futurepack.depend.api.helper.HelperOreDict;
import net.minecraft.resources.ResourceLocation;

public class Json2Recipes 
{
	private static final Map<String, Consumer<JsonArray>> recipeManagers = new TreeMap<String, Consumer<JsonArray>>();
	
	static
	{
		register(FPAssemblyManager.NAME, FPAssemblyManager::init);
		register(FPZentrifugeManager.NAME, FPZentrifugeManager::init);
		register(FPCrushingManager.NAME, FPCrushingManager::init);
		register(FPIndustrialFurnaceManager.NAME, FPIndustrialFurnaceManager::init);
		register(FPIndustrialNeonFurnaceManager.NAME, FPIndustrialNeonFurnaceManager::init);
		register(FPRecyclerTimeManipulatorManager.NAME, FPRecyclerTimeManipulatorManager::init);
		register(FPRecyclerLaserCutterManager.NAME, FPRecyclerLaserCutterManager::init);
		register(FPRecyclerShredderManager.NAME, FPRecyclerShredderManager::init);
	}
	
	public static void register(String recipeType, Consumer<JsonArray> process)
	{
		if(recipeManagers.get(recipeType) == null)
		{
			recipeManagers.put(recipeType, process);
		}
		else
		{
			throw new IllegalArgumentException("Recipe type " + recipeType + " is already registered");
		}
	}
	
	public static void readerJson(ResourceLocation path, JsonObject obj)
	{
		try
		{
			String type = obj.getAsJsonPrimitive("type").getAsString();
			JsonArray recipes = obj.getAsJsonArray("recipes");
			Consumer<JsonArray> manager = recipeManagers.get(type);
			if(manager==null)
			{
				FPLog.logger.warn("Unknown recipe type %s used in %s", type, path);
			}
			else
			{
				runWhenTagsAreLoaded(() -> manager.accept(recipes));
			}
			
		}
		catch(Exception e)
		{
			FPLog.logger.error("Exception while loading recipe file %s", path);
			FPLog.logger.catching(Level.ERROR, e);
		}
	}
	
	private static List<Runnable> todos;
	
	private static void runWhenTagsAreLoaded(Runnable todo)
	{
		if(HelperOreDict.areTagsLoaded())
		{
			AsyncTaskManager.addTask(AsyncTaskManager.RESOURCE_RELOAD, todo, true);
		}
		else
		{
			Runnable task = () -> {
				HelperOreDict.waitForTagsToLoad();
				synchronized (Json2Recipes.class)
				{
					for(Runnable r : todos)
					{
						AsyncTaskManager.addTask(AsyncTaskManager.RESOURCE_RELOAD, r, true);
					}
					todos = null;
				}
				
			};
			synchronized (Json2Recipes.class)
			{
				if(todos==null)
				{
					todos = Collections.synchronizedList(new ArrayList<Runnable>());
					Thread t = new Thread(task, "Wait fro tags to load");
					t.start();
				}

				todos.add(todo);
			}	
		}
	}
}
