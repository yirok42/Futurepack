package futurepack.common.recipes.assembly;

import futurepack.api.ItemPredicateBase;
import futurepack.common.item.tools.compositearmor.CompositeArmorPart;
import futurepack.common.recipes.EnumRecipeSync;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.world.item.ItemStack;

public class AssemblyArmorRecipe extends AssemblyRecipe
{

	public AssemblyArmorRecipe(String id, ItemStack out, ItemPredicateBase[] in)
	{
		super(id, out, in);
		out.getOrCreateTagElement(CompositeArmorPart.NBT_TAG_NAME);
	}
	
	@Override
	public ItemStack getOutput(ItemStack[] in)
	{
		ItemStack in1 = in[0].copy();
		in1.setCount(1);
		in1.getOrCreateTagElement(CompositeArmorPart.NBT_TAG_NAME);
		return in1;
	}
	
	
	@Override
	public void write(FriendlyByteBuf buf)
	{
		super.write(buf);
	}
	
	public static AssemblyArmorRecipe read(FriendlyByteBuf buf)
	{
		return new AssemblyArmorRecipe(buf.readUtf(), buf.readItem(), EnumRecipeSync.readPredicates(buf));
	}

}
