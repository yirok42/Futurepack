package futurepack.common.recipes.assembly;

import java.util.Arrays;
import java.util.Random;

import futurepack.api.ItemPredicateBase;
import futurepack.common.modification.IModificationPart;
import futurepack.common.modification.IModificationPart.EnumCorePowerType;
import futurepack.common.modification.PartsManager;
import futurepack.common.recipes.EnumRecipeSync;
import futurepack.depend.api.ItemPredicate;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.world.item.ItemStack;

public class AssemblyDualRecipe extends AssemblyRecipe
{

	public AssemblyDualRecipe(String id, ItemStack out, ItemPredicateBase[] in)
	{
		super(id, out, in);
	}
	
	public AssemblyDualRecipe(String id, ItemStack out, ItemStack[] in)
	{
		super(id, out, Arrays.stream(in).map(ItemPredicate::new).toArray(ItemPredicate[]::new));//wow something like this works + it looks not completly horrible
	} 
	
	@Override
	public ItemStack getOutput(ItemStack[] in)
	{
		ItemStack it = super.getOutput(in);
		
		if(!it.hasTag())
		{
			it.setTag(new CompoundTag());
		}
		
		int ram = 0;
		int core = 0;
		int corebase=0;
		float rambase=0;
		
		for(int i=0;i<in.length;i++)
		{
			IModificationPart part = PartsManager.getPartFromItem(in[i]);
			if(part==null)
				continue;
			
			if(part != null)
			{	
				int c = part.getCorePower(EnumCorePowerType.PROVIDED);
				if(c>0)
				{
					corebase = Math.max(corebase, c);
					core += c;
				}
				
				float r = part.getRamSpeed();
				if(r>0)
				{
					rambase = Math.max(rambase, r);
					ram += r;
				}	
			}
		}
		Random r = new Random();
		
		if(ram>0)
			ram = ram - r.nextInt((int)rambase);
		if(core>0)
			core = core - r.nextInt(corebase);
		
		it.getTag().putInt("rambase", ram);
		it.getTag().putInt("corebase", core);
		
		return it; 
	}
	
	@Override
	public void write(FriendlyByteBuf buf)
	{
		super.write(buf);
	}
	
	public static AssemblyDualRecipe read(FriendlyByteBuf buf)
	{
		return new AssemblyDualRecipe(buf.readUtf(), buf.readItem(), EnumRecipeSync.readPredicates(buf));
	}
}
