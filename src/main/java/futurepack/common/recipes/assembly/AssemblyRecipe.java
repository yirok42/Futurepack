package futurepack.common.recipes.assembly;

import java.util.Arrays;

import futurepack.api.ItemPredicateBase;
import futurepack.api.interfaces.IOptimizeable;
import futurepack.common.recipes.EnumRecipeSync;
import futurepack.common.research.CustomPlayerData;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.world.item.ItemStack;

public class AssemblyRecipe implements IOptimizeable
{
	public final String id;
	private ItemPredicateBase[] input = new ItemPredicateBase[3];
	private ItemStack output;
//	private InfoSlot[] slots;

	public AssemblyRecipe(String id, ItemStack out, ItemPredicateBase[] in) 
	{
		this.id = id;
		this.output = out.copy();
		for(int i=0;i<input.length;i++)
		{
			input[i] = in[i];
		}		
		
//		slots = new InfoSlot[in.length+1];
//		for(int i=0;i<input.length;i++)
//		{
//			int px = (width()-(18*input.length)) / 2 + (18*i);
//			int py = 6;
//			slots[i] = new InfoSlot(px, py, input[i], new String[]{});
//			slots[i].setSlotType(SlotType.INPUT_SLOT);
//		}
//		
//		int px = 34;//5 + 18 *input.length+5;
//		int py = 28;
//		slots[input.length] = new InfoSlot(px, py, output, new String[]{});
//		slots[input.length].setSlotType(SlotType.OUTPUT_SLOT);
//		slots[input.length].height+=4;
//		slots[input.length].width+=4;
	}
	
	public boolean matches(ItemStack[] in)
	{
		for(int i=0;i<input.length;i++)
		{
			if(input[i]==null)
				continue;
			if(in[i]==null || in[i].isEmpty())
				return false;
			if(!input[i].apply(in[i], false))
				return false;
		}
		return true;
	}
	
	public void useItems(ItemStack[] in)
	{
		for(int i=0;i<input.length;i++)
		{
			if(input[i]==null)
				continue;
			in[i].shrink(input[i].getMinimalItemCount(in[i]));
			if(in[i].getCount()<=0)
				in[i]=ItemStack.EMPTY;
		}
	}
	
	public ItemStack getOutput(ItemStack[] in) 
	{
		return output.copy();
	}
	
	@Override
	public String toString() 
	{
		return "AssemblyTable: " + id + " " +Arrays.toString(input) + " to " + output;
	}
	
	public boolean isLocalResearched()
	{
		return CustomPlayerData.createForLocalPlayer().canProduce(output);
	}
	
	public ItemPredicateBase[] getInput()
	{
		return input;
	}

	public ItemStack getOutput()
	{
		return output;
	}

	private int points = 0;
	
	@Override
	public void addPoint()
	{
		points++;
	}
	
	@Override
	public int getPoints()
	{
		return points;
	}
	
	@Override
	public void resetPoints()
	{
		points = 0;	
	}
	
	public void write(FriendlyByteBuf buf)
	{
		buf.writeUtf(id);
		buf.writeItem(output);
		EnumRecipeSync.writePredicates(input, buf);
	}
	
	public static AssemblyRecipe read(FriendlyByteBuf buf)
	{
		return new AssemblyRecipe(buf.readUtf(), buf.readItem(), EnumRecipeSync.readPredicates(buf));
	}
}
