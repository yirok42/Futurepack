package futurepack.common.modification;

public interface IPartCore extends IModificationPart
{
	@Override
	default boolean isRam()
	{
		return false;
	}
	
	@Override
	default boolean isCore()
	{
		return true;
	}
	
	@Override
	default boolean isChip()
	{
		return false;
	}
	
	@Override
	default float getRamSpeed()
	{
		return 0;
	}
	
	@Override
	default float getChipPower(EnumChipType type)
	{
		return 0;
	}
	
	@Override
	default int getCorePower(EnumCorePowerType type)
	{
		switch (type)
		{
		case PROVIDED:
		case BOTH:
			return getCorePower();
		default:
			return 0;
		}
	}
	
	int getCorePower();
}
