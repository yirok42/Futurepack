package futurepack.common.filter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.script.Bindings;

import net.minecraft.nbt.ByteArrayTag;
import net.minecraft.nbt.ByteTag;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.DoubleTag;
import net.minecraft.nbt.FloatTag;
import net.minecraft.nbt.IntArrayTag;
import net.minecraft.nbt.IntTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.nbt.LongArrayTag;
import net.minecraft.nbt.LongTag;
import net.minecraft.nbt.ShortTag;
import net.minecraft.nbt.StringTag;
import net.minecraft.nbt.Tag;

public class NBTWrapper implements Bindings
{
	public final CompoundTag nbt;

	public NBTWrapper(CompoundTag base)
	{
		this.nbt = base;
	}

	@Override
	public int size() 
	{
		return nbt.size();
	}

	@Override
	public boolean isEmpty() 
	{
		return nbt.isEmpty();
	}

	@Override
	public boolean containsValue(Object value) 
	{
		return values().contains(value);
	}

	@Override
	public void clear() 
	{
		throw new UnsupportedOperationException("clear");
	}

	@Override
	public Set<String> keySet() 
	{
		return nbt.getAllKeys();
	}

	@Override
	public Collection<Object> values() 
	{
		Set<String> keys = keySet();
		ArrayList<Object> list = new ArrayList<Object>(keys.size());
		for(String k : keys)
		{
			list.add(get(k));
		}
		return list;
	}

	@Override
	public Set<Entry<String, Object>> entrySet() 
	{
		Set<String> keys = keySet();
		Map<String, Object> entries = new HashMap<>(keys.size());
		for(String k : keys)
		{
			entries.put(k, get(k));
		}
		return entries.entrySet();
	}

	@Override
	public Object put(String name, Object value) 
	{
		Object old = get(name);
		nbt.put(name, getNBTBase(value));
		return old;
	}

	@Override
	public void putAll(Map<? extends String, ? extends Object> toMerge) 
	{
		toMerge.forEach(this::put);
	}

	@Override
	public boolean containsKey(Object key) 
	{
		return nbt.contains((String) key);
	}

	@Override
	public Object get(Object key) 
	{
		Tag base = nbt.get((String) key);
		return base == null ? null : getValue(base);
	}

	@Override
	public Object remove(Object key) 
	{
		Object ret = get(key);
		nbt.remove((String)key);
		return ret;
	}

	public static Tag getNBTBase(Object obj)
	{
		Class<?> c = obj.getClass();
		if(c==byte.class)
		{
			return ByteTag.valueOf((byte) obj);
		}
		else if(c==short.class || c==Short.class)
		{
			return ShortTag.valueOf((short) obj);
		}
		else if(c==int.class || c==Integer.class)
		{
			return IntTag.valueOf((int) obj);
		}
		else if(c==long.class || c==Long.class)
		{
			return LongTag.valueOf((long) obj);
		}
		else if(c==float.class)
		{
			return FloatTag.valueOf((float) obj);
		}
		else if(c==double.class || c==Double.class)
		{
			return DoubleTag.valueOf((double) obj);
		}
		else if(c==byte[].class)
		{
			return new ByteArrayTag((byte[]) obj);
		}
		else if(c==String.class)
		{
			return StringTag.valueOf((String) obj);
		}
		else if(c==int[].class)
		{
			return new IntArrayTag((int[]) obj);
		}
		else if(c==long[].class)
		{
			return new LongArrayTag((long[]) obj);
		}
		else if(c.isArray())
		{
			Object[] oa = (Object[]) obj;
			ListTag list = new ListTag();
			for(Object o : oa)
			{
				list.add(getNBTBase(o));
			}
			return list;
		}
		else if( obj instanceof Map)
		{
			Map<String, Object> map = (Map<String, Object>) obj;
			CompoundTag compount = new CompoundTag();
			map.forEach((k,v) -> compount.put(k, getNBTBase(v)));
			return compount;
		}
		else
		{
			throw new IllegalArgumentException("Object " + obj + " of class " + c + " could not be turned into an nbt tag");
		}
	}
	
	public static Object getValue(Tag base)
	{
		switch(base.getId()) {
	      case 0:
	         return null;
	      case 1:
	         return ((ByteTag)base).getAsByte();
	      case 2:
	         return ((ShortTag)base).getAsShort();
	      case 3:
	         return ((IntTag)base).getAsInt();
	      case 4:
	         return ((LongTag)base).getAsLong();
	      case 5:
	         return ((FloatTag)base).getAsFloat();
	      case 6:
	         return ((DoubleTag)base).getAsDouble();
	      case 7:
	         return ((ByteArrayTag)base).getAsByteArray();
	      case 8:
	         return ((StringTag)base).getAsString();
	      case 11:
	    	  return ((IntArrayTag)base).getAsIntArray();
	      case 12:
	    	  return ((LongArrayTag)base).getAsLongArray();
	      case 10:
	         return new NBTWrapper((CompoundTag)base);
	      case 9:
	    	  ListTag list = (ListTag) base;
	    	  Object[] raw = list.toArray();
	    	  for(int i=0;i<raw.length;i++)
	    	  {
	    		  raw[i] = getValue((Tag) raw[i]);
	    	  }
	    	  return raw;
	      
	      default:
	         return new IllegalArgumentException("Unknown NBTType " + base.getClass() + " id " + base.getId() + " " + base);
	      }
	}
}
