package futurepack.common.sync;

import java.util.function.Supplier;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.screens.Screen;
import net.minecraft.client.gui.screens.inventory.AbstractContainerScreen;
import net.minecraft.core.BlockPos;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.world.entity.Entity;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.network.NetworkDirection;
import net.minecraftforge.network.NetworkEvent;

public class MessageOpenGuiClient
{
	FPGuiHandler id;
	int containerID;
	private Object[] data;
	
	protected MessageOpenGuiClient(FPGuiHandler gui, int containerID, Object[] data)
	{
		this.id = gui;
		this.containerID = containerID;
		this.data = data;
	}
	
	public MessageOpenGuiClient(FPGuiHandler gui, int containerID, BlockPos pos)
	{
		this(gui, containerID, new Object[]{pos});
	}
	
	public MessageOpenGuiClient(FPGuiHandler gui, int containerID, Entity entity)
	{
		this(gui, containerID, new Object[]{entity});
	}
	
	public static MessageOpenGuiClient decode(FriendlyByteBuf buf) 
	{
		FPGuiHandler id = FPGuiHandler.fromId(buf.readVarInt());
		int window = buf.readVarInt();
		Object[] data = id.readFromBuffer(buf);
		return new MessageOpenGuiClient(id, window, data);
	}
	
	public static void encode(MessageOpenGuiClient msg, FriendlyByteBuf buf) 
	{
		buf.writeVarInt(msg.id.getID());
		buf.writeVarInt(msg.containerID);
		msg.id.writeToBuffer(buf, msg.data);
	}
	
	public static void consume(MessageOpenGuiClient message, Supplier<NetworkEvent.Context> sctx) 
	{
		NetworkEvent.Context ctx = sctx.get();
		
		if(ctx.getDirection() == NetworkDirection.PLAY_TO_CLIENT)
		{
			ctx.enqueueWork(() -> 
			{
				openGuiClient(message, ctx);
            });
		}
		ctx.setPacketHandled(true);
	}
	
	private static void openGuiClient(MessageOpenGuiClient message, NetworkEvent.Context ctx)
	{
		DistExecutor.runWhenOn(Dist.CLIENT, () -> new Runnable()
		{
			@Override
			public void run() 
			{
				Supplier<Screen> sup = message.id.getGui(Minecraft.getInstance().player, Minecraft.getInstance().level, message.data);
				if (sup != null) 
				{
					Screen gui = sup.get();
					if (gui != null) 
					{
						if(gui instanceof AbstractContainerScreen<?>)
						{
							AbstractContainerScreen<?> cont = (AbstractContainerScreen)gui;
							Minecraft.getInstance().player.containerMenu  = cont.getMenu();
							Minecraft.getInstance().player.containerMenu.containerId = message.containerID;
						}
						
						Minecraft.getInstance().setScreen(gui);
					}
				}
			}
		});
	}
}
