package futurepack.common.sync;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.function.Supplier;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.screens.inventory.AbstractContainerScreen;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.world.inventory.AbstractContainerMenu;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.network.NetworkEvent;

public class MessageAdditionalContainerData<T extends AbstractContainerMenu & ISyncable> 
{
	private final byte[] data;
	
	public MessageAdditionalContainerData(T t) throws IOException
	{
		ISyncable data = t;
		ByteArrayOutputStream bout = new ByteArrayOutputStream(100);
		DataOutputStream out = new DataOutputStream(bout);

		data.writeAdditional(out);
		out.close();
		this.data = bout.toByteArray();
	}
	
	public MessageAdditionalContainerData(byte[] data)
	{
		this.data = data;
	}
	
	public static MessageAdditionalContainerData decode(FriendlyByteBuf buf) 
	{
		byte[] barr = buf.readByteArray();
		return new MessageAdditionalContainerData(barr);
	}
	
	public static void encode(MessageAdditionalContainerData msg, FriendlyByteBuf buf) 
	{
		buf.writeByteArray(msg.data);
		
	}
	
	public static void consume(MessageAdditionalContainerData message, Supplier<NetworkEvent.Context> ctx) 
	{
		DistExecutor.runWhenOn(Dist.CLIENT, () -> new Runnable()
		{
			@Override
			public void run() 
			{
				AbstractContainerScreen screen = (AbstractContainerScreen) Minecraft.getInstance().screen;	
				ISyncable data = null;
				if(screen!=null)
					data = (ISyncable)screen.getMenu();
				else
				{
					if(Minecraft.getInstance().player.containerMenu instanceof ISyncable)
					{
						data = (ISyncable) Minecraft.getInstance().player.containerMenu;
					}
				}
				
				if(data!=null)
				{
					ByteArrayInputStream bin = new ByteArrayInputStream(message.data);
					DataInputStream in = new DataInputStream(bin);
					try
					{
						data.readAdditional(in);
						in.close();
					}
					catch(IOException e)
					{
						e.printStackTrace();
					}
				}
			}
		});
		ctx.get().setPacketHandled(true);
	}
}
