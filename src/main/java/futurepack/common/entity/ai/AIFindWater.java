package futurepack.common.entity.ai;

import java.util.EnumSet;

import net.minecraft.core.BlockPos;
import net.minecraft.util.Mth;
import net.minecraft.world.entity.Mob;
import net.minecraft.world.entity.ai.goal.Goal;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.levelgen.Heightmap;
import net.minecraft.world.level.material.Material;
import net.minecraft.world.phys.Vec3;

public class AIFindWater extends Goal
{
	private final Mob parentEntity;
	private final ExtendedMovementController controller;

	private BlockPos foundWater;
	
	public AIFindWater(Mob liv)
	{
		parentEntity = liv;
		this.setFlags(EnumSet.of(Goal.Flag.MOVE));
		liv.moveControl = controller = new ExtendedMovementController(liv);
	}
	
	@Override
	public boolean canUse()
	{
		if(!parentEntity.level.isDay())
			return false;
			
		BlockPos ent = new BlockPos(parentEntity.position()).below();
		if(parentEntity.level.getBlockState(ent).getMaterial() == Material.WATER)
		{
			return false;
		}
		Level w = parentEntity.level;
		
		int x = Mth.floor(parentEntity.getX() + (w.random.nextDouble()*2-1)*32 );
		int z = Mth.floor(parentEntity.getZ() + (w.random.nextDouble()*2-1)*32);

								
		for(int i=0;i<8;i++)
		{
			for(int j=0;j<8;j++)
			{				
				int y = w.getHeight(Heightmap.Types.WORLD_SURFACE, x+i, z+i) -1;
				BlockPos pos = new BlockPos(x+i, y, z+j);
				if(w.getBlockState(pos).getMaterial() == Material.WATER)
				{
					foundWater = pos;
					return true;
				}
			}
		}
		
		return false;		
	}
	
	
	@Override
	public void start()
	{
		if(foundWater!=null)
		{
			parentEntity.getMoveControl().setWantedPosition(foundWater.getX()+0.5, foundWater.getY()+0.5, foundWater.getZ()+0.5, 1.0F);
		}
	}
	
	@Override
	public boolean canContinueToUse()
	{
		if(foundWater==null)
			return false;
		if(!parentEntity.level.canSeeSkyFromBelowWater(foundWater))
			return false;
		
		double dis = foundWater.distToCenterSqr(parentEntity.position());
		
		if(dis<=2F)
			return false;
		
		double x = parentEntity.getMoveControl().getWantedX();
		double y = parentEntity.getMoveControl().getWantedY();
		double z = parentEntity.getMoveControl().getWantedZ();	
		
		double dis2 = parentEntity.distanceToSqr(x, y, z);
		if(dis2 < 2F)
		{
			parentEntity.getMoveControl().setWantedPosition(foundWater.getX()+0.5, foundWater.getY()+0.5, foundWater.getZ()+0.5, 1.0F);
		}
		else if(dis2 > 255)
		{
			Vec3 vec = parentEntity.position();
			Vec3 sub = vec.vectorTo(Vec3.atLowerCornerOf(foundWater));
			double l = sub.length();
			sub = sub.scale(14 / l);
			
			Vec3 pos = vec.add(sub);
	
			parentEntity.getMoveControl().setWantedPosition(pos.x, pos.y, pos.z, 1.0F);	
		}
		else if(controller.isWaiting())//AT to make this public
		{
			double x2 = x + parentEntity.getRandom().nextDouble()*2 -1;
			double y2 = y + parentEntity.getRandom().nextDouble()*2 -1;
			double z2 = z + parentEntity.getRandom().nextDouble()*2 -1;
			
			dis = parentEntity.distanceToSqr(x2, y2, z2);
			
			if(dis < dis2 +4)
			{
				BlockPos pos = new BlockPos(x2,y2,z2);
				BlockState state = parentEntity.level.getBlockState(pos);
				if(state.getCollisionShape(parentEntity.level, pos).isEmpty())
				{
					x=x2;
					y=y2;
					z=z2;
					parentEntity.getMoveControl().setWantedPosition(x2, y2, z2, 1.0F);	
				}
			}
		}
		
		//WorldServer serv = (WorldServer) parentEntity.world;
		//serv.addParticle(ParticleTypes.NOTE, true, x, y, z, 1, 0D, 0D, 0D, 0F);
		
		
		return dis > 2F;
	}
	
	@Override
	public void stop()
	{
		super.stop();
		foundWater = null;
	}
}
