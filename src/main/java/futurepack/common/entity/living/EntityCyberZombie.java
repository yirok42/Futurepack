package futurepack.common.entity.living;

import futurepack.common.FPEntitys;
import futurepack.common.potions.FPPotions;
import net.minecraft.world.effect.MobEffectInstance;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.ai.attributes.AttributeSupplier.Builder;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.entity.monster.Zombie;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;

public class EntityCyberZombie extends Zombie
{

	public EntityCyberZombie(EntityType<EntityCyberZombie> type, Level worldIn)
	{
		super(type, worldIn);
	}
	
	public EntityCyberZombie(Level worldIn)
	{
		this(FPEntitys.CYBER_ZOMBIE, worldIn);
	}
	
	
	public static Builder registerAttributes()
    {
		return Zombie.createAttributes().add(Attributes.ARMOR, 4.0D);
    }
	
	@Override
	public boolean doHurtTarget(Entity entityIn)
    {
        boolean flag = super.doHurtTarget(entityIn);

        if (flag && this.getMainHandItem().isEmpty() && entityIn instanceof LivingEntity)
        {
            float f = this.level.getCurrentDifficultyAt(this.blockPosition()).getEffectiveDifficulty();
            ((LivingEntity)entityIn).addEffect(new MobEffectInstance(FPPotions.PARALYZED, 14 * (int)f));
        }

        return flag;
    }

	@Override
	protected ItemStack getSkull()
    {
        return ItemStack.EMPTY;
    }
}
