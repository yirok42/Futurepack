package futurepack.common.entity.living;

import java.nio.charset.StandardCharsets;
import java.util.UUID;

import futurepack.common.FPEntitys;
import futurepack.common.item.FoodItems;
import futurepack.common.item.misc.MiscItems;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.network.syncher.EntityDataAccessor;
import net.minecraft.network.syncher.EntityDataSerializers;
import net.minecraft.network.syncher.SynchedEntityData;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.Mob;
import net.minecraft.world.entity.ai.attributes.AttributeInstance;
import net.minecraft.world.entity.ai.attributes.AttributeModifier;
import net.minecraft.world.entity.ai.attributes.AttributeModifier.Operation;
import net.minecraft.world.entity.ai.attributes.AttributeSupplier;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.entity.ai.goal.BreedGoal;
import net.minecraft.world.entity.ai.goal.FloatGoal;
import net.minecraft.world.entity.ai.goal.FollowOwnerGoal;
import net.minecraft.world.entity.ai.goal.FollowParentGoal;
import net.minecraft.world.entity.ai.goal.LeapAtTargetGoal;
import net.minecraft.world.entity.ai.goal.LookAtPlayerGoal;
import net.minecraft.world.entity.ai.goal.MeleeAttackGoal;
import net.minecraft.world.entity.ai.goal.PanicGoal;
import net.minecraft.world.entity.ai.goal.RandomLookAroundGoal;
import net.minecraft.world.entity.ai.goal.SitWhenOrderedToGoal;
import net.minecraft.world.entity.ai.goal.WaterAvoidingRandomStrollGoal;
import net.minecraft.world.entity.ai.goal.target.HurtByTargetGoal;
import net.minecraft.world.entity.ai.goal.target.NearestAttackableTargetGoal;
import net.minecraft.world.entity.ai.goal.target.NonTameRandomTargetGoal;
import net.minecraft.world.entity.ai.goal.target.OwnerHurtByTargetGoal;
import net.minecraft.world.entity.ai.goal.target.OwnerHurtTargetGoal;
import net.minecraft.world.entity.animal.Animal;
import net.minecraft.world.entity.animal.Turtle;
import net.minecraft.world.entity.animal.Wolf;
import net.minecraft.world.entity.animal.horse.AbstractHorse;
import net.minecraft.world.entity.monster.AbstractSkeleton;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.phys.Vec3;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

//TODO: vollmond boost
public class EntityAlphaJawaul extends EntityRideableAnimalBase
{
	private static final EntityDataAccessor<Byte> SPEED_BOOST = SynchedEntityData.defineId(EntityAlphaJawaul.class, EntityDataSerializers.BYTE);
	private static final EntityDataAccessor<Byte> GROWTH = SynchedEntityData.defineId(EntityAlphaJawaul.class, EntityDataSerializers.BYTE);
	private static final EntityDataAccessor<Byte> ATTACK_BOOST = SynchedEntityData.defineId(EntityAlphaJawaul.class, EntityDataSerializers.BYTE);

	private static final UUID SPEED_BOOST_ID = UUID.nameUUIDFromBytes("tamed jawaul speed boost".getBytes(StandardCharsets.UTF_8));
	private static final UUID ATTACK_BOOST_ID = UUID.nameUUIDFromBytes("tamed jawaul attack boost".getBytes(StandardCharsets.UTF_8));

	private static final UUID AGE_ID = UUID.nameUUIDFromBytes("tamed jawaul age boost".getBytes(StandardCharsets.UTF_8));
	
	
	public static AttributeSupplier.Builder registerAttributes()
	{
		return Mob.createMobAttributes().add(Attributes.MOVEMENT_SPEED, (double) 0.3F)
				.add(Attributes.MAX_HEALTH, 20.0D)
				.add(Attributes.ATTACK_DAMAGE, 4.0D)
				.add(Attributes.JUMP_STRENGTH);
	}

	public static final int BOOL_IS_FAINTED = 128;

	public EntityAlphaJawaul(EntityType<? extends EntityAlphaJawaul> type, Level worldIn)
	{
		super(type, worldIn);
	}

	@Override
	protected void registerGoals()
	{
		this.goalSelector.addGoal(1, new PanicGoal(this, 1.2D));
		this.goalSelector.addGoal(2, new BreedGoal(this, 1.0D, AbstractHorse.class));
		this.goalSelector.addGoal(4, new FollowParentGoal(this, 1.0D));
		this.goalSelector.addGoal(6, new WaterAvoidingRandomStrollGoal(this, 0.7D));
		this.goalSelector.addGoal(7, new LookAtPlayerGoal(this, Player.class, 6.0F));
		this.goalSelector.addGoal(8, new RandomLookAroundGoal(this));

		this.goalSelector.addGoal(1, new FloatGoal(this));
		this.goalSelector.addGoal(2, new SitWhenOrderedToGoal(this));
		this.goalSelector.addGoal(4, new LeapAtTargetGoal(this, 0.4F));
		this.goalSelector.addGoal(5, new MeleeAttackGoal(this, 1.0D, true));
		this.goalSelector.addGoal(6, new FollowOwnerGoal(this, 1.0D, 10.0F, 2.0F, false));
		this.goalSelector.addGoal(7, new BreedGoal(this, 1.0D));
		this.goalSelector.addGoal(8, new WaterAvoidingRandomStrollGoal(this, 1.0D));
//		this.goalSelector.addGoal(9, new BegGoal(this, 8.0F));
		this.goalSelector.addGoal(10, new LookAtPlayerGoal(this, Player.class, 8.0F));
		this.goalSelector.addGoal(10, new RandomLookAroundGoal(this));
		this.targetSelector.addGoal(1, new OwnerHurtByTargetGoal(this));
		this.targetSelector.addGoal(2, new OwnerHurtTargetGoal(this));
		this.targetSelector.addGoal(3, (new HurtByTargetGoal(this)).setAlertOthers());
//		this.targetSelector.addGoal(4, new NearestAttackableTargetGoal<>(this, PlayerEntity.class, 10, true, false, this::isAngryAt));
		this.targetSelector.addGoal(5, new NonTameRandomTargetGoal<>(this, Animal.class, false, Wolf.PREY_SELECTOR));
		this.targetSelector.addGoal(6, new NonTameRandomTargetGoal<>(this, Turtle.class, false, Turtle.BABY_ON_LAND_SELECTOR));
		this.targetSelector.addGoal(7, new NearestAttackableTargetGoal<>(this, AbstractSkeleton.class, false));
//		this.targetSelector.addGoal(8, new ResetAngerGoal<>(this, true));
	}

	protected void applyRandomAttributes()
	{
		this.getAttribute(Attributes.MAX_HEALTH).setBaseValue((double) this.getModifiedMaxHealth() + 5F);
		this.getAttribute(Attributes.MOVEMENT_SPEED).setBaseValue(this.getModifiedMovementSpeed() + 0.05F);
		this.getAttribute(Attributes.JUMP_STRENGTH).setBaseValue(this.getModifiedJumpStrength() + 0.2F);

	}

	@Override
	protected void defineSynchedData()
	{
		super.defineSynchedData();
		this.getEntityData().define(SPEED_BOOST, (byte) 0);
		this.getEntityData().define(GROWTH, (byte) 0);
		this.getEntityData().define(ATTACK_BOOST, (byte) 0);

	}

	@Override
	public void addAdditionalSaveData(CompoundTag compound)
	{
		super.addAdditionalSaveData(compound);
		compound.putByte("speed_boost", getSpeedBoost());
		compound.putByte("growth", getGrowth());
		compound.putByte("attack_boost", getAttackBoost());

	}

	@Override
	public void readAdditionalSaveData(CompoundTag compound)
	{
		super.readAdditionalSaveData(compound);
		this.setGrowth(compound.getByte("growth"));
		this.setSpeedBoost(compound.getByte("speed_boost"));
		this.setAttackBoost(compound.getByte("attack_boost"));

	}

	protected float getModifiedMaxHealth()
	{
		return 15.0F + (float) this.random.nextInt(8) + (float) this.random.nextInt(9);
	}

	protected double getModifiedJumpStrength()
	{
		return (double) 0.4F + this.random.nextDouble() * 0.2D + this.random.nextDouble() * 0.2D
				+ this.random.nextDouble() * 0.2D;
	}

	protected double getModifiedMovementSpeed()
	{
		return ((double) 0.45F + this.random.nextDouble() * 0.3D + this.random.nextDouble() * 0.3D
				+ this.random.nextDouble() * 0.3D) * 0.25D;
	}

	private boolean wasRidden = false;
	
	@Override
	public void aiStep()
	{
		super.aiStep();

		byte g = getGrowth();
		byte newG = (byte) Math.min(100, (int) (tickCount / 24000F * 12.5F));

		if (newG > g)
			setGrowth(newG);

		if (!this.level.isClientSide && this.isAlive())
		{
			if (this.random.nextInt(900) == 0 && this.deathTime == 0)
			{
				this.heal(1.0F);
			}
		}
		if(this.isInSittingPose())
		{
			this.setTarget(null);
			this.setDeltaMovement(0, this.getDeltaMovement().y, 0);
			this.getNavigation().stop();
		}
		
		if(wasRidden != isVehicle())
		{
			updateStats();
			wasRidden = isVehicle();
		}
	}

	public byte getGrowth()
	{
		return this.entityData.get(GROWTH);
	}

	public byte getSpeedBoost()
	{
		return this.entityData.get(SPEED_BOOST);
	}

	public byte getAttackBoost()
	{
		return this.entityData.get(ATTACK_BOOST);
	}

	private void setGrowth(byte value)
	{

		this.entityData.set(GROWTH, value);
		updateStats();
	}

	private void setSpeedBoost(byte value)
	{
		this.entityData.set(SPEED_BOOST, value);
		updateStats();
	}

	private void setAttackBoost(byte value)
	{
		this.entityData.set(ATTACK_BOOST, value);
		updateStats();
	}

	/**
	 * @return s Vec3d with xyz_scale, y_height, thickness
	 */
	public Vec3 getSizeModifier()
	{
		byte g = getGrowth();

		float xyz_scale = 0.4F + 0.9F * (g * 0.01F);
		float y_height = 1F, thickness = 0.95F; // thickness as in the body and parts get thiker but not larger. so a

		byte speed = getSpeedBoost();
		byte atk = getAttackBoost();

		// thin 3x3 picel leg would get upscalled to 5x5 or something like that
		// test if thicknes is just xz scale
		y_height += speed * 0.01F * 0.1F;
		thickness -= speed * 0.01F * 0.01F;

		thickness += atk * 0.01F * 0.1F;
		xyz_scale += atk * 0.01F * 0.3F;
		y_height -= atk * 0.01F * 0.01F;

		// xyz_scale: 0.1F to 1.5F;1.5F
		// y_height: 0.8F to 1.5F;1.3F
		// thickness: 0.8F to 1.45;1.3F

		if (isInSittingPose())
		{
			xyz_scale += (Math.sin(System.currentTimeMillis() / 733D) + 1D) * 0.01;
		}

		return new Vec3(xyz_scale, y_height, thickness);
	}

	private float getHealthModifier()
	{
		Vec3 vec = getSizeModifier();
		double f = (vec.x * vec.y * vec.x * vec.z * vec.x * vec.z) / (0.8 * 0.8 * 0.8);
		return (float) (0.2 + f);
	}

	public void updateStats()
	{
		float f = getHealthModifier() * 20F;
		this.getAttribute(Attributes.MAX_HEALTH).setBaseValue(f);
		this.setHealth(f);

		float attack_bonus = getAttackBoost() / 100F * 20F;
		AttributeModifier attack_boost = new AttributeModifier(ATTACK_BOOST_ID, "lvl attack boost", attack_bonus, Operation.ADDITION);
		this.getAttribute(Attributes.ATTACK_DAMAGE).removeModifier(attack_boost);
		this.getAttribute(Attributes.ATTACK_DAMAGE).addTransientModifier(attack_boost);

		float speed_bonus = getSpeedBoost() / 100F * 0.8F;
		
		if(!isVehicle())
			speed_bonus *= 0.1F;
		
		AttributeModifier speed_boost = new AttributeModifier(SPEED_BOOST_ID, "lvl speed boost", speed_bonus, Operation.ADDITION);
		this.getAttribute(Attributes.MOVEMENT_SPEED).removeModifier(speed_boost);
		this.getAttribute(Attributes.MOVEMENT_SPEED).addTransientModifier(speed_boost);
		
		f = this.getGrowth() / 100F;
		AttributeModifier AGE_BOOST = new AttributeModifier(AGE_ID, "growth attack boost", f-1F, Operation.MULTIPLY_BASE);
		this.getAttribute(Attributes.ATTACK_DAMAGE).removeModifier(AGE_BOOST);
		this.getAttribute(Attributes.ATTACK_DAMAGE).addTransientModifier(AGE_BOOST);
		this.getAttribute(Attributes.MOVEMENT_SPEED).removeModifier(AGE_BOOST);
		this.getAttribute(Attributes.MOVEMENT_SPEED).addTransientModifier(AGE_BOOST);
		
		
//		print(this.getAttribute(Attributes.MOVEMENT_SPEED));
	}
	
	private static void print(AttributeInstance attr)
	{
		double base = attr.getBaseValue();
		StringBuilder build = new StringBuilder();
		build.append("base ").append(base);
		for(AttributeModifier am : attr.getModifiers())
		{
			build.append(" ").append(am.getOperation().toString()).append(" x ").append(am.getAmount());
		}		
		build.append(" -> ").append(attr.getValue());
		System.out.println(build.toString());
	}

	public void tick()
	{
//		this.stepHeight = 1.0F;
		if (tickCount == 0)
		{
			applyRandomAttributes();
		}
		this.maxUpStep = Math.max(0.5F, (float) getPassengersRidingOffset());
		
		if(getWatchableBoolean(BOOL_IS_FAINTED))
		{
			this.setInSittingPose(true);
		}
		
		
		super.tick();
	}

	public float getTailRotation()
	{
		float f = (this.getMaxHealth() - this.getHealth()) / this.getMaxHealth();
		return 0.55F - (f * 0.4F * (float) Math.PI);
	}

	@Override
	public double getPassengersRidingOffset()
	{
		Vec3 size = getSizeModifier();
		return super.getPassengersRidingOffset() * 1.1 * size.x * size.y - 0.0625F * 3;
	}

	/**
	 * can be saddled
	 */
	@Override
	public boolean isSaddleable()
	{

		return super.isSaddleable() && isOldEnough();
	}

	private boolean calculating = false;
	
	
	public boolean isOldEnough()
	{
		return getGrowth() >= 90;
	}
	
	
	@Override
	public boolean hurt(DamageSource source, float amount)
	{
		if(!level.isClientSide)
		{
			if (getWatchableBoolean(BOOL_IS_FAINTED) && source.isBypassInvul())
			{
				setWatchableBoolean(BOOL_IS_FAINTED, false);
			}
	
			boolean notDead = isDeadOrDying();
			calculating = true;
			boolean b = super.hurt(source, amount);
			calculating = false;
			if (!notDead && isDeadOrDying())
			{
				if (!source.isBypassInvul())
				{
					setWatchableBoolean(BOOL_IS_FAINTED, true);
					boolean h = getWatchableBoolean(BOOL_IS_FAINTED);
					b = false;
				}
				else
				{
					die(source);
				}
			}
			return b;
		}
		else
		{
			return super.hurt(source, amount);
		}
	}

	@Override
	public boolean isDeadOrDying()
	{
		if (getWatchableBoolean(BOOL_IS_FAINTED))
		{
			return true;
		}
		return super.isDeadOrDying();
	}

	@Override
	protected void tickDeath()
	{
		if (getWatchableBoolean(BOOL_IS_FAINTED))
		{
			this.deathTime = 0;
			return;
		}
		super.tickDeath();
	}
	
	@Override
	public void die(DamageSource cause)
	{
		if(calculating)
			return;
		super.die(cause);
	}

	@Override
	public void heal(float healAmount)
	{
		if(getHealth()==0 && getWatchableBoolean(BOOL_IS_FAINTED) && healAmount> 0)
		{
			setHealth(0.01F);
		}
		super.heal(healAmount);
		
		if (healAmount > 0 && getWatchableBoolean(BOOL_IS_FAINTED) && getHealth() > 0F)
		{
			setWatchableBoolean(BOOL_IS_FAINTED, false);
			setInSittingPose(false);
		}
	}

	@Override
	public void spawnAnim()
	{
		if (this.level.isClientSide)
		{
			for (int i = 0; i < 20; ++i)
			{
				double d0 = this.random.nextGaussian() * 0.02D;
				double d1 = this.random.nextGaussian() * 0.02D;
				double d2 = this.random.nextGaussian() * 0.02D;
				double d3 = 10.0D;
				this.level.addParticle(ParticleTypes.POOF, this.getX(1.0D) - d0 * d3, this.getRandomY() - d1 * d3, this.getRandomZ(1.0D) - d2 * d3, d0, d1, d2);
			}
		} 
		else
		{
			this.level.broadcastEntityEvent(this, (byte) 20);
		}

	}

	@Override
	@OnlyIn(Dist.CLIENT)
	public void handleEntityEvent(byte id)
	{
		if (id == 20)
		{
			this.spawnAnim();
		} 
		else
		{
			super.handleEntityEvent(id);
		}

	}

	@Override
	public InteractionResult mobInteract(Player pl, InteractionHand hand)
	{
		ItemStack stack = pl.getItemInHand(InteractionHand.MAIN_HAND);
		if(!stack.isEmpty() && stack.getItem() == FoodItems.astrofood4 && this.getGrowth() < 100)
		{
			if(!level.isClientSide)
			{
				byte g = (byte) Math.min(100, this.getGrowth()+5);
				this.setGrowth(g);
			}
			return InteractionResult.sidedSuccess(level.isClientSide);
		}
		else if(!stack.isEmpty() && stack.getItem() == FoodItems.astrofood3 && this.getSpeedBoost() < 20)
		{
			if(this.isOldEnough())
			{
				if(!level.isClientSide)
				{
					byte g = (byte) Math.min(100, this.getSpeedBoost()+1);
					this.setSpeedBoost(g);
				}
				return InteractionResult.sidedSuccess(level.isClientSide);
			}
			else
			{
				if(!level.isClientSide)
					pl.sendMessage(new TranslatableComponent("entity.futurepack.alpha_jawaul.not_old_enough"), getUUID());
				return InteractionResult.FAIL;
			}
		}
		else if(!stack.isEmpty() && stack.getItem() == FoodItems.astrofood1 && this.getAttackBoost() < 20)
		{
			if(this.isOldEnough())
			{
				if(!level.isClientSide)
				{
					byte g = (byte) Math.min(100, this.getAttackBoost()+1);
					this.setAttackBoost(g);
				}
				return InteractionResult.sidedSuccess(level.isClientSide);
			}
			else
			{
				if(!level.isClientSide)
					pl.sendMessage(new TranslatableComponent("entity.futurepack.alpha_jawaul.not_old_enough"), getUUID());
				return InteractionResult.FAIL;
			}
		}
		else  if(!stack.isEmpty() && stack.getItem() == MiscItems.hambone  && this.isAlive() && !getWatchableBoolean(BOOL_IS_FAINTED))
		{
			if(!level.isClientSide)
				this.setInSittingPose(!this.isInSittingPose());
			return InteractionResult.sidedSuccess(level.isClientSide) ;
		}
		else if(!stack.isEmpty() && stack.getItem() == Blocks.CHEST.asItem() && !hasChest() && isSaddled() && isTame())
		{
			if(!level.isClientSide)
			{
				this.horseChest.setItem(1, stack.split(1));
			}
			return InteractionResult.sidedSuccess(level.isClientSide);
		}
		else if (isSaddled() && isTame() && stack.isEmpty())
		{
			this.mountTo(pl);
			return InteractionResult.sidedSuccess(this.level.isClientSide);
		}
		else
		{
			InteractionResult result = super.mobInteract(pl, hand);
		
			return result;
		}
	}

	public static EntityAlphaJawaul createFromJawaul(EntityJawaul target)
	{
		if (target.isAlpha())
		{
			EntityAlphaJawaul jawaul = new EntityAlphaJawaul(FPEntitys.ALPHA_JAWAUL, target.getCommandSenderWorld());
			jawaul.absMoveTo(target.getX(), target.getY(), target.getZ(), target.getYRot(),
					target.getXRot());
			return jawaul;
		} 
		else
		{
			return null;
		}
	}
}
