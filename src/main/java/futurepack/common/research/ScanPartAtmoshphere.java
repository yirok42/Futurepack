package futurepack.common.research;

import java.util.Collection;

import futurepack.api.ParentCoords;
import futurepack.api.interfaces.IBlockSelector;
import futurepack.api.interfaces.IChunkAtmosphere;
import futurepack.api.interfaces.IPlanet;
import futurepack.api.interfaces.IScanPart;
import futurepack.common.FPBlockSelector;
import futurepack.common.spaceships.FPPlanetRegistry;
import futurepack.world.dimensions.atmosphere.AtmosphereManager;
import net.minecraft.core.BlockPos;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.chunk.LevelChunk;
import net.minecraft.world.level.material.Material;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraftforge.common.util.LazyOptional;

public class ScanPartAtmoshphere implements IScanPart
{

	@Override
	public Component doBlock(Level w, BlockPos pos, boolean inGUI, BlockHitResult res)
	{
		IPlanet pl = FPPlanetRegistry.instance.getPlanetSafe(w.dimension());
		
		if(pl.hasBreathableAtmosphere())
		{
			return new TranslatableComponent("chat.escanner.athmosphere.breathable");
		}
		BlockPos start = pos.relative(res.getDirection());
		FPBlockSelector sel = new FPBlockSelector(w, new SelectAir(pos, 16));
		sel.selectBlocks(start);
		double air = 0;
		double maxAir = 0;
		
		Collection<ParentCoords> list = sel.getAllBlocks();
		for(BlockPos p : list)
		{
			LevelChunk c = w.getChunkAt(p);
			LazyOptional<IChunkAtmosphere> opt = c.getCapability(AtmosphereManager.cap_ATMOSPHERE, res.getDirection());
			if(opt.isPresent())
			{
				IChunkAtmosphere atm = opt.orElseThrow(NullPointerException::new);
				air += atm.getAirAt(p.getX() & 15, p.getY() & 255, p.getZ() & 15);
				maxAir += atm.getMaxAir();
				
			}
		}
		
		float filled = (float) (air / maxAir) * 100;
		
		int pec = (int)filled;
		int komma = (int)(filled * 100) - (pec*100);
		
		return new TranslatableComponent("chat.escanner.athmosphere.filled", pec, komma, time((int) air));
	}

	@Override
	public Component doEntity(Level w, LivingEntity e, boolean inGUI)
	{
		return null;
	}
	
	private String time(int ticks)
	{
		int seconds = (ticks / 20);
		int minuits = (seconds / 60);
		int hour = minuits / 60;
		minuits %= 60;
		seconds %= 60;
		
		return String.format("%sh %smin %ss", hour, minuits, seconds);
	}
	
	private static class SelectAir implements IBlockSelector
	{
		private final int maxDis;
		private final BlockPos start;
		
		public SelectAir(BlockPos start, int maxDis)
		{
			this.start = start;
			this.maxDis = maxDis*maxDis;
		}
		
		@Override
		public boolean isValidBlock(Level w, BlockPos pos, Material m, boolean diagonal, ParentCoords parent)
		{
			return w.isEmptyBlock(pos);
		}

		@Override
		public boolean canContinue(Level w, BlockPos pos, Material m, boolean diagonal, ParentCoords parent)
		{
			return start.distSqr(pos) <= maxDis;
		}
		
	}

}
