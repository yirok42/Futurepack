package futurepack.common.research;

import java.util.Map;
import java.util.TreeMap;

import futurepack.api.interfaces.IScanPart;
import net.minecraft.core.BlockPos;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.level.ChunkPos;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.chunk.LevelChunk;
import net.minecraft.world.phys.BlockHitResult;

public class ScanPartMagnetism implements IScanPart
{

	@Override
	public Component doBlock(Level w, BlockPos pos, boolean inGUI, BlockHitResult res)
	{
		long time = System.currentTimeMillis();
		double mag = getMagnetIntensity(w, pos, 4 * 16);//max value supported 64
		time = System.currentTimeMillis() - time;
		if(mag>0)
		{
			return new TranslatableComponent("chat.escanner.magnetism", (float)mag);
		}
		return null;
	}

	@Override
	public Component doEntity(Level w, LivingEntity e, boolean inGUI)
	{
		return null;
	}
	
	public static double getMagnetIntensity(Level w, BlockPos pos, int range)
	{
		double max = 0;
		BlockPos.MutableBlockPos xyz = new BlockPos.MutableBlockPos().set(pos);
		Map<Long, LevelChunk> m = new TreeMap<Long, LevelChunk>();
		
		for(int x=pos.getX()-range;x<pos.getX()+range+1;x++)
		{
			for(int z=pos.getZ()-range;z<pos.getZ()+range+1;z++)
			{
				LevelChunk c = m.computeIfAbsent(ChunkPos.asLong(x>>4, z>>4), l -> w.getChunk(ChunkPos.getX(l), ChunkPos.getZ(l)));
				for(int y=pos.getY()-range;y<pos.getY()+range+1;y++)
				{
					xyz.set(x, y, z);
					BlockState b = c.getBlockState(xyz);
					double i = MagnetismManager.getMagnetismOfBlock(b.getBlock());
					if(i>0)
					{
						double dis = pos.distToCenterSqr(x, y, z);
						max = Math.max(max, (i*i) / dis);
					}
				}
			}
		}
		return Math.sqrt(max);
	}
}
