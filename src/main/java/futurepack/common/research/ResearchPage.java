package futurepack.common.research;

import java.util.ArrayList;

import javax.annotation.Nullable;

import futurepack.api.Constants;
import futurepack.api.event.ResearchPageRegisterEvent;
import futurepack.api.interfaces.IGuiRenderable;
import futurepack.common.gui.RenderableAspect;
import futurepack.common.gui.RenderableItem;
import futurepack.common.item.tools.ToolItems;
import futurepack.depend.api.EnumAspects;
import net.minecraft.client.resources.language.I18n;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.block.Blocks;
import net.minecraftforge.common.MinecraftForge;

public class ResearchPage
{
	static
	{
		init(); //So this is present all the time
	}
	
	public static ResearchPage[] pages;
	public static int size;
	
	private static int superid=0;
	
	public static ResearchPage base;
	public static ResearchPage story;
	public static ResearchPage chips;
	//public static ResearchPage deco;
	public static ResearchPage logistic;
	//public static ResearchPage energy;
	public static ResearchPage production;
	public static ResearchPage space;
	public static ResearchPage tools;
	
	public static ResearchPage getPageSave(String name)
	{
		if(name==null)
		{
			return pages[0];
		}
			
		for(int i=0;i<size;i++)
		{
			if(name.equals(pages[i].name))
			{
				return pages[i];
			}
		}
		ResearchPage page = new ResearchPage(name);
		page.setIcon(new ItemStack(Blocks.DIRT));
		
		return page;
		
	}
	
	public static ResearchPage getPage(int id)
	{
		return pages[id];
	}
	
	/**
	 * Used in {@link ResearchLoader} to make sure, <i>/fp research reload</i>, works.
	 */
	public static void init() 
	{
		size = 0;
		superid = 0;
		pages = new ResearchPage[18];
		base = new ResearchPage("base").setIcon(new ItemStack(ToolItems.scrench)).setVisiblity(EnumPageVisibility.HIDDEN);
		story = new ResearchPage("story").setIcon(new ItemStack(Items.WRITABLE_BOOK));
		//energy = new ResearchPage("energy").setIcon(EnumAspects.EN_ERZEUGUNG);
		production = new ResearchPage("production").setIcon(EnumAspects.PRODUCTION).setVisiblity(EnumPageVisibility.HIDDEN);
		logistic = new ResearchPage("logistic").setIcon(EnumAspects.ITEMLOG).setVisiblity(EnumPageVisibility.HIDDEN);
		chips = new ResearchPage("chips").setIcon(EnumAspects.CHIP_ASSEMBLY).setVisiblity(EnumPageVisibility.HIDDEN);
		//deco = new ResearchPage("deco").setIcon(new ItemStack(FPItems.tools, 1, 27));
		space = new ResearchPage("space").setIcon(EnumAspects.RAUMFAHRT).setVisiblity(EnumPageVisibility.HIDDEN);
		tools = new ResearchPage("tools").setIcon(EnumAspects.WERKZEUGE).setVisiblity(EnumPageVisibility.HIDDEN);
		
		space.bg = new ResourceLocation(Constants.MOD_ID, "textures/gui/research_bg_2.jpg");
		space.bgh = 448;
		space.bgw = 813;
		
		story.bg = new ResourceLocation(Constants.MOD_ID, "textures/gui/research_bg_3.jpg");
		
		
		MinecraftForge.EVENT_BUS.post(new ResearchPageRegisterEvent());
	}
	
	private ArrayList<Research> childs = new ArrayList<Research>();
	private String name;
	public final int id;
	private IGuiRenderable icon;
	private EnumPageVisibility visiblity;
	
	int bgw=980,bgh=540;//texture is 1920 x 1080 so 
	ResourceLocation bg = new ResourceLocation(Constants.MOD_ID, "textures/gui/research_bg.jpg");
	
	public ResearchPage(String name)
	{
		id = superid++;
		size = superid;
		pages[id] = this;
		this.name = name;
		visiblity = EnumPageVisibility.ALWAYS;
	}
	
	////@ TODO: OnlyIn(Dist.CLIENT)
	public ResearchPage setIcon(ItemStack it)
	{
		this.icon = new RenderableItem(it);
		return this;
	}
	
	public ResearchPage setIcon(EnumAspects asp)
	{
		this.icon = new RenderableAspect(asp);
		return this;
	}
	
	public ResearchPage setVisiblity(EnumPageVisibility v)
	{
		this.visiblity = v;
		return this;
	}
	
	public void registerResearch(Research res)
	{
		childs.add(res);
	}
	
	public String getTranslationKey()
	{
		return "research.page."+name;
	}
	
	//@ TODO: OnlyIn(Dist.CLIENT)
	public String getLocalizedName()
	{
		return I18n.get(getTranslationKey()+".name");
	}

	public ResourceLocation getBackground()
	{
		return bg;
	}

	public ArrayList<Research> getEntries()
	{
		return childs;
	}
	
	public int getWidth()
	{
		return bgw;
	}
	
	public int getHeight()
	{
		return bgh;
	}

	//@ TODO: OnlyIn(Dist.CLIENT)
	public IGuiRenderable getIcon()
	{
		return icon;
	}
	
	public static enum EnumPageVisibility
	{
		
		ALWAYS,
		/**
		 * Only whne someone already has some researches on this page it will can shown
		 */
		HIDDEN;
	}

	public boolean isVisible(@Nullable CustomPlayerData custom)
	{
		if(visiblity == EnumPageVisibility.ALWAYS)
			return true;
		else if(visiblity == EnumPageVisibility.HIDDEN)
		{
			if(custom!=null)
			{
				for(Research r : childs)
				{
					if(custom.hasResearch(r))
						return true;
				}
			}
		}
		return false;
	}

	public EnumPageVisibility getVisibility()
	{
		return visiblity;
	}
}
