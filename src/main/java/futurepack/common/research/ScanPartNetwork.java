package futurepack.common.research;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import futurepack.api.PacketBase;
import futurepack.api.interfaces.INetworkUser;
import futurepack.api.interfaces.IScanPart;
import futurepack.api.interfaces.tilentity.ITileNetwork;
import futurepack.common.network.NetworkManager;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Registry;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.Style;
import net.minecraft.network.chat.TextComponent;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.phys.BlockHitResult;

public class ScanPartNetwork implements IScanPart 
{

	@Override
	public Component doBlock(Level w, BlockPos pos, boolean inGUI, BlockHitResult res) 
	{
		BlockEntity[] t = {null};
		w.getServer().submit(() -> {
			t[0] = w.getBlockEntity(pos);
		}).join();
		BlockEntity tile = t[0];
		if(tile instanceof ITileNetwork netty)
		{
			if(netty.isNetworkAble())
			{
				Set<ITileNetwork> nodes = NetworkManager.pingNetwork(tile);
				
				TextComponent main = new TextComponent("Network Nodes:\n");
				nodes.stream()
						.filter(p -> !p.isWire())
						.map(p ->  getString(w, (BlockEntity)p))
						.forEach(c -> main.append(c).append("\n"));
				
				return main;
			}
		}
		return null;
	}
	
	private static Component getString(Level w, BlockEntity t)
	{
		var block_name = t.getBlockState().getBlock().getName().setStyle(Style.EMPTY.withBold(true));
		var dimension_name = (t.getLevel() != w ?  (" in " + w.getServer().registryAccess().registryOrThrow(Registry.DIMENSION_TYPE_REGISTRY).getKey(t.getLevel().dimensionType())) : null);
		
		var result =  block_name.append(new TextComponent(" at [" + t.getBlockPos().toShortString() + "]").setStyle(Style.EMPTY.withBold(false)));
				
		if(dimension_name!=null)
		{
			result = result.append(new TextComponent(dimension_name).setStyle(Style.EMPTY.withBold(false)));
		}
				
		return result;
	}

	@Override
	public Component doEntity(Level w, LivingEntity e, boolean inGUI) 
	{
		return null;
	}

}
