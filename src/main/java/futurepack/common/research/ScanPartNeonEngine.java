package futurepack.common.research;

import java.util.Arrays;
import java.util.Collection;
import java.util.concurrent.Callable;

import futurepack.api.capabilities.CapabilityNeon;
import futurepack.api.capabilities.IEnergyStorageBase.EnumEnergyMode;
import futurepack.api.capabilities.INeonEnergyStorage;
import futurepack.api.interfaces.IScanPart;
import futurepack.common.block.modification.TileEntityModificationBase;
import net.minecraft.ChatFormatting;
import net.minecraft.core.BlockPos;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TextComponent;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraftforge.common.util.LazyOptional;

public class ScanPartNeonEngine implements IScanPart
{

	@Override
	public Callable<Collection<Component>> doBlockMulti(Level w, BlockPos pos, boolean inGUI, BlockHitResult res)
	{
		return () -> Arrays.asList(doBlocks(w, pos, inGUI, res));
		
	}

	private Component[] doBlocks(Level w, BlockPos pos, boolean inGUI, BlockHitResult res)
	{
		BlockEntity[] t = {null};
		w.getServer().submit(() -> {
			t[0] = w.getBlockEntity(pos);
		}).join();
		BlockEntity tile = t[0];
		if(tile!=null)
		{
			Component[] result = new Component[] {null,null};
			
			INeonEnergyStorage store = null;
			LazyOptional<INeonEnergyStorage> opt = tile.getCapability(CapabilityNeon.cap_NEON, res.getDirection());
			
			if(opt==null)
			{
				throw new NullPointerException("TileEntity " + tile + " returned null for NEON capability!");
			}
			
			if(!opt.isPresent())
			{
				opt = tile.getCapability(CapabilityNeon.cap_NEON, res.getDirection());
				if(!opt.isPresent())
				{
					return new Component[0];
				}
			}
			
			store = opt.orElseThrow(NullPointerException::new);
			if(store !=null)
			{
				float i = store.get();
				result[0] = new TextComponent( (inGUI ? ChatFormatting.DARK_GREEN : ChatFormatting.GREEN)  + "Power: " + ChatFormatting.RESET + i + " NE");
			}
			
			if(tile instanceof TileEntityModificationBase)
			{
				TileEntityModificationBase mod = (TileEntityModificationBase) tile;
				float ticksPerSecond = mod.getPureRam();
				float energiePerTick = mod.getDefaultPowerUsage();
				boolean working = mod.isWorking();
				float energyPerSecond = ticksPerSecond * energiePerTick;
				String energyType = mod.getEnergyType() == EnumEnergyMode.USE ? "Needs " : (mod.getEnergyType() == EnumEnergyMode.PRODUCE ? "Produces " : "Does something with");
				result[1] = new TextComponent( (inGUI ? ChatFormatting.DARK_GRAY : ChatFormatting.GRAY)  + energyType + energyPerSecond + " NE/s and is " + (working ? "working" : "not working"));
			}
			return result;
		}
		return new Component[0];
	}
	
	@Override
	public Component doEntity(Level w, LivingEntity e, boolean inGUI)
	{
		return null;
	}

	@Override
	public Component doBlock(Level w, BlockPos pos, boolean inGUI, BlockHitResult res) 
	{
		return null;
	}

}
