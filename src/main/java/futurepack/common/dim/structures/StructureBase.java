package futurepack.common.dim.structures;

import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.annotation.Nullable;

import futurepack.common.FPLog;
import futurepack.common.dim.structures.generation.IDungeonEventListener;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LevelWriter;
import net.minecraft.world.level.ServerLevelAccessor;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.levelgen.Heightmap.Types;
import net.minecraft.world.level.levelgen.structure.BoundingBox;
import net.minecraft.world.level.storage.loot.LootTables;
import net.minecraft.world.phys.AABB;

public class StructureBase
{
	protected final BlockState[][][] blocks;
	protected DungeonChest[] chests=null;
	protected OpenDoor[] doors;
	public boolean hide = true;
	public final String name;
	protected Map<BlockPos, CompoundTag> tilEntityData;
	
	public StructureBase(@Nullable String name, BlockState[][][] states)
	{
		blocks = states;
		this.name = name;
	}
	
	public StructureBase(StructureBase base)
	{
		blocks = base.blocks;
		chests = base.chests;
		doors = base.doors;
		name = base.name;
	}
	
	public int getWidth()
	{
		return blocks.length;
	}
	
	public int getHeight()
	{
		return blocks[0].length;
	}
	
	public int getDepth()
	{
		return blocks[0][0].length;
	}
	
	/*
	 * n * 3 int array n is the count of the chest and 3 the dimensions
	 */
	public void setChests(DungeonChest[] pos)
	{
		chests = pos;
	}
	

	public void setTileData(Map<BlockPos, CompoundTag> tilEntityData)
	{
		this.tilEntityData = tilEntityData;
	}
	
	public void setOpenDoors(OpenDoor[] doorA)
	{
		for(int i=0;i<doorA.length;i++)
		{
			for(int j=i;j<doorA.length;j++)
			{
				if(i!=j && doorA[i].equals(doorA[j]))
				{
					throw new IllegalArgumentException("Contains door entries with are identical");
				}
			}
		}
		
		doors = doorA;		
	}
	
	public OpenDoor[] getRawDoors()
	{
		return doors;
	}
	
	public void generate(Level w, BlockPos start, List<BoundingBox> spawnedRooms)
	{
//		for(int x=0;x<getWidth();x++)
//		{
//			for(int y=0;y<getHeight();y++)
//			{
//				for(int z=0;z<getDepth();z++)
//				{
//					BlockPos xyz = start.add(x,y,z);
//					w.setBlockState(xyz, Blocks.bookshelf.getDefaultState(), 2);			
//				}
//			}
//		}
		spawnedRooms.add(getBoundingBox(start));
		
		generateBase(w, start);
		
		if(tilEntityData!=null)
		{
			BlockPos.MutableBlockPos mut = new BlockPos.MutableBlockPos();
			tilEntityData.forEach((pos,tag) -> {
				mut.set(start);
				mut.move(pos);
				BlockEntity tile = w.getBlockEntity(mut);
				if(tile!=null)
				{
					tile.load(tag);
				}
			});
		}
		
		
		final int h =  getHeight();
		final int ww = getWidth();
		final int d = getDepth();
		
		if(hide)
		{
			BlockPos top = start.offset(0, h, 0);
			BlockPos xz = w.getHeightmapPos(Types.WORLD_SURFACE, top);
			boolean tooBig = false;
			if(top.getY()>xz.getY())
			{
				tooBig = true;
			}
			else
			{
				top = start.offset(ww, h, 0);
				xz = w.getHeightmapPos(Types.WORLD_SURFACE, top);
				if(top.getY()>xz.getY())
				{
					tooBig = true;
				}
				else
				{
					top = start.offset(0, h, d);
					xz = w.getHeightmapPos(Types.WORLD_SURFACE, top);
					if(top.getY()>xz.getY())
					{
						tooBig = true;
					}
					else
					{
						top = start.offset(ww, h, d);
						xz = w.getHeightmapPos(Types.WORLD_SURFACE, top);
						if(top.getY()>xz.getY())
						{
							tooBig = true;
						}
					}
				}
			}
			if(tooBig)
				hideWithBlocks(w, start);
		}
		
		AABB bb = new AABB(start, start.offset(ww, h, d));
		List<ItemEntity> l = w.getEntitiesOfClass(ItemEntity.class, bb);
		if(!l.isEmpty())
		{
			FPLog.logger.debug("Generatring Dungeon Strulkture " + name + " dropped items");
		}
		l.forEach(ItemEntity::discard);
	}
	
	public void generateBase(LevelWriter w, BlockPos start)
	{
		final int h =  getHeight();
		final int ww = getWidth();
		final int d = getDepth();
		final int x0 = start.getX(), y0 = start.getY(), z0 = start.getZ();
		
		BlockPos.MutableBlockPos mut = new BlockPos.MutableBlockPos();
		for(int y=0;y<h;y++)
		{		
			for(int x=0;x<ww;x++)
			{
				for(int z=0;z<d;z++)
				{
					if(blocks[x][y][z]!=null)
					{
						mut.set(x0+x, y0+y, z0+z);
						w.setBlock(mut, blocks[x][y][z], 2);
					}				
				}
			}
		}
	}
	
	private void hideWithBlocks(Level w, BlockPos start)
	{

	}
	
	private void fillDownExcept(Level w, BlockPos pos, BlockState state, Direction face)
	{
		BlockPos xyz = pos;
		if(w.isEmptyBlock(xyz))
		{
			while(w.isEmptyBlock(xyz))
			{
				w.setBlock(xyz, state, 2);
				xyz = xyz.below();
			}
			if(Direction.NORTH != face && w.isEmptyBlock(pos.north()) && w.isEmptyBlock(pos.north().below()))
				fillDownExcept(w, pos.below(1).north(), state, face);
			if(Direction.SOUTH != face && w.isEmptyBlock(pos.south()) && w.isEmptyBlock(pos.south().below()))
				fillDownExcept(w, pos.below(1).south(), state, face);
			if(Direction.WEST  != face && w.isEmptyBlock(pos.west( )) && w.isEmptyBlock(pos.west( ).below()))
				fillDownExcept(w, pos.below(1).west( ), state, face);
			if(Direction.EAST  != face && w.isEmptyBlock(pos.east( )) && w.isEmptyBlock(pos.east( ).below()))
				fillDownExcept(w, pos.below(1).east( ), state, face);
		}
	}
	
	
	public void addChestContentBase(ServerLevelAccessor w, BlockPos start, Random rand, CompoundTag extraData, LootTables manager)
	{
		for(DungeonChest ints : chests)
		{
			ints.genLoot(w, rand, start, extraData, manager);
		}
	}

	public BoundingBox getBoundingBox(BlockPos start)
	{
		return BoundingBox.fromCorners(start, start.offset(getWidth()-1,getHeight()-1,getDepth()-1));
	}
	
	@Override
	public String toString()
	{
		return String.format("StructureBase(name=%s,%s,%s,%s|Chest:%s|Doors:%s)",name, getWidth(),getHeight(),getDepth(), chests==null?0 : chests.length, doors==null?0:doors.length);
	}
	
	public BlockState[][][] getBlocks()
	{
		return this.blocks;
	}
	
	public IDungeonEventListener getEventListener()
	{
		return null;
	}
}
