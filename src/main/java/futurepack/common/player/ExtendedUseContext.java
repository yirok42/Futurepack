package futurepack.common.player;

import net.minecraft.world.InteractionHand;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.context.UseOnContext;
import net.minecraft.world.level.Level;
import net.minecraft.world.phys.BlockHitResult;

public class ExtendedUseContext extends UseOnContext 
{
	//this is so I can access the constructor 
	protected ExtendedUseContext(Level worldIn, Player player, InteractionHand handIn, ItemStack heldItem, BlockHitResult rayTraceResultIn) 
	{
		super(worldIn, player, handIn, heldItem, rayTraceResultIn);
	}

}
