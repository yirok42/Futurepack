package futurepack.common.item;

import java.util.function.Supplier;

import net.minecraft.util.LazyLoadedValue;
import net.minecraft.world.item.Tier;
import net.minecraft.world.item.crafting.Ingredient;

public enum FPToolMaterials implements Tier 
{
	COMPOSITE(3, 3000, 7F, 2F, 15, () -> Ingredient.of(ResourceItems.composite_metal));
	
	/** The level of material this tool can harvest (3 = DIAMOND, 2 = IRON, 1 = STONE, 0 = WOOD/GOLD) */
	private final int harvestLevel;
	private final int maxUses;
	private final float efficiency;
	private final float attackDamage;
	private final int enchantability;
	private final LazyLoadedValue<Ingredient> repairMaterial;

	private FPToolMaterials(int harvestLevelIn, int maxUsesIn, float efficiencyIn, float attackDamageIn, int enchantabilityIn, Supplier<Ingredient> repairMaterialIn) 
	{
		this.harvestLevel = harvestLevelIn;
		this.maxUses = maxUsesIn;
		this.efficiency = efficiencyIn;
		this.attackDamage = attackDamageIn;
		this.enchantability = enchantabilityIn;
		this.repairMaterial = new LazyLoadedValue<>(repairMaterialIn);
	}

	@Override
	public int getUses() 
	{
		return this.maxUses;
	}
	@Override
	public float getSpeed() 
	{
		return this.efficiency;
	}
	@Override
	public float getAttackDamageBonus() 
	{
		return this.attackDamage;
	}
	@Override
	public int getLevel() 
	{
		return this.harvestLevel;
	}
	@Override
	public int getEnchantmentValue() 
	{
		return this.enchantability;
	}
	@Override
	public Ingredient getRepairIngredient() 
	{
		return this.repairMaterial.get();
	}
}
