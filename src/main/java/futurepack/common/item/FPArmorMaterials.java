package futurepack.common.item;

import java.util.function.Supplier;

import net.minecraft.sounds.SoundEvent;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.util.LazyLoadedValue;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.item.ArmorMaterial;
import net.minecraft.world.item.crafting.Ingredient;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

public enum FPArmorMaterials implements ArmorMaterial
{
	COMPOSITE("composite", 33, new int[]{2, 5, 6, 2}, 9, SoundEvents.ARMOR_EQUIP_IRON, 1.0F, () -> {
	      return Ingredient.of(ResourceItems.composite_metal);
	   }, 0F),
	;

	private static final int[] MAX_DAMAGE_ARRAY = new int[]{13, 15, 16, 11};
	private final String name;
	private final int maxDamageFactor;
	private final int[] damageReductionAmountArray;
	private final int enchantability;
	private final SoundEvent soundEvent;
	private final float toughness;
	private final LazyLoadedValue<Ingredient> repairMaterial;
	private final float knockback_resistance;

	private FPArmorMaterials(String name, int maxDamageFactor, int[] damageReductionAmountArray, int enchantability, SoundEvent soundEvent, float toughness, Supplier<Ingredient> repair, float knockback_resistance) 
	{
		this.name = name;
		this.maxDamageFactor = maxDamageFactor;
		this.damageReductionAmountArray = damageReductionAmountArray;
		this.enchantability = enchantability;
		this.soundEvent = soundEvent;
		this.toughness = toughness;
		this.repairMaterial = new LazyLoadedValue<>(repair);
		this.knockback_resistance = knockback_resistance;
	}

	@Override
	public int getDurabilityForSlot(EquipmentSlot slotIn)
	{
		return MAX_DAMAGE_ARRAY[slotIn.getIndex()] * this.maxDamageFactor;
	}
	@Override
	public int getDefenseForSlot(EquipmentSlot slotIn)
	{
		return this.damageReductionAmountArray[slotIn.getIndex()];
	}
	@Override
	public int getEnchantmentValue() 
	{
		return this.enchantability;
	}
	@Override
	public SoundEvent getEquipSound() 
	{
		return this.soundEvent;
	}
	@Override
	public Ingredient getRepairIngredient() 
	{
		return this.repairMaterial.get();
	}
	@Override
	@OnlyIn(Dist.CLIENT)
	public String getName() 
	{
		return this.name;
	}
	@Override
	public float getToughness() 
	{
		return this.toughness;
	}

	@Override
	public float getKnockbackResistance() 
	{
		return knockback_resistance;
	}
}
