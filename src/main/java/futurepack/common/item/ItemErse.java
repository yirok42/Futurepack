package futurepack.common.item;

import futurepack.common.FPConfig;
import net.minecraft.world.food.FoodProperties;
import net.minecraft.world.item.BlockItem;
import net.minecraft.world.item.Item;
import net.minecraft.world.level.block.Block;

public class ItemErse extends BlockItem 
{
	private FoodProperties food;
	
	public ItemErse(Block crop, Item.Properties builder) 
	{
		super(crop, builder);
		food = new FoodProperties.Builder().nutrition(getHealAmount()).saturationMod(getSaturationModifier()).build();
	}

	public int getHealAmount() 
	{
		return FPConfig.COMMON.erse_food_value.get().intValue();
	}

	public float getSaturationModifier() 
	{
		return FPConfig.COMMON.erse_food_saturation.get().floatValue();
	}
	
	@Override
	public boolean isEdible() 
	{
		return food!=null;
	}
	
	@Override
	public FoodProperties getFoodProperties() 
	{
		if(food==null)
		{
			return super.getFoodProperties();
		}
		else
		{
			if(food.getNutrition() != getHealAmount() || food.getSaturationModifier() != getSaturationModifier())
			{
				food = new FoodProperties.Builder().nutrition(getHealAmount()).saturationMod(getSaturationModifier()).build();
				return food;
			}
			else
			{
				return food;
			}
		}
		
	}
}
