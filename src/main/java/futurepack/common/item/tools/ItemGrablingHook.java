package futurepack.common.item.tools;

import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;

import futurepack.api.interfaces.IItemNeon;
import futurepack.common.entity.throwable.EntityHook;
import futurepack.depend.api.helper.HelperItems;
import net.minecraft.network.chat.Component;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.sounds.SoundSource;
import net.minecraft.util.Mth;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResultHolder;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;

public class ItemGrablingHook extends Item implements IItemNeon
{
	private Map<LivingEntity, EntityHook> Client_map = new WeakHashMap<LivingEntity, EntityHook>();
	private Map<LivingEntity, EntityHook> Server_map = new WeakHashMap<LivingEntity, EntityHook>();
	//IIcon inUse;
	
	public void clean()
	{
		Client_map.clear();
		Server_map.clear();
	}
	
	public ItemGrablingHook(Item.Properties props) 
	{
		super(props);
//		setCreativeTab(FPMain.tab_items);
//		setMaxStackSize(1);
//		setFull3D();
	}
	
	@Override
	public InteractionResultHolder<ItemStack> use(Level w, Player pl, InteractionHand hand)
	{
		ItemStack it = pl.getItemInHand(hand);
		if(getNeon(it)>1)
		{
			if(!map(w).containsKey(pl))
			{
				if(!w.isClientSide)
				{
					w.playSound(null, pl.blockPosition(), SoundEvents.ARROW_SHOOT, SoundSource.NEUTRAL, 0.5F, 0.4F / (w.random.nextFloat() * 0.4F + 0.8F));
						
					EntityHook h = new EntityHook(w,pl);
					h.shootFromRotation(pl, pl.getXRot(), pl.yHeadRot, 0F, 1.6F, 1F);
					map(w).put(pl, h);
					w.addFreshEntity(h);
				}				
			}
			else
			{
				EntityHook hook = map(w).get(pl);
				hook.discard();
				//it.damageItem(1, pl);
				addNeon(it, -1);
				map(w).remove(pl);		
			}
		}
		return InteractionResultHolder.success(it);
	}
	
	public Map<LivingEntity, EntityHook> map(Level w)
	{
		return map(w.isClientSide);
	}  
	
	public Map<LivingEntity, EntityHook> map(boolean remote)
	{
		return remote ? Client_map : Server_map;
	}  

	@Override
	public int getMaxNeon(ItemStack it) 
	{
		return 250;
	}
	
	@Override
	public int getBarColor(ItemStack stack)
	{
		return Mth.hsvToRgb(0.52F, 1.0F, (0.5F + (float)getNeon(stack) / (float)getMaxNeon(stack) * 0.5F));
	}
	
	@Override
	public int getBarWidth(ItemStack stack)
	{
		return (int) (13 * ( ((double)getNeon(stack) / (double)getMaxNeon(stack))));
	}
	
	@Override
	public boolean isBarVisible(ItemStack stack)
	{
		return getNeon(stack) < getMaxNeon(stack);
	}
	
	@Override
	public void appendHoverText(ItemStack it, Level w, List<Component> l, TooltipFlag p_77624_4_) 
	{
		l.add(HelperItems.getTooltip(it, this));
		super.appendHoverText(it, w, l, p_77624_4_);
	}
}
