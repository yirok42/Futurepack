package futurepack.common.item.tools.compositearmor;

import java.util.EnumMap;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Nullable;

import com.google.common.collect.Multimap;

import futurepack.depend.api.helper.HelperItems;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.ai.attributes.Attribute;
import net.minecraft.world.entity.ai.attributes.AttributeModifier;
import net.minecraft.world.item.ItemStack;

public abstract class ItemModulStatChange extends ItemModulArmorBase
{
	protected static final long major = 0x54235678789L;
	protected static final long head = 0x121290902390L;
	protected static final long chest = 0x8909034590909034L;
	protected static final long legs = 0x34897895675670L;
	protected static final long feet = 0x123123456567789L;
	protected static long[] ARMOR_MODIFIERS = new long[]{head, chest, legs, feet};
	
	protected Map<EquipmentSlot, Multimap<Attribute, AttributeModifier>> attributes = new EnumMap<EquipmentSlot, Multimap<Attribute,AttributeModifier>>(EquipmentSlot.class);
	
	@Override
	public Multimap<Attribute, AttributeModifier> getAttributeModifiers(EquipmentSlot slot, ItemStack stack)
	{
		return attributes.computeIfAbsent(slot, this::getDefaultAttributeModifiers);
	}
	
	public ItemModulStatChange(EquipmentSlot slot, Properties props)
	{
		super(slot, props);
	}

	@Override
	public boolean isSlotFitting(ItemStack stack, EquipmentSlot type, @Nullable CompositeArmorPart armorPart)
	{
		if(type.getType() == EquipmentSlot.Type.ARMOR)
		{
			if(armorPart!=null)
			{
				for(int i=0;i<armorPart.getSlots();i++)
				{
					ItemStack st = armorPart.getStackInSlot(i);
					if(st.isEmpty())
						continue;
					else if(st.getItem() == this)
						return false;
				}
			}
			return true;
		}
		
		return false;
	}
	
	@Override
	public abstract Multimap<Attribute, AttributeModifier> getDefaultAttributeModifiers(EquipmentSlot slot);
	
	protected UUID getUUID(EquipmentSlot slot)
	{
		int materialName = HelperItems.getRegistryName(this).toString().hashCode();
		UUID uuid = new UUID(major ^materialName, ARMOR_MODIFIERS[slot.getIndex()] ^materialName);
		return uuid;
	}
}
