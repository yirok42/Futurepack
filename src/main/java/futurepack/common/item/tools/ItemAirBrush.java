package futurepack.common.item.tools;

import java.util.Collection;

import futurepack.common.item.misc.ItemLackTank;
import futurepack.common.recipes.airbrush.AirbrushRegistry;
import futurepack.common.sync.FPGuiHandler;
import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.sounds.SoundSource;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.InteractionResultHolder;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.context.UseOnContext;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.properties.Property;

public class ItemAirBrush extends Item 
{
	
	public ItemAirBrush(Item.Properties props) 
	{
		super(props.stacksTo(1));
	}
	
	
	public static ItemStack getItem(CompoundTag nbt)
	{
		if(nbt != null && nbt.contains("item"))
		{
			CompoundTag tag = nbt.getCompound("item");
			return ItemStack.of(tag);
		}
		
		return ItemStack.EMPTY;
	}
	
	public static void setItem(CompoundTag nbt, ItemStack it)
	{
		if(it != null)
		{
			CompoundTag tag = it.save(new CompoundTag());
			nbt.put("item", tag);
			return;
		}
		nbt.remove("item");
	}
	
	@Override
	public InteractionResultHolder<ItemStack> use(Level w, Player pl, InteractionHand hand)
	{
		ItemStack it = pl.getItemInHand(hand);
		if(pl.getItemInHand(hand) == it && pl.isShiftKeyDown())
		{
			if(!w.isClientSide)
				FPGuiHandler.AIRBRUSH.openGui((ServerPlayer)pl, (Object[])null);
			return InteractionResultHolder.success(it);
		}
		return InteractionResultHolder.pass(it);
	}

	@Override
	public InteractionResult useOn(UseOnContext context)
	{
		Player pl = context.getPlayer();
		if(pl.isShiftKeyDown())
			return InteractionResult.PASS;
		
		ItemStack it = context.getItemInHand();

		if(!context.getLevel().isClientSide)
		{
			if(!it.hasTag())
				it.setTag(new CompoundTag());
			ItemStack inside = getItem(it.getTag());
			if(inside !=null && !inside.isEmpty())
			{
				BlockState state = context.getLevel().getBlockState(context.getClickedPos());
				boolean success = false;
				if(AirbrushRegistry.isUncolorItem(inside))
				{
					Block uncolored = AirbrushRegistry.getUncoloredBlock(state.getBlock());
					if(uncolored != null)
					{
						replaceBlock(context.getLevel(), context.getClickedPos(), pl, state, uncolored);
						success = true;
					}
				}
				else if(inside.getItem() instanceof ItemLackTank)
				{
					Block colored = AirbrushRegistry.getColoredBlock(state.getBlock(), ((ItemLackTank)inside.getItem()).getColor());
					if(colored != null)
					{
						replaceBlock(context.getLevel(), context.getClickedPos(), pl, state, colored);
						success = true;
					}
				}
				
				if(success && !pl.isCreative())
				{
					if(inside.isDamageableItem())
					{
						Item container = inside.getItem().getCraftingRemainingItem();
						inside.hurtAndBreak(1, pl, breaker -> {});
						if(inside.isEmpty())
						{
							inside = new ItemStack(container);
						}
					}
					else
					{
						inside.shrink(1);					
					}
					if(inside.getCount()<=0)
					{
						inside = null;
					}
					setItem(it.getTag(), inside);
				}
				
			}
		}
		return InteractionResult.SUCCESS;
	}
	
	@SuppressWarnings("unchecked")
	public static void replaceBlock(Level w, BlockPos pos, Player pl, BlockState oldstate, Block newBlock)
	{
		BlockState newstate = newBlock.defaultBlockState();
		Collection<Property<?>> col = newstate.getProperties();
		for(@SuppressWarnings("rawtypes") Property prop : col)
		{
			if(oldstate.hasProperty(prop))
			{
				newstate = newstate.setValue(prop, oldstate.getValue(prop));
			}
		}
		
		w.levelEvent(2001, pos, Block.getId(newstate));//same event as in w.destroyBlock(BlockPos, boolean drop)
		w.setBlock(pos, newstate, 3);

		w.playLocalSound(pos.getX()+0.5, pos.getY()+0.5, pos.getZ()+0.5, SoundEvents.FIRE_EXTINGUISH, SoundSource.BLOCKS, 2.0F, 0.2F, true);
	}
	
	public static boolean isInArray(Object[] arr, Object par2)
	{
		for(Object  t : arr)
		{
			if(par2.equals(t))
			{
				return true;
			}
		}
		return false;
	}
	
	public static boolean isInArray(int[] arr, int par2)
	{
		for(int  t : arr)
		{
			if(par2 == t)
			{
				return true;
			}
		}
		return false;
	}
}
