package futurepack.common.item.tools;

import net.minecraft.tags.BlockTags;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Tier;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.material.Material;

public class ItemPlasmaSchneider extends ItemPoweredMineToolBase
{

	public ItemPlasmaSchneider(Item.Properties props, Tier tier) 
	{
		super(props, tier, BlockTags.MINEABLE_WITH_PICKAXE);
		max_speed = 250F;
	}
	
	@Override
	public boolean isMineable(BlockState bs)
	{
		return bs.getMaterial()==Material.METAL;
	}
	
	 @Override
	 public boolean canPerformAction(ItemStack stack, net.minecraftforge.common.ToolAction toolAction) 
	 {
		 return net.minecraftforge.common.ToolActions.DEFAULT_PICKAXE_ACTIONS.contains(toolAction);
	 }
}
