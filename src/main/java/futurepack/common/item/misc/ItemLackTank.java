package futurepack.common.item.misc;

import javax.annotation.Nullable;

import futurepack.common.DirtyHacks;
import net.minecraft.world.item.DyeColor;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;


public class ItemLackTank extends Item 
{
	private static final ItemLackTank[] tanks = new ItemLackTank[16];
	
	private final DyeColor color;
	
	public ItemLackTank(Item.Properties props, DyeColor color)
	{
		super(props.defaultDurability(256));
		this.color = color;
		tanks[color.getId()] = this;
	}
	
	@Override
	public void setDamage(ItemStack stack, int damage) 
	{
		super.setDamage(stack, damage);
		if(stack.getDamageValue()>=getMaxDamage(stack))
		{
			ItemStack it = new ItemStack(MiscItems.lack_tank_empty);
			DirtyHacks.replaceData(stack, it, ItemStack.class); //this might can be go wrong, as I am using dirty Reflection
		}
	}
	
	public DyeColor getColor()
	{
		return color;
	}
	
	@Nullable
	public static ItemLackTank getItemFromColor(DyeColor col)
	{
		return tanks[col.getId()];
	}
}
