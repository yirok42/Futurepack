package futurepack.common.item.misc;

import futurepack.api.interfaces.IPlanet;
import futurepack.common.spaceships.FPPlanetRegistry;
import futurepack.depend.api.helper.HelperResearch;
import futurepack.world.dimensions.Dimensions;
import net.minecraft.Util;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResultHolder;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;

public class ItemTelescope extends Item
{
	public ItemTelescope(Item.Properties props)
	{
		super(props);
	}
	
	@Override
	public InteractionResultHolder<ItemStack> use(Level w, Player pl, InteractionHand hand)
	{
		if(!w.isClientSide && HelperResearch.isUseable(pl, this))
		{
			if(pl.level.dimension().location().equals(Dimensions.MENELAUS_ID))
			{
				pl.sendMessage(new TranslatableComponent("research.planet.discover", "Tyros"), Util.NIL_UUID);
				IPlanet tyr = FPPlanetRegistry.instance.getPlanetByDimension(Dimensions.TYROS_ID);
				return InteractionResultHolder.success(FPPlanetRegistry.instance.getItemFromPlanet(tyr));
			}
			else
			{
				pl.sendMessage(new TranslatableComponent("research.planet.discover", "Menelaus"), Util.NIL_UUID);
				IPlanet men = FPPlanetRegistry.instance.getPlanetByDimension(Dimensions.MENELAUS_ID);
				return InteractionResultHolder.success(FPPlanetRegistry.instance.getItemFromPlanet(men));
			}
		}
		return super.use(w, pl, hand);
	}
}
