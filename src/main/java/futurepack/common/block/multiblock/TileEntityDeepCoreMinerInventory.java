package futurepack.common.block.multiblock;

import java.util.function.Supplier;

import javax.annotation.Nullable;

import futurepack.api.FacingUtil;
import futurepack.api.interfaces.tilentity.ITileClientTickable;
import futurepack.api.interfaces.tilentity.ITileScrollableInventory;
import futurepack.api.interfaces.tilentity.ITileServerTickable;
import futurepack.common.FPTileEntitys;
import futurepack.common.block.FPTileEntityBase;
import futurepack.common.block.multiblock.BlockDeepCoreMiner.EnumDeepCoreMiner;
import futurepack.depend.api.helper.HelperEnergyTransfer;
import futurepack.depend.api.helper.HelperInventory;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.IItemHandlerModifiable;
import net.minecraftforge.items.ItemHandlerHelper;

public class TileEntityDeepCoreMinerInventory extends FPTileEntityBase implements ITileScrollableInventory, ITileClientTickable, ITileServerTickable
{
	public TileEntityDeepCoreMinerInventory(BlockPos pos, BlockState state) 
	{
		super(FPTileEntitys.DEEPCORE_INVENTORY, pos, state);
	}

	@Override
	public void tickServer(Level pLevel, BlockPos pPos, BlockState pState)
	{
		tick();
	}
	
	@Override
	public void tickClient(Level pLevel, BlockPos pPos, BlockState pState)
	{
		tick();
	}
	
	public void tick()
	{
		TileEntityDeepCoreMinerMain main = getMain();
		if(main!=null)
		{
			IItemHandler inv = main.getDeepCoreInventory(false);

			for(Direction face : FacingUtil.VALUES)
			{
				level.getProfiler().push("Inserting " + face.getSerializedName());
				
				BlockPos other = worldPosition.relative(face);
				BlockEntity tile = level.getBlockEntity(other);
				
				if(tile!=null && tile.getClass()!=TileEntityDeepCoreMinerInventory.class)
				{
					IItemHandler handler = HelperInventory.getHandler(tile, face.getOpposite());
					if(handler!=null)
						HelperInventory.transferItemStacks(inv, handler);
				}
				level.getProfiler().pop();
			}
		}
	}
	
	@Nullable
	private TileEntityDeepCoreMinerMain getMain()
	{
		BlockState state = level.getBlockState(worldPosition);
		if(state.getBlock() == MultiblockBlocks.deepcore_miner)
		{			
			EnumDeepCoreMiner type =  state.getValue(BlockDeepCoreMiner.variants);
			if(type.isInventory())
			{
				BlockPos rel = null;
				BlockEntity tile1, tile2;
				tile1 = level.getBlockEntity(worldPosition.offset(type.getMain(false)));			
				tile2 = level.getBlockEntity(worldPosition.offset(type.getMain(true)));
				
				if(tile1!=null && tile2 !=null && tile1.getClass() == TileEntityDeepCoreMinerMain.class && tile2.getClass() == TileEntityDeepCoreMinerMain.class)
				{
					//this should never happen but well, lets make sure nothing fuck up;
					TileEntityDeepCoreMinerMain main1 = (TileEntityDeepCoreMinerMain) tile1, main2 = (TileEntityDeepCoreMinerMain) tile2;
					Direction f1 = main1.getFacing(), f2 = main2.getFacing();
					if(f1!=null && f2 !=null)
					{
						BlockPos p1 = main1.getBlockPos().relative(f1, 2), p2 = main2.getBlockPos().relative(f2, 2);
						boolean flag1 = p1.getX() == worldPosition.getX() && p1.getZ() == worldPosition.getZ();
						boolean flag2 = p2.getX() == worldPosition.getX() && p2.getZ() == worldPosition.getZ();
						
						if(flag1 && flag2)
							throw new IllegalStateException("WTF what have you done?! PLase report this and also give me your world (Futurepack)!");
						else if(flag1)
							return main1;
						else if(flag2)
							return main2;
					}
					else if(f1 != null && f2 == null)
						return main1;
					else if(f1 == null && f2 != null)
						return main2;
				}
				else if(tile1!=null && tile1.getClass() == TileEntityDeepCoreMinerMain.class)
				{
					return (TileEntityDeepCoreMinerMain) tile1;
				}			
				else if(tile2 !=null && tile2.getClass() == TileEntityDeepCoreMinerMain.class)
				{
					return (TileEntityDeepCoreMinerMain) tile2;
				}			
				return null;
				
			}
			else
			{
				throw new RuntimeException("DeepCoreMinerInventory is at wrong Position! " + this.worldPosition);
			}
		}
		return null;
	}
	
	private LazyOptional<IItemHandler> itemOpt;
	
	@SuppressWarnings("unchecked")
	@Override
	public <T> LazyOptional<T> getCapability(Capability<T> capability, Direction facing)
	{
		if(capability == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY)
		{
			if(itemOpt!=null)
				return (LazyOptional<T>) itemOpt;
			else
			{
				TileEntityDeepCoreMinerMain main = getMain();
				Supplier<TileEntityDeepCoreMinerMain> deepcore_main;
				if(main!=null)
				{
					deepcore_main = () -> main;
				}
				else
				{
					deepcore_main = this::getMain;
				}
				itemOpt = LazyOptional.of(() -> deepcore_main.get().getDeepCoreInventory(false));
				itemOpt.addListener(p -> itemOpt = null);
				return (LazyOptional<T>) itemOpt;
			}
		}
		return super.getCapability(capability, facing);
	}
	
	@Override
	public void setRemoved() 
	{
		HelperEnergyTransfer.invalidateCaps(itemOpt);
		super.setRemoved();
	}
	
	@Override
	public IItemHandlerModifiable getScrollableInventory()
	{
		return getMain().getOreInv();
	}
	
	public int getComparatorOutput()
	{
		IItemHandlerModifiable mod = getScrollableInventory();
		return ItemHandlerHelper.calcRedstoneFromInventory(mod);
	}
}
