package futurepack.common.block.modification;

import futurepack.common.FPTileEntitys;
import futurepack.common.sync.FPGuiHandler;
import futurepack.depend.api.helper.HelperResearch;
import net.minecraft.core.BlockPos;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.BlockHitResult;

public class BlockRocketLauncher extends BlockEntityLaserBase 
{

	protected BlockRocketLauncher(Properties props) 
	{
		super(props, () -> FPTileEntitys.ROCKET_LAUNCHER);
	}

	@Override
	public InteractionResult use(BlockState state, Level w, BlockPos pos, Player pl, InteractionHand hand, BlockHitResult hit)
	{
		if(HelperResearch.canOpen(pl, state) && !w.isClientSide)
		{
			FPGuiHandler.ROCKET_LAUNCHER_EDIT.openGui(pl, pos);
			return InteractionResult.SUCCESS;
		}
		
		return super.use(state, w, pos, pl, hand, hit);
	}
}
