package futurepack.common.block.modification;

import futurepack.api.Constants;
import futurepack.common.FPTileEntitys;
import futurepack.common.modification.EnumChipType;
import net.minecraft.core.BlockPos;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.level.block.state.BlockState;

public class TileEntityEntityHealer extends TileEntityLaserBase<LivingEntity>
{

	public TileEntityEntityHealer(BlockPos pos, BlockState state)
	{
		super(FPTileEntitys.ENTITY_HEALER, LivingEntity.class, pos , state);
		
		setConfig("attack.player", true);
		setConfig("attack.neutral", true);
		setConfig("attack.mobs", false);
		setConfig("kill.not", false);//dont work anyway
		
		setConfig("player.warn", false);
	}

	@Override
	public void resetConfig() {
		super.resetConfig();
		setConfig("attack.player", true);
		setConfig("attack.neutral", true);
		setConfig("attack.mobs", false);
	}

	@Override
	public boolean isEntityValid(LivingEntity entity)
	{
		if(!matchConfig(entity))
			return false;
		
		boolean alive = entity.isAlive() && entity.getHealth() < (entity.getMaxHealth() * (getConfiguration("kill.not") ? 0.5 : 1));
		return alive;
	}

	@Override
	public void progressEntity(LivingEntity entity)
	{
		if(entity.getMaxHealth() > entity.getHealth())
		{
			int h = 1;
			int p = 10;		
			if(energy.get()>10)
			{
				int power = (int)(p / (1 + getChipPower(EnumChipType.INDUSTRIE)));		
				this.energy.use(power);
				entity.heal(h);
			}		
		}
	}

	@Override
	public boolean shouldWork()
	{
		return energy.get() > 10;
	}

	@Override
	public ResourceLocation getTexture()
	{
		return new ResourceLocation(Constants.MOD_ID, "textures/model/eater_3.png");
	}

	@Override
	public int getLaserColor()
	{
		return 0x76FF37;
	}

	@Override
	public ResourceLocation getLaser()
	{
		return new ResourceLocation(Constants.MOD_ID, "textures/model/laser_healer.png");
	}

	@Override
	public float getRange()
	{
		return super.getRange() * 2F;
	}
	
	@Override
	public boolean canAttackOwner()
	{
		return true;
	}
}

