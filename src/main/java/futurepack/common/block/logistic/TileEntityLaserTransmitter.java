package futurepack.common.block.logistic;

import futurepack.api.PacketBase;
import futurepack.api.interfaces.INetworkUser;
import futurepack.common.FPTileEntitys;
import futurepack.common.block.BlockRotateableTile;
import futurepack.common.block.TileEntityNetworkMaschine;
import futurepack.common.network.NetworkManager;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;

public class TileEntityLaserTransmitter extends TileEntityNetworkMaschine implements INetworkUser
{
	public TileEntityLaserTransmitter(BlockPos pos, BlockState state) 
	{
		super(FPTileEntitys.LASER_TRASNMITTER, pos, state);
	}
	
	@Override
	public void onFunkPacket(PacketBase pkt)
	{
		sendPacket(pkt, pkt.getNetworkDepth()+1);
	}
	
	private BlockPos lastPos;
	private long gameTime;
	
	private void sendPacket(PacketBase pkt, int networkDepth)
	{
		BlockState state = getBlockState();
		Direction face = state.getValue(BlockRotateableTile.FACING);
		
		if(level.getGameTime() - gameTime < 20 * 60)
		{
			
			BlockEntity tile = NetworkManager.supplyAsync(level, () -> level.getBlockEntity(lastPos));
			if(tile instanceof TileEntityLaserTransmitter)
			{
				TileEntityLaserTransmitter laser = (TileEntityLaserTransmitter) tile;
				if(laser.getBlockState().getValue(BlockRotateableTile.FACING) == face.getOpposite())
				{
					laser.onPacketRecived(pkt, networkDepth);
					return;
				}
			}
		}
		BlockPos.MutableBlockPos xyz = new BlockPos.MutableBlockPos().set(worldPosition.relative(face));
		for(int i=0;i<1024;i++)
		{
			if(level.isEmptyBlock(xyz))
			{
				xyz.move(face);
				BlockEntity tile = NetworkManager.supplyAsync(level, () -> level.getBlockEntity(xyz));
				if(tile instanceof TileEntityLaserTransmitter)
				{
					lastPos = xyz.immutable();
					gameTime = level.getGameTime();
					
					TileEntityLaserTransmitter laser = (TileEntityLaserTransmitter) tile;
					if(laser.getBlockState().getValue(BlockRotateableTile.FACING) == face.getOpposite())
					{
						laser.onPacketRecived(pkt, networkDepth);
						return;
					}
				}
			}
			else
			{
				break;
			}
		}
	}
	
	private void onPacketRecived(PacketBase pkt, int networkDepth)
	{
		NetworkManager.sendPacketUnthreaded(this, pkt, networkDepth);
	}

	@Override
	public void postPacketSend(PacketBase pkt){	}

	@Override
	public Level getSenderWorld()
	{
		return this.level;
	}
	
	@Override
	public BlockPos getSenderPosition()
	{
		return this.worldPosition;
	}
}
