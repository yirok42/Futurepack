package futurepack.common.block.logistic.monorail;

import java.util.Random;

import futurepack.api.interfaces.IBlockMonocartWaypoint;
import futurepack.common.entity.monocart.EntityMonocart;
import futurepack.common.entity.monocart.EntityMonocartBase;
import futurepack.common.sync.FPGuiHandler;
import futurepack.depend.api.helper.HelperResearch;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.network.chat.Component;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.util.StringRepresentable;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.EntityBlock;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition.Builder;
import net.minecraft.world.level.block.state.properties.EnumProperty;
import net.minecraft.world.phys.BlockHitResult;

public class BlockMonorailStation extends BlockMonorailBasic implements IBlockMonocartWaypoint, EntityBlock
{

//	public static final BooleanProperty loading = BooleanProperty.create("loading");
	public static final EnumProperty<EnumMonorailStation> loading = EnumProperty.create("loading", EnumMonorailStation.class);
	
	public BlockMonorailStation(Properties props) 
	{
		super(props);
	}
	
	@Override
	public boolean isBendable()
	{
		return false;
	}
	
	@Override
	public BlockEntity newBlockEntity(BlockPos pos, BlockState state)
	{
		return new TileEntityMonorailStation(pos, state);
	}
	
	@Override
	public InteractionResult use(BlockState state, Level w, BlockPos pos, Player pl, InteractionHand hand, BlockHitResult hit)
	{
		if(HelperResearch.canOpen(pl, state))
		{
			FPGuiHandler.MONORAIL_STATION.openGui(pl, pos);
		}	
		return InteractionResult.SUCCESS;
	}
	
	@Override
	public void neighborChanged(BlockState state, Level w, BlockPos pos, Block neighborBlock, BlockPos npos, boolean isMoving) 
	{
		super.neighborChanged(state, w, pos, neighborBlock, npos, isMoving);
		TileEntityMonorailStation stat = (TileEntityMonorailStation) w.getBlockEntity(pos);
		stat.onNeighbourChange();
	}
	
	@Override
	public void onMonocartPasses(Level w, BlockPos pos, BlockState state, EntityMonocartBase e)
	{	
		if(state.getValue(BlockMonorailStation.loading)==EnumMonorailStation.ready)
		{
			TileEntityMonorailStation stat = (TileEntityMonorailStation) w.getBlockEntity(pos);
			if(e instanceof EntityMonocart)
			{
				EntityMonocart mono = (EntityMonocart) e;
				BlockPos[] posses = mono.getWaypoint();
				if(posses!=null)
				{
					for(BlockPos wayp : posses)
					{
						if(pos.equals(wayp))
						{
							e.setPaused(true);
							w.setBlockAndUpdate(pos, state.setValue(BlockMonorailStation.loading, EnumMonorailStation.transfer));
							w.scheduleTick(pos, this, 20);
							stat.progressMonoCart(mono);
							break;
						}
					}
				}		
			}
			
		}
		else if(state.getValue(BlockMonorailStation.loading)==EnumMonorailStation.cooldown)
		{
			e.setPaused(false);
		}
	}	
	
	@Override
	public void tick(BlockState state, ServerLevel w, BlockPos pos, Random rand)
	{
		if(state.getValue(BlockMonorailStation.loading)==EnumMonorailStation.transfer)
		{
			if (!w.hasNeighborSignal(pos)) 
			{
			  w.setBlockAndUpdate(pos, state.setValue(BlockMonorailStation.loading, EnumMonorailStation.cooldown));
			  w.scheduleTick(pos, this, 40);
			}
			else
			{
				w.scheduleTick(pos, this, 20);
			}
		}
		else if(state.getValue(BlockMonorailStation.loading)==EnumMonorailStation.cooldown)
		{
			w.setBlockAndUpdate(pos, state.setValue(BlockMonorailStation.loading, EnumMonorailStation.ready));
			TileEntityMonorailStation stat = (TileEntityMonorailStation) w.getBlockEntity(pos);
			stat.onNeighbourChange();
		}
		
		super.tick( state, w, pos,rand);
	}
	
	@Override
	public int getSignal(BlockState state, BlockGetter blockAccess, BlockPos pos, Direction side)
	{
		return state.getValue(BlockMonorailStation.loading)==EnumMonorailStation.transfer ? 15 : 0;
	}
	
//	@Override
//	public IBlockState getStateFromMeta(int meta)
//    {
//		if(meta>5)
//		{
//			return this.getDefaultState().with(loading, EnumMonorailStation.transfer).with(State_bendless, EnumMonorailStates.getFromMeta(meta%6));
//		}
//        return this.getDefaultState().with(loading, EnumMonorailStation.ready).with(State_bendless, EnumMonorailStates.getFromMeta(meta));
//    }
//
//	@Override
//    public int getMetaFromState(IBlockState state)
//    {
//        return state.get(State_bendless).getMeta() + (state.get(BlockMonorailStation.loading)==EnumMonorailStation.transfer ? 6 : 0);
//    }
	
	@Override
	protected void createBlockStateDefinition(Builder<Block, BlockState> builder)
	{
		builder.add(loading);
		super.createBlockStateDefinition(builder);
	}
	
	public static enum EnumMonorailStation implements StringRepresentable
	{
		ready,//green
		transfer,//red
		cooldown;//yellow

		@Override
		public String getSerializedName()
		{
			return name();
		}
		
	}

	@Override
	public Component getName(Level w, BlockPos pos, BlockState state)
	{
		TileEntityMonorailStation tile = (TileEntityMonorailStation) w.getBlockEntity(pos);
		return tile.getName();
	}

	@Override
	public boolean isWaypoint(Level w, BlockPos pos, BlockState state)
	{
		return true;
	}
}
