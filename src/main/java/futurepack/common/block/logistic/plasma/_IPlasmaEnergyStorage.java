package futurepack.common.block.logistic.plasma;

public interface _IPlasmaEnergyStorage
{
	boolean canAcceptFrom(_IPlasmaEnergyStorage other);
	
	boolean canTransferTo(_IPlasmaEnergyStorage other);
	
	/**
	 * @return the current amount of energy stored
	 */
	public long get();
	
	/**
	 * @return the max amount of energy stored
	 */
	public long getMax();
	
	/**
	 * @param used the amount of energy to use
	 * @return the actual used amount
	 */
	public long use(long used);
	
	/**
	 * @param added the amount of energy to add
	 * @return the actual added amount
	 */
	public long add(long added);
	
}
