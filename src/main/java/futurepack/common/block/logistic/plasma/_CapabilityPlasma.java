package futurepack.common.block.logistic.plasma;

import net.minecraft.nbt.CompoundTag;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityManager;
import net.minecraftforge.common.capabilities.CapabilityToken;
import net.minecraftforge.common.util.INBTSerializable;

public class _CapabilityPlasma implements _IPlasmaEnergyStorage, INBTSerializable<CompoundTag>
{
	public static final Capability<_IPlasmaEnergyStorage> cap_PLASMA = CapabilityManager.get(new CapabilityToken<>(){});
	
//	public static class Storage implements IStorage<IPlasmaEnergyStorage>
//	{
//
//		@Override
//		public Tag writeNBT(Capability<IPlasmaEnergyStorage> capability, IPlasmaEnergyStorage instance, Direction side)
//		{
//			return LongTag.valueOf(instance.get());
//		}
//
//		@Override
//		public void readNBT(Capability<IPlasmaEnergyStorage> capability, IPlasmaEnergyStorage instance, Direction side, Tag nbt)
//		{
//			if(nbt instanceof LongTag)
//			{
//				long power = ((LongTag)nbt).getAsLong();
//				
//				if(power > instance.get())
//				{
//					instance.add(power - instance.get());
//				}
//				else if(power < instance.get())
//				{
//					instance.use(instance.get() - power);
//				}
//			}
//		}
//
//	}
	
	public static final _IPlasmaEnergyStorage EMPTY = new _IPlasmaEnergyStorage()
	{
		@Override
		public long use(long used) 
		{
			return 0;
		}
		
		@Override
		public long getMax() 
		{
			return 0;
		}
		
		@Override
		public long get() 
		{
			return 0;
		}
		
		@Override
		public boolean canTransferTo(_IPlasmaEnergyStorage other) 
		{
			return false;
		}
		
		@Override
		public boolean canAcceptFrom(_IPlasmaEnergyStorage other) 
		{
			return false;
		}
		
		@Override
		public long add(long added) 
		{
			return 0;
		}
	};
	
	protected long energy = 0;
	private final long maxenergy;
	private final boolean in, out;

	public _CapabilityPlasma()
	{
		this(Short.MAX_VALUE, true, true);
	}
	
	public _CapabilityPlasma(long maxpower, boolean in, boolean out)
	{
		this.maxenergy = maxpower;
		this.energy = 0;
		this.in = in;
		this.out = out;
	}


	@Override
	public boolean canTransferTo(_IPlasmaEnergyStorage other)
	{
		return out;
	}

	@Override
	public boolean canAcceptFrom(_IPlasmaEnergyStorage other)
	{
		return in;
	}

	@Override
	public CompoundTag serializeNBT()
	{
		CompoundTag nbt = new CompoundTag();
		nbt.putLong("plasma", get());
		return nbt;
	}

	@Override
	public void deserializeNBT(CompoundTag nbt)
	{
		this.energy = nbt.getLong("plasma");
	}

	@Override
	public long get()
	{
		return energy;
	}

	@Override
	public long getMax()
	{
		return maxenergy;
	}

	@Override
	public long use(long used)
	{
		if(energy >= used)
		{
			energy -= used;
			return used;
		}
		else
		{
			long p = energy;
			energy = 0;
			return p;
		}
	}

	@Override
	public long add(long added)
	{
		if(energy + added <= getMax())
		{
			energy += added;
			return added; 
		}
		else
		{
			long p = getMax() - energy;
			energy = getMax();
			return p;
		}
	}

	public void set(long e)
	{
		energy = e;
	}
}
