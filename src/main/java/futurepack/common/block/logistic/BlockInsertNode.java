package futurepack.common.block.logistic;

import java.util.HashMap;
import java.util.List;

import futurepack.api.interfaces.IBlockServerOnlyTickingEntity;
import futurepack.common.FPTileEntitys;
import futurepack.common.item.tools.ToolItems;
import futurepack.common.sync.FPGuiHandler;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LevelAccessor;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.DirectionalBlock;
import net.minecraft.world.level.block.Mirror;
import net.minecraft.world.level.block.Rotation;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition.Builder;
import net.minecraft.world.level.block.state.properties.DirectionProperty;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.Shapes;
import net.minecraft.world.phys.shapes.VoxelShape;

public class BlockInsertNode extends BlockPipeBase<TileEntityInsertNode> implements IBlockServerOnlyTickingEntity<TileEntityInsertNode>
{
	protected static final VoxelShape AABB_UP    = Shapes.or(Block.box(2, 0, 2, 14, 4, 14), BlockPipeBase.BOX_BASE);
	protected static final VoxelShape AABB_DOWN  = Shapes.or(Block.box(2, 12, 2, 14, 16, 14), BlockPipeBase.BOX_BASE);
    protected static final VoxelShape AABB_NORTH = Shapes.or(Block.box(2, 2, 12D, 14, 14, 16D), BlockPipeBase.BOX_BASE);
    protected static final VoxelShape AABB_SOUTH = Shapes.or(Block.box(2, 2, 0D, 14, 14, 4D), BlockPipeBase.BOX_BASE);
    protected static final VoxelShape AABB_WEST  = Shapes.or(Block.box(12, 2, 2, 16D, 14, 14), BlockPipeBase.BOX_BASE);
    protected static final VoxelShape AABB_EAST  = Shapes.or(Block.box(0, 2, 2, 4, 14, 14), BlockPipeBase.BOX_BASE);
	
    public static final DirectionProperty FACING = DirectionalBlock.FACING;
    
	public BlockInsertNode(Block.Properties props)
	{
		super(props, true);
//		super(Material.IRON);
//		setCreativeTab(FPMain.tab_maschiens);
	}
	
//	@Override
//	public boolean isOpaqueCube(IBlockState state) 
//	{
//		return false;
//	}
	
//	@Override
//	public boolean isNormalCube(BlockState state, IBlockReader worldIn, BlockPos pos)
//	{
//		return false;
//	}

	@Override
	public void appendHoverText(ItemStack stack, BlockGetter worldIn, List<Component> tooltip, TooltipFlag flagIn) 
	{
		super.appendHoverText(stack, worldIn, tooltip, flagIn);
		tooltip.add(new TranslatableComponent("tooltip.futurepack.items.insertnode.joke"));//"It is an Item Retriever, the name is a german joke."
	}
	
//	@Override
//	public Direction[] getValidRotations(BlockState state, IBlockReader world, BlockPos pos)
//	{
//		return FacingUtil.VALUES;
//	}
	
	@Override
	public BlockState rotate(BlockState state, LevelAccessor world, BlockPos pos, Rotation direction)
	{
		return rotate(state, direction);
	}
	
	@Override
	public BlockState rotate(BlockState state, Rotation rot)
	{
		return state.setValue(FACING, rot.rotate(state.getValue(FACING)));
	}
	
	@Override
	public BlockState mirror(BlockState state, Mirror mirrorIn)
	{
		return state.setValue(FACING, mirrorIn.mirror(state.getValue(FACING)));
	}
	
	@Override
	public BlockState getStateForPlacement(BlockPlaceContext context)
	{
		Direction face = context.getClickedFace();
		return super.getStateForPlacement(context).setValue(FACING, face);
	}
	
	protected static HashMap<Integer, VoxelShape> voxelShadeInsertNodeCache = new HashMap<Integer, VoxelShape>();
	
	@Override
	public VoxelShape getShape(BlockState state, BlockGetter w, BlockPos pos, CollisionContext sel)
	{
		if(Boolean.TRUE.equals(state.getValue(HOLOGRAM))) {
			return super.getShape(state, w, pos, sel);
		}
		
		Direction enumfacing = state.getValue(FACING);
	      
		Integer cacheKey = getVoxelShadeCacheKey(state) | (enumfacing.get3DDataValue() << 12);
		
		VoxelShape shape = voxelShadeInsertNodeCache.get(cacheKey);
		
		if(shape != null)
		{
			return shape;
		}
			
		shape = super.getShape(state, w, pos, sel);
		
        switch (enumfacing)
        {
            case EAST:
            	shape = Shapes.or(AABB_EAST, shape); break;
            case WEST:
            	shape = Shapes.or(AABB_WEST, shape); break;
            case SOUTH:
            	shape = Shapes.or(AABB_SOUTH, shape); break;
            case NORTH:
            	shape = Shapes.or(AABB_NORTH, shape); break;
            case UP:
            	shape = Shapes.or(AABB_UP, shape); break;
            case DOWN:
            	shape = Shapes.or(AABB_DOWN, shape); break;
            default:
            	return shape;
        }
        
        voxelShadeInsertNodeCache.put(cacheKey, shape);

        return shape;
	}

	
	@Override
	protected void createBlockStateDefinition(Builder<Block, BlockState> builder)
	{
		super.createBlockStateDefinition(builder);
		builder.add(FACING);
	}
	

	@Override
	public InteractionResult use(BlockState state, Level w, BlockPos pos, Player pl, InteractionHand hand, BlockHitResult hit)
	{
		if(pl.getItemInHand(InteractionHand.MAIN_HAND)!=null && pl.getItemInHand(InteractionHand.MAIN_HAND).getItem()==ToolItems.scrench)
		{
			return super.use(state, w, pos, pl, hand, hit);
		}
		else if(!w.isClientSide)
		{
			FPGuiHandler.GENERIC_CHEST.openGui(pl, pos);
			return InteractionResult.SUCCESS;
		}
		return InteractionResult.PASS;
	}

	@Override
	public boolean hasSpecial() 
	{
		return false;
	}

	@Override
	public BlockEntityType<TileEntityInsertNode> getTileEntityType(BlockState pState)
	{
		return FPTileEntitys.INSERT_NODE;
	}


}
