package futurepack.common.block.logistic;

import java.util.List;

import javax.annotation.Nullable;

import futurepack.api.EnumLogisticIO;
import futurepack.api.EnumLogisticType;
import futurepack.api.FacingUtil;
import futurepack.api.capabilities.CapabilityLogistic;
import futurepack.api.capabilities.CapabilityNeon;
import futurepack.api.capabilities.IEnergyStorageBase.EnumEnergyMode;
import futurepack.api.capabilities.INeonEnergyStorage;
import futurepack.api.interfaces.IBlockServerOnlyTickingEntity;
import futurepack.common.item.tools.ItemLogisticEditor;
import futurepack.common.item.tools.ToolItems;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LevelAccessor;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.RenderShape;
import net.minecraft.world.level.block.Rotation;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition.Builder;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.level.block.state.properties.BooleanProperty;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.Shapes;
import net.minecraft.world.phys.shapes.VoxelShape;
import net.minecraftforge.common.util.LazyOptional;

public abstract class BlockWireBase<T extends TileEntityWireBase> extends BlockWaterLoggedHologram implements IBlockServerOnlyTickingEntity<T>
{
	public static final BooleanProperty upB = BlockStateProperties.UP,
			downB = BlockStateProperties.DOWN,
			northB = BlockStateProperties.NORTH,
			eastB = BlockStateProperties.EAST,
			southB = BlockStateProperties.SOUTH,
			westB = BlockStateProperties.WEST;
	
	 
	
	public static BooleanProperty hologram = BooleanProperty.create("hologram");
	
	
	private final VoxelShape box;
	
	public BlockWireBase(Block.Properties props, boolean hasNBTCustomDrops) 
	{
		super(props, hasNBTCustomDrops);//hasNBTCustomDrops default false
		box = Shapes.box(0.25F, 0.25F, 0.25F, 0.75F, 0.75F, 0.75F);
		registerDefaultState(this.stateDefinition.any().setValue(hologram, false).setValue(upB, false).setValue(downB, false).setValue(northB, false).setValue(southB, false).setValue(eastB, false).setValue(westB, false).setValue(WATERLOGGED, false));
	}
	
	public BlockWireBase(Block.Properties props) 
	{
		this(props, false);
	}
	
	@Override
	public VoxelShape getShape(BlockState state, BlockGetter w, BlockPos pos, CollisionContext sel)
	{
		if(Boolean.TRUE.equals(state.getValue(hologram))) {
			return super.getShape(state, w, pos, sel);
		}
		
		return box;
	}
	
	@Override
	public RenderShape getRenderShape(BlockState state)
    {
		if(state.getValue(hologram))
			return RenderShape.INVISIBLE;
		
        return RenderShape.MODEL;
    }
	
	public abstract BlockEntityType<T> getTileEntityType(BlockState pState);
	
	protected abstract int getMaxNeon();
	
	@Override
	public void appendHoverText(ItemStack stack, BlockGetter worldIn, List<Component> tooltip, TooltipFlag flagIn) 
	{
		super.appendHoverText(stack, worldIn, tooltip, flagIn);
		tooltip.add(new TranslatableComponent("tooltip.futurepack.block.conduct.neon", getMaxNeon()));
	}
	
	@Override
	protected void createBlockStateDefinition(Builder<Block, BlockState> builder)
	{
		super.createBlockStateDefinition(builder);
		builder.add(upB, downB, eastB, westB, southB, northB, hologram);
	}
	
	@Override
	public BlockState getStateForPlacement(BlockPlaceContext context)
	{
		BlockState state = super.getStateForPlacement(context);
		BlockPos pos = context.getClickedPos();
		Level w = context.getLevel();
		for(Direction face : FacingUtil.VALUES)
		{
			BlockPos xyz = pos.relative(face);
			BlockState facingState = w.getBlockState(xyz);
			BlockEntity tile = w.getBlockEntity(xyz);
			state = withConnection(state, face, canConnect(w, pos, xyz, facingState, tile, face));
		}
		return state;
	}
	
	/**
	 * This is called pre placed
	 * 
	 * @param w	the World
	 * @param pos the wire position 
	 * @param xyz the position to connect to
	 * @param facingState the state of the block to connect to
	 * @param tile the TileEntity of the block to connect to, can be null
	 * @param face the direct to connect
	 * @return
	 */
	public boolean canConnect(LevelAccessor w, BlockPos pos, BlockPos xyz, BlockState facingState, @Nullable BlockEntity tile, Direction face)
	{
		if(tile!=null)
		{
			LazyOptional<INeonEnergyStorage> capT = tile.getCapability(CapabilityNeon.cap_NEON, face.getOpposite());
			if(capT.isPresent())
			{	
				INeonEnergyStorage store = capT.orElseThrow(NullPointerException::new);
				if(store.getType()!=EnumEnergyMode.NONE)
				{	
					return true;
				}
			}
		}
		
		return false;
	}
	
	/**
	 * Called when the wire is already placed
	 * 
	 * @param w	the World
	 * @param pos the wire position 
	 * @param xyz the position to connect to
	 * @param state the wire state
	 * @param facingState the state of the block to connect to
	 * @param wire the Wire TileEntity
	 * @param tile the TileEntity of the block to connect to, can be null
	 * @param face the direct to connect
	 * @return
	 */
	public boolean canConnect(LevelAccessor w, BlockPos pos, BlockPos xyz, BlockState state, BlockState facingState, TileEntityWireBase wire, @Nullable BlockEntity tile, Direction face)
	{
		if(canConnect(w, pos, xyz, facingState, tile, face))
		{
			LazyOptional<INeonEnergyStorage> capW = wire.getCapability(CapabilityNeon.cap_NEON, face);
			if(capW.isPresent())
			{
				return true;
			}
		}
		return false;
	}
	
	@Override
	public BlockState updateShape(BlockState state, Direction face, BlockState facingState, LevelAccessor w, BlockPos pos, BlockPos xyz)
	{
		BlockEntity tile = w.getBlockEntity(xyz);
		BlockEntity tile2 = w.getBlockEntity(pos);
		try
		{
			TileEntityWireBase wire = (TileEntityWireBase) tile2;
			
			boolean connect = canConnect(w, pos, xyz, state, facingState, wire, tile, face);
			
			state = withConnection(state, face, connect);
	
			return state;
		}
		catch(ClassCastException e)
		{
			e.printStackTrace();
			return this.defaultBlockState();
		}
	}
	
	protected static BlockState withConnection(BlockState state, Direction face, boolean connect)
	{
		switch (face)
		{
		case UP:
			return state.setValue(upB, connect);
		case DOWN:
			return state.setValue(downB, connect);
		case NORTH:
			return state.setValue(northB, connect);
		case SOUTH:
			return state.setValue(southB, connect);
		case WEST:
			return state.setValue(westB, connect);
		case EAST:
			return state.setValue(eastB, connect);
		default:
			return state;
		}
	}
	
	protected boolean isConnected(BlockState state, Direction face)
	{
		switch (face)
		{
		case UP:
			return state.getValue(upB);
		case DOWN:
			return state.getValue(downB);
		case NORTH:
			return state.getValue(northB);
		case SOUTH:
			return state.getValue(southB);
		case WEST:
			return state.getValue(westB);
		case EAST:
			return state.getValue(eastB);
		default:
			return false;
		}
	}
	
	@Override
	public InteractionResult use(BlockState state, Level w, BlockPos pos, Player pl, InteractionHand hand, BlockHitResult hit)
	{
		if(pl.getItemInHand(InteractionHand.MAIN_HAND)!=null && pl.getItemInHand(InteractionHand.MAIN_HAND).getItem()==ToolItems.scrench)
		{
			if(!w.isClientSide)
			{
				TileEntityWireBase wire = (TileEntityWireBase) w.getBlockEntity(pos);
				wire.getCapability(CapabilityLogistic.cap_LOGISTIC, hit.getDirection()).ifPresent(inter -> 
				{
					if(inter.getMode(EnumLogisticType.ENERGIE) != EnumLogisticIO.NONE)
					{
						inter.setMode(EnumLogisticIO.NONE, EnumLogisticType.ENERGIE);
					}
					else
					{
						inter.setMode(EnumLogisticIO.INOUT, EnumLogisticType.ENERGIE);
					}
					wire.setChanged();
					w.sendBlockUpdated(pos, Blocks.AIR.defaultBlockState(), w.getBlockState(pos), 2);
				});
			}
			else
			{
				ItemLogisticEditor.sheduledRenderUpdate(pos, w);
			}
			return InteractionResult.SUCCESS;
		}
		return super.use(state, w, pos, pl, hand, hit);
	}
	
	@Override
	public BlockState rotate(BlockState state, Rotation rot) {	
		boolean north = false;
		boolean east  = false;
		boolean south = false;
		boolean west  = false;
		
		if(rot != Rotation.NONE) {
			if(state.getValue(BlockWireBase.northB)) {
				switch(rot) {
					case CLOCKWISE_90: east = true; break;
					case CLOCKWISE_180: south = true; break;
					case COUNTERCLOCKWISE_90: west = true; break;
					default: break;
				}
			}
			if(state.getValue(BlockWireBase.eastB)) {
				switch(rot) {
					case CLOCKWISE_90: south = true; break;
					case CLOCKWISE_180: west = true; break;
					case COUNTERCLOCKWISE_90: north = true; break;
					default: break;
				}
			}
			if(state.getValue(BlockWireBase.southB)) {
				switch(rot) {
					case CLOCKWISE_90: west = true; break;
					case CLOCKWISE_180: north = true; break;
					case COUNTERCLOCKWISE_90: east = true; break;
					default: break;
				}
			}
			if(state.getValue(BlockWireBase.westB)) {
				switch(rot) {
					case CLOCKWISE_90: north = true; break;
					case CLOCKWISE_180: east = true; break;
					case COUNTERCLOCKWISE_90: south = true; break;
					default: break;
				}
			}
			
			return this.defaultBlockState().setValue(northB, north).setValue(eastB, east).setValue(southB, south).setValue(westB, west).setValue(upB, state.getValue(upB)).setValue(downB, state.getValue(downB));
		}
		
		return state;
	}	
	
	
	
}
