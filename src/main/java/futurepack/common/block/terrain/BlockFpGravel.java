package futurepack.common.block.terrain;

import net.minecraft.core.BlockPos;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.GravelBlock;
import net.minecraft.world.level.block.state.BlockState;

public class BlockFpGravel extends GravelBlock
{
	public BlockFpGravel(Block.Properties properties) 
	{
		super(properties);
	}
	
	@Override
	public int getDustColor(BlockState state, BlockGetter reader, BlockPos pos) 
	{
		return 0x904400;
	}
}
