package futurepack.common.block;

import futurepack.api.interfaces.tilentity.ITileWithOwner;
import futurepack.common.DirtyHacks;
import futurepack.depend.api.helper.HelperInventory;
import net.minecraft.core.BlockPos;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.EntityBlock;
import net.minecraft.world.level.block.RenderShape;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.material.Material;

public abstract class BlockHoldingTile extends Block implements EntityBlock
{
	protected final boolean hasNBTCustomDrops;

	public BlockHoldingTile(Block.Properties builder, boolean hasNBTCustomDrops) 
	{
		super(hasNBTCustomDrops ? noDrops(builder) : builder);
		this.hasNBTCustomDrops = hasNBTCustomDrops;
	}
	
	public BlockHoldingTile(Block.Properties builder) 
	{
		this(builder, true);
	}

	
	private static Block.Properties noDrops(Block.Properties builder)
	{
		Block.Properties copy = Block.Properties.of(Material.CAKE);
		DirtyHacks.replaceData(copy, builder, Block.Properties.class);
		copy.noDrops();
		return copy;
	}
	
	@SuppressWarnings("deprecation")
	@Override
	public void onRemove(BlockState state, Level w, BlockPos pos, BlockState newState, boolean isMoving)
	{
		if (state.getBlock() != newState.getBlock()) 
		{
			if(hasNBTCustomDrops)
				HelperInventory.dropNBT(w, pos, state);
			
			super.onRemove(state, w, pos, newState, isMoving);
			w.removeBlockEntity(pos);
		}
	}

	@Override
	public RenderShape getRenderShape(BlockState state)
    {
        return RenderShape.MODEL;
    }

	@Override
	public void setPlacedBy(Level w, BlockPos pos, BlockState state, LivingEntity liv, ItemStack it)
	{
		HelperInventory.placeNBT(w, pos, liv, it);
		
		if(liv instanceof Player)
		{
			BlockEntity tile = w.getBlockEntity(pos);
			if(tile!=null && tile instanceof ITileWithOwner)
			{
				((ITileWithOwner)tile).setOwner((Player) liv);
			}
		}
	}
	
//	@Override
//	public void harvestBlock(World worldIn, PlayerEntity player, BlockPos pos, BlockState state, @Nullable TileEntity te, ItemStack stack) //COPY from BlockContainer
//	{
//		if (te instanceof INameable && ((INameable)te).hasCustomName()) 
//		{
//			player.addStat(Stats.BLOCK_MINED.get(this));
//			player.addExhaustion(0.005F);
//			if (worldIn.isRemote) 
//			{
//				return;
//			}
//
//			int i = EnchantmentHelper.getEnchantmentLevel(Enchantments.FORTUNE, stack);
//			Item item = this.getItemDropped(state, worldIn, pos, i).asItem();
//			if (item == Items.AIR)
//			{
//				return;
//	         }
//
//			ItemStack itemstack = new ItemStack(item, this.quantityDropped(state, worldIn.rand));
//			itemstack.setDisplayName(((INameable)te).getCustomName());
//			spawnAsEntity(worldIn, pos, itemstack);
//		}
//		else
//		{
//			super.harvestBlock(worldIn, player, pos, state, (TileEntity)null, stack);
//		}
//	}

	@SuppressWarnings("deprecation")
	@Override
	public boolean triggerEvent(BlockState state, Level worldIn, BlockPos pos, int id, int param) //COPY from BlockContainer
	{
		super.triggerEvent(state, worldIn, pos, id, param);
		BlockEntity tileentity = worldIn.getBlockEntity(pos);
		return tileentity == null ? false : tileentity.triggerEvent(id, param);
	}
}
