package futurepack.common.block.inventory;

import java.util.function.Supplier;

import futurepack.api.interfaces.IBlockServerOnlyTickingEntity;
import futurepack.api.interfaces.tilentity.ITileServerTickable;
import futurepack.common.block.BlockRotateable;
import futurepack.common.sync.FPGuiHandler;
import futurepack.depend.api.helper.HelperResearch;
import net.minecraft.core.BlockPos;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.BlockHitResult;

public class BlockModul<T extends BlockEntity & ITileServerTickable> extends BlockRotateable implements IBlockServerOnlyTickingEntity<T>
{
	protected final Supplier<BlockEntityType<T>> type;
	protected final Supplier<FPGuiHandler> supGui;
	
	public BlockModul(Block.Properties props, Supplier<BlockEntityType<T>> type, Supplier<FPGuiHandler> supGui)
	{	
		super(props);
		this.type = type;
		this.supGui = supGui;
	}
	
	@Override
	public InteractionResult use(BlockState state, Level w, BlockPos pos, Player pl, InteractionHand hand, BlockHitResult hit)
	{
		if(HelperResearch.canOpen(pl, state))
		{
			supGui.get().openGui(pl, pos);
		}
		return InteractionResult.SUCCESS;
	}
		
	@Override
	public BlockEntityType<T> getTileEntityType(BlockState pState)
	{
		return type.get();
	}

}
