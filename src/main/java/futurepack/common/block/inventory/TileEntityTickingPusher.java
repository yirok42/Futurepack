package futurepack.common.block.inventory;

import futurepack.common.FPTileEntitys;
import futurepack.depend.api.interfaces.ITileInventoryProvider;
import net.minecraft.core.BlockPos;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;

public class TileEntityTickingPusher extends TileEntityPusher implements ITileInventoryProvider
{
    private int cooldown;

    public TileEntityTickingPusher(BlockEntityType<? extends TileEntityTickingPusher> type, BlockPos pos, BlockState state) 
	{
		super(type, pos, state);
	}

    public TileEntityTickingPusher(BlockPos pos, BlockState state) 
	{
		this(FPTileEntitys.PUSHER_TICKING, pos, state);
	}

    @Override
	public void tickServer(Level level, BlockPos pos, BlockState pState)
	{
    	
//    	hier sind bugs
		if(cooldown++ > 20)
		{
            cooldown = 0;
			neighborChanged(level, pos, null, true);
		}
		
		pushOutItems();
	}
}
