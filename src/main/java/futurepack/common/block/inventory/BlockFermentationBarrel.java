package futurepack.common.block.inventory;

import futurepack.api.interfaces.IBlockServerOnlyTickingEntity;
import futurepack.common.FPTileEntitys;
import futurepack.common.block.BlockRotateable;
import futurepack.common.sync.FPGuiHandler;
import futurepack.depend.api.helper.HelperResearch;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition.Builder;
import net.minecraft.world.level.block.state.properties.IntegerProperty;
import net.minecraft.world.phys.BlockHitResult;

public class BlockFermentationBarrel extends BlockRotateable implements IBlockServerOnlyTickingEntity<TileEntityFermentationBarrel>
{
	public static final IntegerProperty FILL = IntegerProperty.create("fill", 0, 7);

	public BlockFermentationBarrel(Block.Properties props) 
	{
		super(props);
	}
	
	@Override
	public InteractionResult use(BlockState state, Level worldIn, BlockPos pos, Player pl, InteractionHand handIn, BlockHitResult hit) 
	{
		if(HelperResearch.canOpen(pl, state))
		{
			FPGuiHandler.FERMENTATION_BARREL.openGui(pl, pos);;
		}
		return InteractionResult.SUCCESS;
	}

	@Override
	protected void createBlockStateDefinition(Builder<Block, BlockState> builder) 
	{
		super.createBlockStateDefinition(builder);
		builder.add(FILL);
	}
	
	@Override
	public BlockState getStateForPlacement(BlockPlaceContext context)
	{
		Direction face = context.getHorizontalDirection();
		return super.getStateForPlacement(context).setValue(HORIZONTAL_FACING, face);
	}

	@Override
	public BlockEntityType<TileEntityFermentationBarrel> getTileEntityType(BlockState pState)
	{
		return FPTileEntitys.FERMENTATION_BARREL;
	}
	
}
