package futurepack.common.block.inventory;

import futurepack.common.FPTileEntitys;
import net.minecraft.core.BlockPos;
import net.minecraft.world.level.block.state.BlockState;

public class TileEntityAdvancedBoardComputer extends TileEntityBoardComputer
{
	public TileEntityAdvancedBoardComputer(BlockPos pos, BlockState state) 
	{
		super(FPTileEntitys.ADVANCED_BOARD_COMPUTER, pos, state);
	}
	
	@Override
	public boolean isAdvancedBoardComputer()
	{
		return true;
	}
	
	@Override
	public int getMaxNE()
	{
		return 1500;
	}
}
