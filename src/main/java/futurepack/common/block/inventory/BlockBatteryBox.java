package futurepack.common.block.inventory;

import futurepack.api.interfaces.IBlockServerOnlyTickingEntity;
import futurepack.common.FPTileEntitys;
import futurepack.common.block.BlockHoldingTile;
import futurepack.common.sync.FPGuiHandler;
import futurepack.depend.api.helper.HelperResearch;
import net.minecraft.core.BlockPos;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.AbstractContainerMenu;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition.Builder;
import net.minecraft.world.level.block.state.properties.IntegerProperty;
import net.minecraft.world.phys.BlockHitResult;

public class BlockBatteryBox extends BlockHoldingTile implements IBlockServerOnlyTickingEntity<TileEntityBatteryBox>
{	
	public static final IntegerProperty CHARGE = IntegerProperty.create("charge", 0, 4);
	
	protected BlockBatteryBox(Block.Properties props)
	{
		super(props);
	}
	
	@Override
	public InteractionResult use(BlockState state, Level w, BlockPos pos, Player pl, InteractionHand hand, BlockHitResult hit)
	{
		if(HelperResearch.canOpen(pl, state))
		{
			FPGuiHandler.BATTERY_BOX.openGui(pl, pos);
		}
		return InteractionResult.SUCCESS;
	}

	@Override
	protected void createBlockStateDefinition(Builder<Block, BlockState> builder)
	{
		super.createBlockStateDefinition(builder);
		builder.add(CHARGE);
	}
	
	@Override
	public void setPlacedBy(Level w, BlockPos pos, BlockState state, LivingEntity liv, ItemStack it)
	{
		super.setPlacedBy(w, pos, state, liv, it);
		
		BlockEntity tile = w.getBlockEntity(pos);
		if(tile instanceof TileEntityBatteryBox)
		{
			((TileEntityBatteryBox) tile).energy.use(0);
		}
	}

	@Override
	public BlockEntityType<TileEntityBatteryBox> getTileEntityType(BlockState pState) 
	{
		return FPTileEntitys.BATTERY_BOX;
	}
	

	@Override
	public boolean hasAnalogOutputSignal(BlockState pState) 
	{
		return true;
	}

	@Override
	public int getAnalogOutputSignal(BlockState pBlockState, Level pLevel, BlockPos pPos)
	{
		if(pLevel.getBlockEntity(pPos) instanceof TileEntityBatteryBox bat )
		{
			return (int) (15 * bat.energy.getFilled());
		}
		return super.getAnalogOutputSignal(pBlockState, pLevel, pPos);
	}
}
