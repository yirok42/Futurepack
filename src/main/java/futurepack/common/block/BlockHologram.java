package futurepack.common.block;

import futurepack.api.interfaces.tilentity.ITileHologramAble;
import net.minecraft.core.BlockPos;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.VoxelShape;

public abstract class BlockHologram extends BlockHoldingTile
{

	public BlockHologram(Block.Properties builder) 
	{
		super(builder);
	}
	
	public BlockHologram(Block.Properties builder, boolean hasNBTCustomDrops) 
	{
		super(builder, hasNBTCustomDrops);
	}
	
//	@Override
//	public boolean isTopSolid(BlockState state, IWorldReader world, BlockPos pos)
//	{
//		TileEntity t = world.getTileEntity(pos);
//		if(t instanceof ITileHologramAble)
//		{
//			ITileHologramAble holo = (ITileHologramAble) t;
//			if(holo.hasHologram())
//			{
//				try
//				{
//					return holo.getHologram().isTopSolid(world, pos);
//				}
//				catch(ClassCastException e){/*Shit happens ;P*/}
//				catch(IllegalArgumentException e){/*Shit happens ;P*/}
//			}
//		}
//		return super.isTopSolid(state, world, pos);
//	}
	
	@Override
	public VoxelShape getShape(BlockState state, BlockGetter w, BlockPos pos, CollisionContext sel)
	{
		BlockEntity t = w.getBlockEntity(pos);
		if(t instanceof ITileHologramAble)
		{
			ITileHologramAble holo = (ITileHologramAble) t;
			if(holo.hasHologram())
			{
				try
				{
					if(holo.hasHologram() && holo.getHologram() != state)
						return holo.getHologram().getShape(w, pos, sel);
				}
				catch(ClassCastException e){/*Shit happens ;P*/}
				catch(IllegalArgumentException e){/*Shit happens ;P*/}
				catch(NullPointerException e) {/*Shit happens ;P*/}
			}
		}
		return super.getShape(state, w, pos, sel);
	}
	
	@Override
	public VoxelShape getCollisionShape(BlockState state, BlockGetter w, BlockPos pos, CollisionContext sel)
	{
		BlockEntity t = w.getBlockEntity(pos);
		if(t instanceof ITileHologramAble)
		{
			ITileHologramAble holo = (ITileHologramAble) t;
			if(holo.hasHologram())
			{
				try
				{
					if(holo.getHologram() != state)
						return holo.getHologram().getCollisionShape(w, pos, sel);
				}
				catch(ClassCastException e){/*Shit happens ;P*/}
				catch(IllegalArgumentException e){/*Shit happens ;P*/}
				catch(NullPointerException e) {/*Shit happens ;P*/}
			}
		}
		return super.getCollisionShape(state, w, pos, sel);
	}
	
	@Override
	public VoxelShape getOcclusionShape(BlockState state, BlockGetter w, BlockPos pos)
	{
		BlockEntity t = w.getBlockEntity(pos);
		if(t instanceof ITileHologramAble)
		{
			ITileHologramAble holo = (ITileHologramAble) t;
			if(holo.hasHologram())
			{
				try
				{
					if(holo.getHologram() != state)
						return holo.getHologram().getBlockSupportShape(w, pos);
				}
				catch(ClassCastException e){/*Shit happens ;P*/}
				catch(IllegalArgumentException e){/*Shit happens ;P*/}
				catch(NullPointerException e) {/*Shit happens ;P*/}
			}
		}
		return super.getOcclusionShape(state, w, pos);
	}
	
	@Override
	public VoxelShape getInteractionShape(BlockState state, BlockGetter w, BlockPos pos)
	{
		BlockEntity t = w.getBlockEntity(pos);
		if(t instanceof ITileHologramAble)
		{
			ITileHologramAble holo = (ITileHologramAble) t;
			if(holo.hasHologram())
			{
				try
				{
					if(holo.getHologram() != state)
						return holo.getHologram().getVisualShape(w, pos, CollisionContext.empty());
				}
				catch(ClassCastException e){/*Shit happens ;P*/}
				catch(IllegalArgumentException e){/*Shit happens ;P*/}
				catch(NullPointerException e) {/*Shit happens ;P*/}
			}
		}
		return super.getInteractionShape(state, w, pos);
	}
	
//	@Override
//	public BlockFaceShape getBlockFaceShape(IBlockReader w, BlockState state, BlockPos pos, Direction face)
//	{
//		TileEntity t = w.getTileEntity(pos);
//		if(t instanceof ITileHologramAble)
//		{
//			ITileHologramAble holo = (ITileHologramAble) t;
//			if(holo.hasHologram())
//			{
//				try
//				{
//					return holo.getHologram().getBlockFaceShape(w, pos, face);
//				}
//				catch(ClassCastException e){/*Shit happens ;P*/}
//				catch(IllegalArgumentException e){/*Shit happens ;P*/}
//			}
//		}
//		return super.getBlockFaceShape(w, state, pos, face);
//	}
}	

