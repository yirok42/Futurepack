package futurepack.common.block.deco;

import net.minecraft.core.BlockPos;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.DoorBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.BlockHitResult;

public class BlockCompositeDoor extends DoorBlock
{

	public BlockCompositeDoor(Block.Properties props)
	{
		super(props);
	}
	
	private int getCloseSound()
    {
        return 1011;//metal door
    }

    private int getOpenSound()
    {
        return 1005;//metal door
    }
	
	@Override
	public InteractionResult use(BlockState state, Level worldIn, BlockPos pos, Player player, InteractionHand hand, BlockHitResult hit)
    {
		state = state.cycle(OPEN);
        worldIn.setBlock(pos, state, 10);
        worldIn.levelEvent(player, state.getValue(OPEN) ? this.getOpenSound() : this.getCloseSound(), pos, 0);
        return InteractionResult.SUCCESS;
    }
	
}
