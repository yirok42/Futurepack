package futurepack.common.block.deco;

import java.util.Random;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.core.particles.ParticleOptions;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LevelAccessor;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.DirectionalBlock;
import net.minecraft.world.level.block.Mirror;
import net.minecraft.world.level.block.Rotation;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition.Builder;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.level.block.state.properties.BooleanProperty;
import net.minecraft.world.level.block.state.properties.DirectionProperty;

public class BlockThruster extends DirectionalBlock
{
//	IIcon[][] cols = new IIcon[3][5];	
//	IIcon[] off = new IIcon[3];
//	private static final IntegerProperty backcol = IntegerProperty.create("bg", 0, 2);
//	private static final IntegerProperty forecol = IntegerProperty.create("fg", 0, 4);
	
	public static final BooleanProperty powered = BlockStateProperties.POWERED;
	public static final DirectionProperty FACING = DirectionalBlock.FACING;
	
	int[] cols = new int[]{0x47fafa, 0xf9d647, 0x31fc22, 0xf28b06, 0x8943d7};
	
	public BlockThruster(Block.Properties props)
	{
		super(props);
		registerDefaultState(this.stateDefinition.any().setValue(powered, false));
	}
	
	@Override
	protected void createBlockStateDefinition(Builder<Block, BlockState> builder)
	{
		builder.add(FACING, powered);
		super.createBlockStateDefinition(builder);
	}
	
	@Override
	public void neighborChanged(BlockState state, Level w, BlockPos pos, Block blockIn, BlockPos fromPos, boolean isMoving) 
	{
		boolean bb = w.getBestNeighborSignal(pos) > 0;
		w.setBlock(pos, state.setValue(powered, bb), 3);
	}
	
	@Override
	public void animateTick(BlockState state, Level w, BlockPos pos, Random rand)
	{
		if(state.getValue(powered))
		{
			Direction f = state.getValue(FACING);
			ParticleOptions red = ParticleTypes.SMOKE;
			w.addParticle(red, pos.getX()+w.random.nextFloat(), pos.getY()+w.random.nextFloat(), pos.getZ()+w.random.nextFloat(), f.getStepX()/2D*w.random.nextFloat(),f.getStepY()/2D*w.random.nextFloat(),f.getStepZ()/2D*w.random.nextFloat());
	        w.addParticle(red, pos.getX()+w.random.nextFloat(), pos.getY()+w.random.nextFloat(), pos.getZ()+w.random.nextFloat(), f.getStepX()/2D*w.random.nextFloat(),f.getStepY()/2D*w.random.nextFloat(),f.getStepZ()/2D*w.random.nextFloat());
		
		}
		super.animateTick(state, w, pos, rand);
	}
	
	@Override
	public BlockState rotate(BlockState state, LevelAccessor world, BlockPos pos, Rotation direction)
	{
		return rotate(state, direction);
	}
	
	@Override
	public BlockState rotate(BlockState state, Rotation rot)
	{
		return state.setValue(FACING, rot.rotate(state.getValue(FACING)));
	}
	
	@Override
	public BlockState mirror(BlockState state, Mirror mirrorIn)
	{
		return state.setValue(FACING, mirrorIn.mirror(state.getValue(FACING)));
	}
	
	@Override
	public BlockState getStateForPlacement(BlockPlaceContext context)
	{
		Direction face = context.getNearestLookingDirection().getOpposite();
		return super.getStateForPlacement(context).setValue(FACING, face);
	}
}
