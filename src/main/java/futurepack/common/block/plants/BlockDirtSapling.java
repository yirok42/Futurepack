package futurepack.common.block.plants;

import net.minecraft.world.level.block.SaplingBlock;
import net.minecraft.world.level.block.grower.AbstractTreeGrower;

public class BlockDirtSapling extends SaplingBlock
{

	public BlockDirtSapling(AbstractTreeGrower p_i48337_1_, Properties properties)
	{
		super(p_i48337_1_, properties);
	}

}
