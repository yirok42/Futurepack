package futurepack.common.block.misc;

import futurepack.api.capabilities.CapabilityNeon;
import futurepack.api.capabilities.IEnergyStorageBase.EnumEnergyMode;
import futurepack.api.capabilities.INeonEnergyStorage;
import futurepack.api.interfaces.tilentity.ITileServerTickable;
import futurepack.common.FPTileEntitys;
import futurepack.common.block.BlockRotateableTile;
import futurepack.common.block.FPTileEntityBase;
import futurepack.common.sync.FPPacketHandler;
import futurepack.depend.api.helper.HelperEnergyTransfer;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.energy.CapabilityEnergy;
import net.minecraftforge.energy.EnergyStorage;
import net.minecraftforge.energy.IEnergyStorage;

public class TileEntityNeonEngine extends FPTileEntityBase implements ITileServerTickable
{

	public boolean working = false;
	private CapabilityNeon power = new CapabilityNeon(10000, EnumEnergyMode.USE);
	private EnergyStorage dummy_rf = new EnergyStorage(1, 0, 0, 0);
	
	public EnumHeat heatState = EnumHeat.OFF;
	public EnumHeat lastState = null;
	private int heat;
	
	
	public TileEntityNeonEngine(BlockPos pos, BlockState state) 
	{
		super(FPTileEntitys.NEON_ENGINE, pos, state);
	}
	
	private LazyOptional<INeonEnergyStorage> neon_opt;
	private LazyOptional<IEnergyStorage> local_rf_opt;
	private LazyOptional<IEnergyStorage> rf_opt;
	
	@Override
	public void tickServer(Level level, BlockPos worldPosition, BlockState state)
	{
		if(heat>0)
			heat--;
		
		working = level.getBestNeighborSignal(worldPosition) > 0;
		if((working||heat>0) && power.get()>=40)
		{
			if(rf_opt!=null)
			{
				rf_opt.ifPresent(storage -> {
					int used = storage.receiveEnergy(power.get()*2, false);
					power.use(used/2);
					if(heat<2000 && working)
						heat+=2;
				});
			}
			else
			{
				Direction dir = state.getValue(BlockRotateableTile.FACING);
				BlockPos xyz = worldPosition.relative(dir);
				BlockEntity tile = level.getBlockEntity(xyz);
				if(tile!=null)
				{
					rf_opt = tile.getCapability(CapabilityEnergy.ENERGY, dir.getOpposite());
					rf_opt.addListener(p -> rf_opt = null);
				}
			}
		}
		if(heat>1900)
			heatState =EnumHeat.HOT;
		else if(heat>1200)
			heatState = EnumHeat.WARM;
		else if(heat>500)
			heatState = EnumHeat.MEDIUM;
		else if(heat>0)
			heatState = EnumHeat.COLD;
		else
			heatState = EnumHeat.OFF;
		
		if(lastState != heatState)
		{
			lastState = heatState;
			
			FPPacketHandler.sendTileEntityPacketToAllClients(this);
		}
	}

	
	@SuppressWarnings("unchecked")
	@Override
	public <T> LazyOptional<T> getCapability(Capability<T> capability, Direction facing)
	{
		if(capability == CapabilityNeon.cap_NEON)
		{
			if(neon_opt!=null)
				return (LazyOptional<T>) neon_opt;
			else
			{
				neon_opt = LazyOptional.of(() -> power);
				neon_opt.addListener(p -> neon_opt = null);
				return (LazyOptional<T>) neon_opt;
			}
		}
		else if(capability == CapabilityEnergy.ENERGY && facing == getBlockState().getValue(BlockRotateableTile.FACING))
		{
			if(local_rf_opt!=null)
				return (LazyOptional<T>) local_rf_opt;
			else
			{
				local_rf_opt = LazyOptional.of(() -> dummy_rf);
				local_rf_opt.addListener(p -> local_rf_opt = null);
				return (LazyOptional<T>) local_rf_opt;
			}
		}
		return super.getCapability(capability, facing);
	}
	
	@Override
	public void setRemoved() 
	{
		HelperEnergyTransfer.invalidateCaps(neon_opt);
		super.setRemoved();
	}
	

	

	


	@Override
	public CompoundTag writeDataUnsynced(CompoundTag nbt)
	{
		super.writeDataUnsynced(nbt);
		nbt.put("energy", power.serializeNBT());
		nbt.putInt("heat", heat);
		return nbt;
	}
	
	@Override
	public void readDataUnsynced(CompoundTag nbt)
	{
		super.readDataUnsynced(nbt);
		power.deserializeNBT(nbt.getCompound("energy"));
		heat = nbt.getInt("heat");
	}

//	@Optional.Method(modid = "CoFHCore")
//	@Override
//	public int receiveEnergy(EnumFacing paramForgeDirection, int paramInt, boolean paramBoolean)
//	{
//		return 0;
//	}
	
	public enum EnumHeat
	{
		OFF(0F, 0, "futurepack:textures/model/engine_2_schwarz.png"),
		COLD(0.001F, 100, "futurepack:textures/model/engine_2.png"),
		MEDIUM(0.002F, 250,"futurepack:textures/model/engine_2_grun.png"),
		WARM(0.003F, 500, "futurepack:textures/model/engine_2_orange.png"),
		HOT(0.004F, 1000, "futurepack:textures/model/engine_2_rot.png");
		
		private float speed;
		private int maxRF;
		private ResourceLocation tex;
		
		EnumHeat(float speed, int maxRF, String root)
		{
			this.speed = speed;
			this.maxRF = maxRF;
			tex = new ResourceLocation(root);
		}
		
		public ResourceLocation getTexture()
		{
			return tex;
		}
		
		public float getSpeed()
		{
			return speed;
		}
		
		public int getMaxRF()
		{
			return maxRF;
		}
	}
}
