package futurepack.common.block.misc;

import futurepack.common.FPConfig;
import net.minecraft.ChatFormatting;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TextComponent;
import net.minecraft.world.item.BlockItem;
import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.block.Block;

public class ItemWIP extends BlockItem 
{

	public ItemWIP(Block blockIn, Properties builder) 
	{
		super(blockIn, builder);
	}
	
	@Override
	protected boolean allowdedIn(CreativeModeTab group) 
	{
		if(FPConfig.IS_RESEARCH_DEBUG.getAsBoolean())
			return super.allowdedIn(group);
		return false;
	}
	
	@Override
	public Component getName(ItemStack it) 
	{
		return new TextComponent(ChatFormatting.RED + "Only Dev " + ChatFormatting.RESET).append(super.getName(it));
	}
}
