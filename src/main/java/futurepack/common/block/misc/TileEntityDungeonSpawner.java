package futurepack.common.block.misc;

import java.util.ArrayList;
import java.util.stream.Stream;

import futurepack.api.interfaces.tilentity.ITileServerTickable;
import futurepack.common.FPTileEntitys;
import futurepack.common.block.FPTileEntityBase;
import futurepack.common.dim.structures.enemys.EnemyEntry;
import futurepack.common.dim.structures.enemys.EnemyWave;
import futurepack.common.dim.structures.enemys.WaveLoader;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.network.protocol.game.ClientboundSetSubtitleTextPacket;
import net.minecraft.network.protocol.game.ClientboundSetTitleTextPacket;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.state.BlockState;

public class TileEntityDungeonSpawner extends FPTileEntityBase implements ITileServerTickable
{
	public static final int range = 3;
	
	static Component start = new TranslatableComponent("dungeon.message.start");
	static Component cleared = new TranslatableComponent("dungeon.message.cleared");
	static Component finished = new TranslatableComponent("dungeon.message.finished");
	
	private ArrayList<EnemyWave> waves = new ArrayList<EnemyWave>();
	
	private boolean solved = false;
	private boolean aktiv = false;
	private int waveNum = 0;
	private int coolDown = -1;
	private int redstonePower = 5;
	
	private boolean firstTick = true;
	
	public TileEntityDungeonSpawner(BlockPos pos, BlockState state)
	{
		super(FPTileEntitys.DUNGEON_SPAWNER, pos, state);
		waves.add(WaveLoader.createWaveFromId("default"));
	}
	
	@Override
	public void onChunkUnloaded()
	{
		reset();
		super.onChunkUnloaded();
	}

	
	@Override
	public void tickServer(Level level, BlockPos worldPosition, BlockState pState)
	{
		if(!level.isClientSide)
		{
			if(firstTick)
			{
				level.updateNeighborsAt(worldPosition, getBlockState().getBlock());
				firstTick=false;
			}
			if(!solved)
			{
				Player pl = level.getNearestPlayer(worldPosition.getX() +0.5, worldPosition.getY()+0.5, worldPosition.getZ()+0.5, range, false);
				if(pl!=null && !aktiv)
				{
					start();
				}
				else if(aktiv)
				{
					nextWave();
				}
			}
		}
	}	
	
	public void reset()
	{
		if(aktiv)
		{
			solved = false;
			waveNum = 0;
			coolDown = -1;
			aktiv = false;
			
			waves.forEach(EnemyWave::clear);
		}		
	}
	
	public void start()
	{
		if(!aktiv && !waves.isEmpty())
		{
			aktiv = true;
			waveNum = 0;
			coolDown = -1;
			Direction face = level.getBlockState(worldPosition).getValue(BlockDungeonSpawner.FACING);
			
			level.updateNeighborsAt(worldPosition, getBlockState().getBlock());
			
			waves.get(waveNum).startWave(level, worldPosition.relative(face));
			broadcastMessage(start, waves.get(waveNum).subtitle);
		}
	}
	
	public void broadcastMessage(Component title, Component subtitle)
	{
		ClientboundSetSubtitleTextPacket subt = new ClientboundSetSubtitleTextPacket(subtitle);
		ClientboundSetTitleTextPacket titl = new ClientboundSetTitleTextPacket(title);
		
		int r = 128;
		Stream<ServerPlayer> list = level.players().stream().filter(p -> p instanceof ServerPlayer).filter(pl -> worldPosition.distSqr(pl.blockPosition()) <= r*r).map(p -> (ServerPlayer)p);
		list.forEach(mp -> {
			if(subtitle!=null)
				mp.connection.send(subt);
			mp.connection.send(titl);
		});
	}
	
	public void nextWave()
	{
		if(aktiv)
		{
			if(waves.isEmpty())
			{
				reset();
				return;
			}
			
			if(waveNum >= waves.size())
			{
				aktiv = false;
				solved = true;
				setChanged();
				level.updateNeighborsAt(worldPosition, getBlockState().getBlock());
			}
			else if(waves.get(waveNum).isComplete(level))
			{
				if(coolDown<0)
				{
					broadcastMessage(cleared, null);
				}
					
				if(coolDown>= waves.get(waveNum).getCoolDown())
				{
					waveNum++;
					if(waveNum < waves.size())
					{
						waves.get(waveNum).startWave(level, worldPosition);
						broadcastMessage(start, waves.get(waveNum).subtitle);
						coolDown = -1;
					}
					else
					{
						solved = true;
						aktiv = false;
						setChanged();
						level.updateNeighborsAt(worldPosition, getBlockState().getBlock());
						broadcastMessage(finished, null);
					}
				}	
				else
				{
					coolDown++;
				}
			}
		}
	}

	@Override
	public CompoundTag writeDataUnsynced(CompoundTag nbt)
	{
		super.writeDataUnsynced(nbt);
		nbt.putBoolean("solved", solved);
		nbt.putBoolean("aktiv", aktiv);
		nbt.putInt("waveNum", waveNum);
		nbt.putInt("coolDown", coolDown);
		nbt.putInt("redpower", redstonePower);
		
		ListTag list = new ListTag();
		for(EnemyWave w : waves)
		{
			list.add(w.serializeNBT());
		}
		nbt.put("waves", list);
		
		return nbt;
	}
	
	@Override
	public void readDataUnsynced(CompoundTag nbt)
	{
		solved = nbt.getBoolean("solved");
		aktiv = nbt.getBoolean("aktiv");
		waveNum = nbt.getInt("waveNum");
		coolDown = nbt.getInt("coolDown");
		redstonePower = nbt.getInt("redpower");
		
		waves = new ArrayList<EnemyWave>();
		ListTag list = nbt.getList("waves", 10);
		for(int i=0;i<list.size();i++)
		{
			waves.add(WaveLoader.createWaveFromNBT(list.getCompound(i)));
		}
		super.readDataUnsynced(nbt);
	}
	
	public int getRedstonePower()
	{
		return solved ? redstonePower : 0;
	}
	
	public int getWaveCount()
	{
		return waves.size();
	}
	
	
	private int[][] waveColors = null;
	
	public int[] getWaveColors(int wave)
	{
		if(waveColors==null)
		{
			waveColors = new int[getWaveCount()][];
			for(int i=0;i<waveColors.length;i++)
			{
				EnemyWave wav = waves.get(i);
				float total = wav.getEnemyTotal();
				waveColors[i] = new int[wav.enemys.size()];
				for(int j=0;j<waveColors[i].length;j++)
				{
					EnemyEntry ent = wav.enemys.get(j);
					waveColors[i][j] = ent.getDangerColor(ent.getEnemyCount() / total);
				}
			}
		}		
		return waveColors[wave];		
	}
	
	public void addWaves(String[] waves )
	{
		this.waves.clear();
		for(String wave : waves)
		{
			this.waves.add(WaveLoader.createWaveFromId(wave));
		}
		
	}
}
