/*
 * Minecraft Forge
 * Copyright (c) 2016-2021.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation version 2.1
 * of the License.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

package futurepack.depend.api;

import net.minecraft.client.renderer.LevelRenderer;
import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.Tag;
import net.minecraft.nbt.TagType;
import net.minecraft.nbt.TagTypes;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LevelAccessor;
import net.minecraft.world.level.LevelWriter;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.chunk.LevelChunk;

/**
 * A class containing constants for magic numbers used in the minecraft codebase.
 * Everything here should be checked each update, and have a comment relating to where to check it.
 *
 */

public class StableConstants
{
    /**
     * NBT Tag type IDS, used when storing the nbt to disc, Should align with {@link TagTypes#getType(int)}
     * and {@link TagType#getPrettyName()}
     *
     * Main use is checking tag type in {@link CompoundTag#contains(String, int)}
     *
     */
    
    public static class NBT
    {
        public static final int TAG_END         = Tag.TAG_END;
        public static final int TAG_BYTE        = Tag.TAG_BYTE;
        public static final int TAG_SHORT       = Tag.TAG_SHORT;
        public static final int TAG_INT         = Tag.TAG_INT;
        public static final int TAG_LONG        = Tag.TAG_LONG;
        public static final int TAG_FLOAT       = Tag.TAG_FLOAT;
        public static final int TAG_DOUBLE      = Tag.TAG_DOUBLE;
        public static final int TAG_BYTE_ARRAY  = Tag.TAG_BYTE_ARRAY;
        public static final int TAG_STRING      = Tag.TAG_STRING;
        public static final int TAG_LIST        = Tag.TAG_LIST;
        public static final int TAG_COMPOUND    = Tag.TAG_COMPOUND;
        public static final int TAG_INT_ARRAY   = Tag.TAG_INT_ARRAY;
        public static final int TAG_LONG_ARRAY  = Tag.TAG_LONG_ARRAY;
        public static final int TAG_ANY_NUMERIC = Tag.TAG_ANY_NUMERIC;
    }


    /**
     * The flags used when calling
     * {@link LevelWriter#setBlock(BlockPos, BlockState, int)}<br>
     * Can be found from {@link Level#setBlock(BlockPos, BlockState, int)} ,
     * {@link Level#markAndNotifyBlock(BlockPos, LevelChunk, BlockState, BlockState, int, int)}, and
     * {@link LevelRenderer#blockChanged(BlockGetter, BlockPos, BlockState, BlockState, int)}<br>
     * Flags can be combined with bitwise OR
     */
    
    public static class BlockFlags {
        /**
         * Calls {@link Block#neighborChanged(BlockState, Level, BlockPos, Block, BlockPos, boolean)
         * neighborChanged} on surrounding blocks (with isMoving as false). Also updates comparator output state.
         */
        public static final int NOTIFY_NEIGHBORS     = Block.UPDATE_NEIGHBORS;
        /**
         * Calls {@link Level#sendBlockUpdated(BlockPos, BlockState, BlockState, int)}.<br>
         * Server-side, this updates all the path-finding navigators.
         */
        public static final int BLOCK_UPDATE         = Block.UPDATE_CLIENTS;
        /**
         * Stops the blocks from being marked for a render update
         */
        public static final int NO_RERENDER          = Block.UPDATE_INVISIBLE;
        /**
         * Makes the block be re-rendered immediately, on the main thread.
         * If NO_RERENDER is set, then this will be ignored
         */
        public static final int RERENDER_MAIN_THREAD = Block.UPDATE_IMMEDIATE;
        /**
         * Causes neighbor updates to be sent to all surrounding blocks (including
         * diagonals). This in turn will call
         * {@link Block#updateIndirectNeighbourShapes(BlockState, LevelAccessor, BlockPos, int, int)}
         * on both old and new states, and
         * {@link Block#updateOrDestroy(BlockState, BlockState, LevelAccessor, BlockPos, int, int)} on the new state.
         */
        public static final int UPDATE_NEIGHBORS     = Block.UPDATE_KNOWN_SHAPE;

        /**
         * Prevents neighbor changes from spawning item drops, used by
         * {@link Block#updateOrDestroy(BlockState, BlockState, LevelAccessor, BlockPos, int)}.
         */
        public static final int NO_NEIGHBOR_DROPS    = Block.UPDATE_SUPPRESS_DROPS;

        /**
         * Tell the block being changed that it was moved, rather than removed/replaced,
         * the boolean value is eventually passed to
         * {@link Block#onRemove(BlockState, Level, BlockPos, BlockState, boolean)}
         * as the last parameter.
         */
        public static final int IS_MOVING            = Block.UPDATE_MOVE_BY_PISTON;

        public static final int DEFAULT = Block.UPDATE_ALL;
        public static final int DEFAULT_AND_RERENDER = Block.UPDATE_ALL_IMMEDIATE;
    }
}
