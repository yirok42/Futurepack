package futurepack.depend.api.helper;

import java.util.Arrays;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.world.level.LevelReader;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.phys.AABB;
import net.minecraft.world.phys.Vec3;
import net.minecraft.world.phys.shapes.Shapes;
import net.minecraft.world.phys.shapes.VoxelShape;

public class HelperBoundingBoxes 
{
	public static AABB rotate(AABB box, float x, float y)
	{
		Vec3 min = new Vec3(box.minX, box.minY, box.minZ);
		Vec3 max = new Vec3(box.maxX, box.maxY, box.maxZ);
		min = rotate(min, x, y);
		max = rotate(max, x, y);
		return new AABB(min, max);
	}
	
	/**
	 * 
	 * @param point
	 * @param x rotation around X axis in degress
	 * @param y rotation around Y axis in degress 
	 * @return
	 */
	public static Vec3 rotate(Vec3 point, float x, float y)
	{
		return point.xRot((float) (x/180.0*Math.PI) ).yRot((float) (y/180.0*Math.PI));
	}
	
	public static AABB[] rotate(float x, float y, AABB...box)
	{
		AABB[] out = new AABB[box.length];
		for(int i=0;i<out.length;i++)
		{
			out[i] = rotate(box[i], x, y);
		}
		return out;
	}
	
	public static VoxelShape createShape(AABB...boxes)
	{
		VoxelShape[] out = new VoxelShape[boxes.length];
		for(int i=0;i<out.length;i++)
		{
			out[i] = Shapes.create(boxes[i]);
		}
		return createShape(out);
	}
	
	public static VoxelShape createShape(VoxelShape...boxes)
	{
		if(boxes.length==0)
			return null;
		else if(boxes.length==1)
		{
			return boxes[0];
		}
		else
		{
			return Shapes.or(boxes[0], boxes);
		}
	}
	
	public static AABB[] fromBoxes(double[][] boxes)
	{
		AABB[] put = new AABB[boxes.length];
		for(int i=0;i<put.length;i++)
		{
			if(boxes[i].length!=6)
				throw new IllegalArgumentException("at pos " + i + ": " + Arrays.toString(boxes[i]));
			else
			{
				put[i] = new AABB(boxes[i][0], boxes[i][1], boxes[i][2], boxes[i][3], boxes[i][4], boxes[i][5]);
			}
		}
		return put;
	}
	
	public static double[][] scale(double[][] boxes, float factor)
	{
		for(int i=0;i<boxes.length;i++)
		{
			for(int j=0;j<boxes[i].length;j++)
			{
				boxes[i][j] *= factor;
			}
				
		}
		return boxes;
	}
	
	public static AABB[] move(AABB[] boxes, double x, double y, double z)
	{
		AABB[] put = new AABB[boxes.length];
		for(int i=0;i<put.length;i++)
		{
			put[i] = boxes[i].move(x, y, z);
		}
		return put;
	}
	
	/**
	 * 
	 * @param boxes
	 * @param x rotation around X axis in degress
	 * @param y rotation around Y axis in degress 
	 * @return
	 */
	public static VoxelShape createBlockShape(double[][] boxes, float x, float y)
	{
		AABB[] bb = fromBoxes(scale(boxes, 1F/16F));
		bb = move(bb, -0.5, -0.5, -0.5);
		bb = rotate(x, y, bb);
		bb = move(bb, 0.5, 0.5, 0.5);
		return createShape(bb);
	}
	
	public static boolean isSideSolid(LevelReader worldIn, BlockPos pos, Direction directionIn)
	{
		return Block.canSupportCenter(worldIn, pos, directionIn);
	}

	public static BlockPos[][][] splitToBlocks(AABB bb) 
	{
		int w = (int) bb.getXsize();
		int h = (int) bb.getYsize();
		int d = (int) bb.getZsize();
		
		if(w==0 && bb.getXsize() > 0)
		{
			w=1;
		}
		if(h==0 && bb.getYsize() > 0)
		{
			h=1;
		}
		if(d==0 && bb.getZsize() > 0)
		{
			d=1;
		}
		
		BlockPos[][][] pos = new BlockPos[w][h][d];
		BlockPos zero = new BlockPos(bb.minX, bb.minY, bb.minZ);
		for(int x=0;x<w;x++)
		{
			for(int y=0;y<h;y++)
			{
				for(int z=0;z<d;z++)
				{
					pos[x][y][z] = zero.offset(x,y,z);
				}
			}
		}
		
		return pos;
	}
}
