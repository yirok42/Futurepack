package futurepack.depend.api.helper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Stream;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

import futurepack.api.ItemPredicateBase;
import futurepack.api.helper.HelperTags;
import futurepack.common.block.inventory.TileEntityTechtable;
import futurepack.depend.api.ItemPredicate;
import futurepack.depend.api.ListPredicate;
import futurepack.depend.api.NullPredicate;
import futurepack.depend.api.VanillaTagPredicate;
import futurepack.dev.RecipeJSONGenerator;
import net.minecraft.core.NonNullList;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TextComponent;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.tags.ItemTags;
import net.minecraft.tags.Tag;
import net.minecraft.tags.TagKey;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.block.Blocks;
import net.minecraftforge.registries.ForgeRegistries;

public class HelperJSON
{
	private static HashMap<String, Integer> variables = new HashMap<String, Integer>();
	
	public static void addVar(String name, int val)
	{
		variables.put(name, val);
	}
	
	/**
	 * Examples for valid JSON
	 * 
	 * Item: "OreDict:ingotIron"
	 * Item: apple
	 * Item: {"name":dirt, size:20, meta:2}
	 * Item:["apple","dirt","OreDict:dustNeon",{"name":"stone","size":2}, ....]
	 * 
	 * @param fakeEntrys if true fake items with the oredict names will added to empty lists
	 */
	public static ArrayList<ItemStack> getItemFromJSON(JsonElement elm, boolean fakeEntrys)
	{
		if(elm==null || elm.isJsonNull())
			return new ArrayList<ItemStack>();
		if(elm.isJsonArray() && elm.getAsJsonArray().size()==0)
			return new ArrayList<ItemStack>();
		if(elm.isJsonObject() && elm.getAsJsonObject().entrySet().isEmpty())
			return new ArrayList<ItemStack>();
		
		//System.out.println("tray Parsing JSON " + elm);
		if(elm.isJsonPrimitive())
		{
			JsonPrimitive prim = elm.getAsJsonPrimitive();
			String s = prim.getAsString();
			if(s.startsWith("tag:"))
			{
				HelperOreDict.waitForTagsToLoad();
				s = s.substring("tag:".length(), s.length());
				//System.err.println(s);
				ArrayList<ItemStack> stack = new ArrayList<ItemStack>();
//				INamedTag<Item> tag = ItemTags.makeWrapperTag(s);
				Stream<Item> values = HelperTags.getValues(ItemTags.create(new ResourceLocation(s)));
				values.map(ItemStack::new).forEach(stack::add);
				if(stack.isEmpty())
				{
					if(fakeEntrys)
					{
						ItemStack it = new ItemStack(Blocks.STONE);
						it.setHoverName(new TextComponent("Any "+ s));
						stack.add(it);
					}
				}
				return stack;
			}
			
//			INamedTag<Item> tag = ItemTags.makeWrapperTag(s);
			Stream<Item> values = HelperTags.getValues(ItemTags.create(new ResourceLocation(s)));
			ArrayList<ItemStack> items = new ArrayList<>(values.map(ItemStack::new).toList());
			if(!items.isEmpty())
			{
				return items;
			}
			else
			{
				Item i = ForgeRegistries.ITEMS.getValue(new ResourceLocation(s));
				if(i!=null && i!=Items.AIR)
				{
					ItemStack it = new ItemStack(i,1);
					return new ArrayList<ItemStack>(Arrays.asList(it));
				}
			}		
		}
		if(elm.isJsonObject())
		{
			JsonObject obj = elm.getAsJsonObject();
			if(obj.has("name"))
			{	
				String name = obj.get("name").getAsString();
				int size = 1;
				if(obj.has("size"))
				{
					size = obj.get("size").getAsInt();
				}
				Item i = ForgeRegistries.ITEMS.getValue(new ResourceLocation(name));
				if(i !=null && i!=Items.AIR)
				{
					ItemStack it = new ItemStack(i, size);
					ArrayList<ItemStack> stacks = new ArrayList<ItemStack>(1);
					stacks.add(it);
					return stacks;
				}
			}
			else if(obj.has("tag"))
			{
				String ore = obj.get("tag").getAsString();
				
				HelperOreDict.waitForTagsToLoad();
				int size = 1;
				if(obj.has("size"))
				{
					size = obj.get("size").getAsInt();
				}
				Stream<Item> values = HelperTags.getValues(ItemTags.create(new ResourceLocation(ore)));
				final int s = size;
				ArrayList<ItemStack> items = new ArrayList<>(values.map(i -> new ItemStack(i, s)).toList());
				
				if(!items.isEmpty())
				{
					return items;
				}
				else
				{
					ArrayList<ItemStack> stacks = new ArrayList<ItemStack>(1);
					if(fakeEntrys)
					{
						ItemStack it = new ItemStack(Blocks.STONE);
						it.setHoverName(new TextComponent("Any "+ ore));
						stacks.add(it);
					}
					return stacks;
				}
			}
		}
		if(elm.isJsonArray())
		{
			JsonArray arr = elm.getAsJsonArray();
			ArrayList<ItemStack> arrl = new ArrayList<ItemStack>(arr.size());
			for(int i=0;i<arr.size();i++)
			{
				arrl.addAll(getItemFromJSON(arr.get(i),fakeEntrys));
			}
			return arrl;
		}
		System.err.println("ItemStack parsing failed: '"+elm+"' is null");
		if(fakeEntrys)
		{
			ItemStack it = new ItemStack(Blocks.BARRIER);
			it.setHoverName(new TextComponent(elm.toString()));
			return new ArrayList<>(Arrays.asList(it));
		}
		return new ArrayList<ItemStack>();
		
	}
	
	public static void setupRendering(List<ItemStack> raw)
	{
		NonNullList<ItemStack> newItems = NonNullList.create();
		List<ItemStack> remove = new ArrayList<>();
		
		for(ItemStack ut : raw)
		{
			if(ut.isEmpty())
				remove.add(ut);
		}
		raw.removeAll(remove);
		raw.addAll(newItems);
	}
	
	public static String ChatToJSON(Component chat)
	{
		String json = Component.Serializer.toJson(chat);
		return json;
	}
	
	public static String ChatToJSON(Component[] chat)
	{
		TextComponent text = new TextComponent("");
		for(int i=0;i<chat.length;i++)
		{
			if(chat[i]!=null)
				text.append(chat[i]);
		}
		return ChatToJSON(text);
	}
	
//	/**
//	 * var lol => 10
//	 * 
//	 * s:2 -> s=2
//	 * s:"2" -> s=2
//	 * s:"<lol>" -> s=10 //because it uses the above defined var
//	 * s:"lol" -> ERROR lol  is not a Number
//	 * s:"<soo>" -> s=0 //because soo is not defined so its asumed as 0
//	 * 
//	 * yes you need <> to use a defined var
//	 */
//	private static int getVarSave(JsonElement jo)
//	{
//		if(jo.isJsonPrimitive())
//		{
//			JsonPrimitive jprim = jo.getAsJsonPrimitive();
//			if(jprim.isString())
//			{
//				String s = jprim.getAsString();
//				if(s.startsWith("<") && s.endsWith(">") && s.length()>2)
//				{
//					String sub = s.substring(1, s.length()-1);
//					Integer val = variables.get(sub);
//					if(val!=null)
//					{
//						return val;
//					}
//					else
//					{
//						System.out.println("[WARN] Dont found '" + sub+"'");
//						return 0;
//					}
//				}
//				else
//				{
//					return Integer.valueOf(s);
//				}
//			}
//			return jprim.getAsInt();
//		}
//		return 0;
//	}
	
	public static ItemPredicateBase getItemPredicateFromJSON(JsonElement elm)
	{
		if(elm==null || elm.isJsonNull())
			return new NullPredicate();
		else if(elm.isJsonObject())
		{
			JsonObject obj = elm.getAsJsonObject();
			if(obj.has("name"))
			{	
				String name = obj.get("name").getAsString();
				int size = 1;
				if(obj.has("size"))
				{
					size = obj.get("size").getAsInt();
				}

				Item i = ForgeRegistries.ITEMS.getValue(new ResourceLocation(name));
				if(i !=null && i!=Items.AIR)
				{
					return new ItemPredicate(i, size);
				}
			}
			else if(obj.has("tag"))
			{
				String ore = obj.get("tag").getAsString();
				int size = 1;
				if(obj.has("size"))
				{
					size = obj.get("size").getAsInt();
				}
				return new VanillaTagPredicate(ore, size);
			}
		}
		else if(elm.isJsonPrimitive())
		{
			JsonPrimitive prim = elm.getAsJsonPrimitive();
			String s = prim.getAsString();
			if(s.startsWith("tag:"))
			{
				s = s.substring("tag:".length(), s.length());
				return new VanillaTagPredicate(s);
			}
			ResourceLocation res = new ResourceLocation(s);
			
			Item i = ForgeRegistries.ITEMS.getValue(res);
			if(i!=null && i!=Items.AIR)
			{
				return new ItemPredicate(i);
			}
			
			HelperOreDict.waitForTagsToLoad();
			
			TagKey<Item> tag = ItemTags.create(res);
			
//			ITag<Item> tag = ItemTags.getCollection().get(res);
			if(tag!=null)//for now everything gets firat intepredted as a tag
			{
				return new VanillaTagPredicate(s);// Dont know if resource locations should always first get intepreteed as Tag
			}
			
		}
		else if(elm.isJsonArray())
		{
			JsonArray arr = elm.getAsJsonArray();
			ItemPredicateBase[] predicates = new ItemPredicateBase[arr.size()];
			for(int i=0;i<arr.size();i++)
			{
				predicates[i] = getItemPredicateFromJSON(arr.get(i));
			}
			return new ListPredicate(true, predicates);
		}
		
		System.err.println("Predicate parsing failed: '"+elm+"' is null ");
		return new NullPredicate();
	}
	
	
	public static void generateRecipe(TileEntityTechtable table)
	{
		System.out.println("Das musst du schon selber implementeiren wugand! - aber bitte mach einen externen call mit extra classe sodass nicht alles hier drinnen ist!");
		
		try
		{	
			ItemStack[] grid = new ItemStack[9];
			for(int i = 0; i < 9; i++)
			{
				grid[i] = table.getItem(i);
			}
			
			RecipeJSONGenerator.generateRecipe(grid, table.getItem(18));
		}
		catch(Throwable tr)
		{
			tr.printStackTrace();
		}

		
	}
}
