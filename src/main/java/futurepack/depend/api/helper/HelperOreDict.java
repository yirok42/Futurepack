package futurepack.depend.api.helper;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

import futurepack.api.helper.HelperTags;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.tags.ItemTags;
import net.minecraft.tags.TagKey;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.ItemLike;

public class HelperOreDict
{
	
	public static void waitForTagsToLoad()
	{
		while(!areTagsLoaded())
		{
			try 
			{
				Thread.sleep(100);
			} catch (InterruptedException e) 
			{
				e.printStackTrace();
			}
		}
	}
	
	public static boolean areTagsLoaded()
	{
		return HelperTags.areTagsLoaded();
	}
	
	private static class ConvertEntry implements Predicate<Item>
	{
		final Item item;
		final ResourceLocation tagId;
		TagKey<Item> tag = null;
		
		public ConvertEntry(Item item, ResourceLocation tagId)
		{
			super();
			this.item = item;
			this.tagId = tagId;
		}
		
		@Override
		public boolean test(Item it)
		{
			if(tag == null)
			{
				waitForTagsToLoad();
				tag = ItemTags.create(tagId);
				if(!this.item.builtInRegistryHolder().containsTag(tag))
				{
					throw new RuntimeException("Can not convert to Item " + item + " as it is not contained in tag " + tagId);
				}
			}
			return it.builtInRegistryHolder().containsTag(tag);
		}
	}
	
	public static class Builder
	{
		private ArrayList<Item> items = new ArrayList<>();
		private ArrayList<ResourceLocation> tags = new ArrayList<>();
		
		public Builder add(ItemLike item, ResourceLocation res)
		{
			items.add(item.asItem());
			tags.add(res);
			return this;
		}
		
		public Builder add(Item item, TagKey<Item> res)
		{
			items.add(item);
			tags.add(res.location());
			return this;
		}
		
		public HelperOreDict build()
		{
			return new HelperOreDict(items.toArray(new Item[items.size()]), tags.toArray(new ResourceLocation[tags.size()]));
		}
	}
	
	/**
	 * set in {@link futurepack.common.FPMain}.preInit()
	 */
	public static HelperOreDict FuturepackConveter;
	
	private final List<ConvertEntry> list;
	
	public HelperOreDict(Item[] items, ResourceLocation[] tags)
	{
		if(items.length!=tags.length)
			throw new IllegalArgumentException("arrays have not the same size!");
		
		list = new ArrayList<>(items.length);
		
		for(int i=0;i<items.length;i++)
		{
			list.add(new ConvertEntry(items[i], tags[i]));
		}
	}
	
	/**
	 * Change the given ItemStack to an corresponding FuturePack Item, if possible.
	 * @param it Original ItemStack
	 * @return Changed ItemStack
	 */
	public Item getChangedItem(Item item)
	{
		if(item == null)
			return null;
		
		for(ConvertEntry e : list)
		{
			if(e.test(item))
			{
				return e.item;
			}
		}

		return item;
	}
	
	public Item getChangedItem(ResourceLocation tagName)
	{
		if(tagName == null)
			return null;
		
		for(ConvertEntry e : list)
		{
			if(tagName.equals(e.tagId))
				return e.item;
		}
		
		HelperOreDict.waitForTagsToLoad();
		TagKey<Item> item = ItemTags.create(tagName);
		if(item!=null)
		{
			Optional<Item> opt = HelperTags.getValues(item).findFirst();
			return opt.orElse(null);
		}
		return null;
	}
	
	public ItemStack getChangedItem(ItemStack it)
	{
		if(it.isEmpty())
			return ItemStack.EMPTY;
		
		return new ItemStack(getChangedItem(it.getItem()));
	}
	
	public ItemStack getChangedItemSizeSensitiv(ItemStack it)
	{
		if(it.isEmpty())
			return ItemStack.EMPTY;
		
		return new ItemStack(getChangedItem(it.getItem()), it.getCount(), it.getTag());
		
	}
	
//	public void addOre(String name, ItemStack ore)
//	{
//		OreDictionary.registerOre(name, ore);
//		int id = OreDictionary.getOreID(name);
//		used.putIfAbsent(id, ore);
//	}
//	
//	public ItemStack getOre(String name)
//	{
//		int id = OreDictionary.getOreID(name);
//		ItemStack item = used.get(id);
//		if(item!=null)
//			return item;
//		
//		return OreDictionary.getOres(name).get(0);
//	}
	
	public static TagKey<Item> getOptionalTag(ResourceLocation res)
	{
		return ItemTags.create(res);
	}
	
//	public static <T> ITag<T> createOptionalTag(ITagCollection<T> tags, ResourceLocation res)
//	{
//		return ITag.Builder.create().addTag(new ITag.OptionalTagEntry(res), "optional:"+res.toString().replace(':', '/')).build(tags::get, tags::);
//	}
}
