package futurepack.client.render.entity;

import futurepack.api.Constants;
import futurepack.common.entity.living.EntityEvilRobot;
import net.minecraft.client.model.HumanoidModel;
import net.minecraft.client.model.geom.ModelLayerLocation;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.model.geom.PartPose;
import net.minecraft.client.model.geom.builders.CubeDeformation;
import net.minecraft.client.model.geom.builders.CubeListBuilder;
import net.minecraft.client.model.geom.builders.LayerDefinition;
import net.minecraft.client.model.geom.builders.MeshDefinition;
import net.minecraft.client.model.geom.builders.PartDefinition;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.entity.HumanoidArm;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;

public class ModelEvilRobot extends HumanoidModel<EntityEvilRobot>
{
	public static final ModelLayerLocation LAYER_LOCATION = new ModelLayerLocation(new ResourceLocation(Constants.MOD_ID, "evil_robot"), "main");
	public static final ModelLayerLocation LAYER_LOCATION_ARMOR = new ModelLayerLocation(new ResourceLocation(Constants.MOD_ID, "evil_robot"), "armor");
	
	public ModelEvilRobot(ModelPart p_170677_) 
	{
		super(p_170677_, RenderType::entityCutoutNoCull);
	}
	
	public static MeshDefinition createMesh(CubeDeformation modelSize, float height) 
	{
	      MeshDefinition meshdefinition = HumanoidModel.createMesh(modelSize, height);
	      PartDefinition partdefinition = meshdefinition.getRoot();
	      partdefinition.addOrReplaceChild("right_arm", CubeListBuilder.create().texOffs(40, 16).addBox(-1.0F, -2.0F, -1.0F, 2.0F, 12.0F, 2.0F, modelSize), PartPose.offset(-5.0F, 2.0F + height, 0.0F));
	      partdefinition.addOrReplaceChild("left_arm", CubeListBuilder.create().texOffs(40, 16).mirror().addBox(-1.0F, -2.0F, -1.0F, 2.0F, 12.0F, 2.0F, modelSize), PartPose.offset(5.0F, 2.0F + height, 0.0F));
	      partdefinition.addOrReplaceChild("right_leg", CubeListBuilder.create().texOffs(0, 16).addBox(1.0F, 0.0F, -1.0F, 2, 12, 2, modelSize), PartPose.offset(-3.9F, 12.0F + height, 0.0F));
	      partdefinition.addOrReplaceChild("left_leg", CubeListBuilder.create().texOffs(0, 16).mirror().addBox(-1.0F, 0.0F, -1.0F, 2.0F, 12.0F, 2.0F, modelSize), PartPose.offset(1.9F, 12.0F + height, 0.0F));
	      return meshdefinition;
	}
	
	public static LayerDefinition createBodyLayer() 
	{
		return LayerDefinition.create(createMesh(CubeDeformation.NONE, 0F), 64, 32);
	}
	
	public static LayerDefinition createArmorLayer() 
	{
		return LayerDefinition.create(HumanoidModel.createMesh(CubeDeformation.NONE, 0F), 64, 32);
	}

	@Override
	public void prepareMobModel(EntityEvilRobot entitylivingbaseIn, float f1, float f2, float partialTickTime)
	{
		this.rightArmPose = HumanoidModel.ArmPose.EMPTY;
		this.leftArmPose = HumanoidModel.ArmPose.EMPTY;
		ItemStack itemstack = entitylivingbaseIn.getItemInHand(InteractionHand.MAIN_HAND);
		
		if (itemstack != null && itemstack.getItem() == Items.BOW && (entitylivingbaseIn).isAggressive())
		{
			if (entitylivingbaseIn.getMainArm() == HumanoidArm.RIGHT)
			{
				this.rightArmPose = HumanoidModel.ArmPose.BOW_AND_ARROW;
			}
			else
			{
				this.leftArmPose = HumanoidModel.ArmPose.BOW_AND_ARROW;
			}
		}
		
		super.prepareMobModel(entitylivingbaseIn, f1, f2, partialTickTime);
	}

// FIXME: maybe evil robot no longer renders items in hand
//	@Override
//	public void postRenderArm(float scale, HandSide side)
//	{
//		float f = side == HandSide.RIGHT ? 1.0F : -1.0F;
//		ModelRenderer modelrenderer = this.getArmForSide(side);
//		modelrenderer.rotationPointX += f;
//		modelrenderer.postRender(scale);
//		modelrenderer.rotationPointX -= f;
//	}
}
