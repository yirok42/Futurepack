package futurepack.client.render.entity;

import com.google.common.collect.ImmutableList;
import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;

import futurepack.api.Constants;
import futurepack.common.entity.living.EntityGehuf;
import net.minecraft.client.model.AgeableListModel;
import net.minecraft.client.model.geom.ModelLayerLocation;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.model.geom.PartPose;
import net.minecraft.client.model.geom.builders.CubeDeformation;
import net.minecraft.client.model.geom.builders.CubeListBuilder;
import net.minecraft.client.model.geom.builders.LayerDefinition;
import net.minecraft.client.model.geom.builders.MeshDefinition;
import net.minecraft.client.model.geom.builders.PartDefinition;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.util.Mth;

public class ModelGehuf extends AgeableListModel<EntityGehuf>
{
	
	// This layer location should be baked with EntityRendererProvider.Context in the entity renderer and passed into this model's constructor
	public static final ModelLayerLocation LAYER_LOCATION = new ModelLayerLocation(new ResourceLocation(Constants.MOD_ID, "gehuf"), "main");
	private final ModelPart gehuf;

	ModelPart legfrontleft;
	ModelPart legfrontright;
	ModelPart legbackright;
	ModelPart legbacktleft;
	ModelPart schwanz;
	ModelPart halts;
	ModelPart head;
	
	private float headRotationAngleX;
	
	public ModelGehuf(ModelPart root) 
	{
		this.gehuf = root.getChild("gehuf");
		halts = gehuf.getChild("neck");
		head = halts.getChild("head");
		schwanz = gehuf.getChild("tail");
		legfrontleft = gehuf.getChild("leg_f_l");
		legfrontright = gehuf.getChild("leg_f_r");
		legbackright= gehuf.getChild("leg_b_r");
		legbacktleft = gehuf.getChild("leg_b_l");
	}

	public static LayerDefinition createBodyLayer() 
	{
		MeshDefinition meshdefinition = new MeshDefinition();
		PartDefinition partdefinition = meshdefinition.getRoot();

		PartDefinition gehuf = partdefinition.addOrReplaceChild("gehuf", CubeListBuilder.create().texOffs(0, 2).addBox(-3.0F, -15.5F, -3.0F, 6.0F, 6.0F, 6.0F, new CubeDeformation(0.0F))
				.texOffs(24, 3).addBox(-9.0F, -15.0F, -3.0F, 6.0F, 5.0F, 6.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 16.0F, -3.0F, 0.0F, 1.5708F, 0.0F));

		PartDefinition leg_f_r = gehuf.addOrReplaceChild("leg_f_r", CubeListBuilder.create().texOffs(0, 20).addBox(-1.0F, 0.0F, -0.99F, 2.0F, 10.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(1.0F, -10.0F, -2.0F));

		PartDefinition leg_f_l = gehuf.addOrReplaceChild("leg_f_l", CubeListBuilder.create().texOffs(8, 20).addBox(-1.0F, 0.0F, -1.01F, 2.0F, 10.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(1.0F, -10.0F, 2.0F));

		PartDefinition leg_b_l = gehuf.addOrReplaceChild("leg_b_l", CubeListBuilder.create().texOffs(8, 20).addBox(-1.0F, 0.0F, -1.0F, 2.0F, 10.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(-7.0F, -10.0F, 2.0F));

		PartDefinition leg_b_r = gehuf.addOrReplaceChild("leg_b_r", CubeListBuilder.create().texOffs(0, 20).addBox(-1.0F, 0.0F, -1.0F, 2.0F, 10.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(-7.0F, -10.0F, -2.0F));

		PartDefinition tail = gehuf.addOrReplaceChild("tail", CubeListBuilder.create().texOffs(16, 20).addBox(-1.0F, 0.0F, -0.5F, 1.0F, 6.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(-9.0F, -13.0F, 0.0F));

		PartDefinition neck = gehuf.addOrReplaceChild("neck", CubeListBuilder.create().texOffs(0, 14).addBox(0.0F, -1.5F, -1.5F, 6.0F, 3.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offset(2.0F, -12.5F, 0.0F));

		PartDefinition head = neck.addOrReplaceChild("head", CubeListBuilder.create().texOffs(26, 14).mirror().addBox(0.0F, -2.5F, -2.5F, 5.0F, 5.0F, 5.0F, new CubeDeformation(0.0F))
				.texOffs(18, 14).addBox(5.0F, -0.5F, -1.5F, 1.0F, 2.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offset(4.5F, 0.0F, 0.0F));

		return LayerDefinition.create(meshdefinition, 64, 32);
	}

	@Override
	public void renderToBuffer(PoseStack poseStack, VertexConsumer buffer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha) 
	{
		gehuf.render(poseStack, buffer, packedLight, packedOverlay);
	}
	
	/*
//	//fields

//  
//	ImmutableList<ModelPart> parts;
//	
//	protected float field_78145_g = 8.0F;
//	protected float field_78151_h = 4.0F;
//	
//
//	public ModelGehuf()
//	{
//		texWidth = 64;
//		texHeight = 32;
//	
//		body1 = new ModelPart(this, 0, 2);
//		body1.addBox(-3F, 0F, -3F, 6, 6, 6);
//		body1.setPos(0F, 0F, 0F);
//		body1.setTexSize(64, 32);
//		body1.mirror = true;
//		setRotation(body1, 0F, 0F, 0F);
//		body2 = new ModelPart(this, 24, 3);
//		body2.addBox(-3F, 0F, -3F, 6, 5, 6);
//		body2.setPos(6F, 0.5F, 0F);
//		body2.setTexSize(64, 32);
//		body2.mirror = true;
//		setRotation(body2, 0F, 0F, 0F);
//		legfrontleft = new ModelPart(this, 0, 20);
//		legfrontleft.addBox(-1F, 0F, -1F, 2, 10, 2);
//		legfrontleft.setPos(-1F, 6F, -2F);
//		legfrontleft.setTexSize(64, 32);
//		legfrontleft.mirror = true;
//		setRotation(legfrontleft, 0F, 0F, 0F);
//		legfrontright = new ModelPart(this, 8, 20);
//		legfrontright.addBox(-1F, 0F, -1F, 2, 10, 2);
//		legfrontright.setPos(-1F, 6F, 2F);
//		legfrontright.setTexSize(64, 32);
//		legfrontright.mirror = true;
//		setRotation(legfrontright, 0F, 0F, 0F);
//		legbackright = new ModelPart(this, 8, 20);
//		legbackright.addBox(-1F, 0F, -1F, 2, 10, 2);
//		legbackright.setPos(7F, 5.5F, 2F);
//		legbackright.setTexSize(64, 32);
//		legbackright.mirror = true;
//		setRotation(legbackright, 0F, 0F, 0F);
//		legbacktleft = new ModelPart(this, 0, 20);
//		legbacktleft.addBox(-1F, 0F, -1F, 2, 10, 2);
//		legbacktleft.setPos(7F, 5.5F, -2F);
//		legbacktleft.setTexSize(64, 32);
//		legbacktleft.mirror = true;
//		setRotation(legbacktleft, 0F, 0F, 0F);
//		schwanz = new ModelPart(this, 16, 20);
//		schwanz.addBox(0F, 0F, -0.5F, 1, 6, 1);
//		schwanz.setPos(9F, 2F, 0F);
//		schwanz.setTexSize(64, 32);
//		schwanz.mirror = true;
//		setRotation(schwanz, 0F, 0F, 0F);
//		halts = new ModelPart(this, 0, 14);
//		halts.addBox(-6F, -1.5F, -1.5F, 6, 3, 3);
//		halts.setPos(-2F, 4.5F, 0F);
//		halts.setTexSize(64, 32);
//		halts.mirror = true;
//		setRotation(halts, 0F, 0F, 0F);
//		head = new ModelPart(this, 26, 14);
//		head.addBox(-5F, -2F, -2.5F, 5, 5, 5);
//		head.setPos(-8F, 4F, 0F);
//		head.setTexSize(64, 32);
//		head.mirror = true;
//		setRotation(head, 0F, 0F, 0F);
//		nose = new ModelPart(this, 18, 14);
//		nose.addBox(-1F, -1F, -1.5F, 1, 2, 3);
//		nose.setPos(-13F, 5F, 0F);
//		nose.setTexSize(64, 32);
//		nose.mirror = true;
//		setRotation(nose, 0F, 0F, 0F);
//		
//		addChild(halts, head);
//		nose.x -= halts.x;
//		nose.y -= halts.y;
//		nose.z -= halts.z;
//		addChild(head, nose);
//		
//		Builder<ModelPart> builder = ImmutableList.builder();
//		builder.add(body1);
//		builder.add(body2);
//		builder.add(legfrontleft);
//		builder.add(legfrontright);
//		builder.add(legbackright);
//		builder.add(legbacktleft);
//		builder.add(schwanz);
////		builder.add(halts);
//		
//		this.parts = builder.build();
//	}
//  
//	private void addChild(ModelPart base, ModelPart child)
//	{
//		child.x -= base.x;
//		child.y -= base.y;
//		child.z -= base.z;
//		base.addChild(child);
//	}
 * */ 

	@Override
	public void setupAnim(EntityGehuf eGehuf, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch)
	{
		float f6 = (180F / (float)Math.PI);
		
		halts.zRot = -headPitch / f6;
		this.head.yRot = netHeadYaw / f6;
		halts.zRot= -this.headRotationAngleX;
		
		this.legfrontleft.zRot = Mth.cos(limbSwing * 0.6662F) * 1.4F * limbSwingAmount;
		this.legfrontright.zRot = Mth.cos(limbSwing * 0.6662F + (float)Math.PI) * 1.4F * limbSwingAmount;
		this.legbackright.zRot = Mth.cos(limbSwing * 0.6662F + (float)Math.PI) * 1.4F * limbSwingAmount;
		this.legbacktleft.zRot = Mth.cos(limbSwing * 0.6662F) * 1.4F * limbSwingAmount;
		
		schwanz.zRot = Mth.cos(limbSwing * 0.6662F + (float)Math.PI / 2) * 0.3F * limbSwingAmount;
	}
	
	@Override
	public void prepareMobModel(EntityGehuf entityGehuf, float limbSwing, float limbSwingAmount, float partialTickTime)
	{
		super.prepareMobModel(entityGehuf, limbSwing, limbSwingAmount, partialTickTime);
		this.headRotationAngleX = entityGehuf.getHeadRotationAngleX(partialTickTime);
	}

	@Override
	protected Iterable<ModelPart> headParts() 
	{
		return ImmutableList.of(halts);
	}
 
	@Override
	protected Iterable<ModelPart> bodyParts() 
	{
		return ImmutableList.of(gehuf);
	}
}
