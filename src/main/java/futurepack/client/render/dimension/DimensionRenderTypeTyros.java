package futurepack.client.render.dimension;

import futurepack.client.render.RenderSkyTyros;
import net.minecraft.client.renderer.DimensionSpecialEffects;
import net.minecraft.world.phys.Vec3;

public class DimensionRenderTypeTyros extends DimensionSpecialEffects 
{

	private final static float cloud_height = 158F;
	
	private final static boolean isNether = false;
	private final static boolean isEnd = false;
	
	public DimensionRenderTypeTyros()
	{
		super(cloud_height, true, SkyType.NORMAL, isEnd, isNether);
		setSkyRenderHandler(new RenderSkyTyros());
	}

	/**
	 * Calculate sunset color
	 */
	@Override
	public Vec3 getBrightnessDependentFogColor(Vec3 sky_color_body, float celestialAngle_clamped) 
	{
		return sky_color_body.multiply((double)(celestialAngle_clamped), (double)(celestialAngle_clamped), (double)(celestialAngle_clamped));
	}

	/**
	 * show fog at
	 */
	@Override
	public boolean isFoggyAt(int p_230493_1_, int p_230493_2_) 
	{
		return false;
	}

}
