package futurepack.client.render.block;

import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.math.Vector3f;

import futurepack.common.block.BlockRotateable;
import futurepack.common.block.misc.TileEntityResearchExchange;
import futurepack.common.item.tools.ToolItems;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.block.model.ItemTransforms;
import net.minecraft.client.renderer.blockentity.BlockEntityRenderer;
import net.minecraft.client.renderer.blockentity.BlockEntityRendererProvider;
import net.minecraft.core.Direction;
import net.minecraft.core.Direction.Axis;
import net.minecraft.world.item.ItemStack;

public class RenderTectern implements BlockEntityRenderer<TileEntityResearchExchange> 
{
	
	public RenderTectern(BlockEntityRendererProvider.Context rendererDispatcherIn) {
//		super(rendererDispatcherIn);
	}

	@Override
	public void render(TileEntityResearchExchange tileEntityIn, float partialTicks, PoseStack matrixStackIn,
			MultiBufferSource bufferIn, int combinedLightIn, int combinedOverlayIn) {
		
		matrixStackIn.pushPose();
		ItemStack it = new ItemStack(ToolItems.escanner);
		
		Direction dir = tileEntityIn.getBlockState().getValue(BlockRotateable.HORIZONTAL_FACING);
		
		matrixStackIn.translate(0.5d, 0.5d, 0.5d);
		matrixStackIn.mulPose(Vector3f.YP.rotationDegrees(dir.toYRot() + (dir.getAxis()==Axis.X ? 180 : 0)));
		matrixStackIn.translate(0.0d, 0.43d, 0.3d);
		
		matrixStackIn.mulPose(Vector3f.XP.rotationDegrees(-68F));
		
		int i = (int)tileEntityIn.getBlockPos().asLong();
		
		Minecraft.getInstance().getItemRenderer().renderStatic(it, ItemTransforms.TransformType.GROUND, combinedLightIn, combinedOverlayIn, matrixStackIn, bufferIn, i);
		
		matrixStackIn.popPose();		
	}
	
}
