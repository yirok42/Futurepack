package futurepack.client.render.block;

import java.util.function.Function;

import net.minecraft.client.renderer.RenderType;
import net.minecraft.resources.ResourceLocation;

public abstract class ModelLaserBase extends SegmentedBlockModel
{
	public ModelLaserBase(Function<ResourceLocation, RenderType> renderTypeIn) 
	{
		super(renderTypeIn);
	}

	public abstract void rotateYUnit(float y);
	    
	public abstract void rotateXUnit(float x);
}
