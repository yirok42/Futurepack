package futurepack.client.particle;

import java.util.ArrayList;

import futurepack.api.FacingUtil;
import futurepack.api.ParentCoords;
import net.minecraft.client.multiplayer.ClientLevel;
import net.minecraft.client.particle.ParticleRenderType;
import net.minecraft.client.particle.SingleQuadParticle;
import net.minecraft.core.Direction;
import net.minecraft.world.phys.Vec3;

public class ParticlePathSearch extends SingleQuadParticle
{
	private Vec3[] path;
	private int pointer = 0;
	
	protected ParticlePathSearch(ClientLevel worldIn, double posXIn, double posYIn, double posZIn, Vec3[] coords)
	{
		super(worldIn, posXIn, posYIn, posZIn);		
		path = coords;
		pointer=path.length-1;
		setPos(path[pointer].x, path[pointer].y, path[pointer].z);
		pointer--;
		lifetime=20*60*2;
	}
	
	public ParticlePathSearch(ClientLevel worldIn, ParentCoords coords)
	{
		this(worldIn, coords.getX(), coords.getY(), coords.getZ(), compressPath(coords));		
	}

	
	private static Vec3[] compressPath(ParentCoords coords)
	{
		ArrayList<Vec3> posList = new ArrayList<Vec3>();
		Direction face = null;
		ParentCoords pos = coords;
		while(pos!=null)
		{
			ParentCoords next = pos.getParent();
			if(next!=null)
			{
				Direction newF = FacingUtil.getSide(pos, next);
				if(newF != face)
				{
					posList.add(new Vec3(pos.getX()+0.5, pos.getY()+0.5, pos.getZ()+0.5));
					face = newF;
				}
			}
			else
			{
				posList.add(new Vec3(pos.getX()+0.5, pos.getY()+0.5, pos.getZ()+0.5));
			}
			pos = next;
		}		
		return posList.toArray(new Vec3[posList.size()]);
	}


	
	@Override
	public void tick()
	{
		if(pointer < 0)
		{
			remove();
		}
		else
		{
			Vec3 v1 = path[pointer];
			
			if(new Vec3(x, y, z).subtract(v1).length() < 0.09 )
			{
				pointer--;
			}
			Vec3 v2 = path[pointer+1];
			
			Vec3 mot = v1.subtract(v2).normalize();
			xd = mot.x * 0.06;
			yd = mot.y * 0.06;
			zd = mot.z * 0.06;			
		}
		
		super.tick();
	}

	@Override
	public ParticleRenderType getRenderType() 
	{
		return ParticleRenderType.PARTICLE_SHEET_OPAQUE;
	}

	@Override
	protected float getU0() 
	{
		return 0;
	}

	@Override
	protected float getU1() 
	{
		return 0;
	}

	@Override
	protected float getV0() 
	{
		return 0;
	}

	@Override
	protected float getV1() 
	{
		return 0;
	}
}
