package futurepack.client.sos;

import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.PoseStack;

import futurepack.api.Constants;
import net.minecraft.client.gui.GuiComponent;
import net.minecraft.resources.ResourceLocation;

public class AnimationScanning extends AnimationBase
{

	private ResourceLocation res = new ResourceLocation(Constants.MOD_ID, "textures/os/lupe64.png");
	
	private long ticks = -1;
	
	@Override
	public void render(PoseStack matrixStack, int mouseX, int mouseY)
	{
		if(ticks==-1)
		{
			ticks = System.currentTimeMillis();
		}
		matrixStack.pushPose();
		
		RenderSystem.setShaderTexture(0, res);
		
		int x= xPos + (83-48)/2;
		int y= yPos + (80-48)/2;
		double pi = 2*Math.PI * (System.currentTimeMillis()-ticks)/1000D;
		matrixStack.translate((Math.sin(pi) *10), (Math.cos(pi) *10), 0);
		
		GuiComponent.blit(matrixStack, x, y, 48, 48, 0, 0, 48, 48, 48, 48);
		matrixStack.popPose();
	}

	@Override
	public boolean isFinished()
	{
		return ticks!=-1 && System.currentTimeMillis()-ticks >= 1200;
	}

}
