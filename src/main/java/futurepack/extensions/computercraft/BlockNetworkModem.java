package futurepack.extensions.computercraft;

import futurepack.common.block.BlockHoldingTile;
import net.minecraft.core.BlockPos;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;

public class BlockNetworkModem extends BlockHoldingTile 
{

	public BlockNetworkModem(Properties builder) 
	{
		super(builder, false);
	}

	@Override
	public BlockEntity newBlockEntity(BlockPos pPos, BlockState pState) 
	{
		return new TileEntityCCModem(pPos, pState);
	}

}
