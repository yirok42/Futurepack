package futurepack.extensions.computercraft;

import java.util.Collections;
import java.util.EnumMap;
import java.util.Map;
import java.util.TreeMap;

import javax.annotation.Nonnull;

import dan200.computercraft.api.lua.LuaException;
import dan200.computercraft.api.lua.LuaFunction;
import dan200.computercraft.api.peripheral.GenericPeripheral;
import dan200.computercraft.api.peripheral.PeripheralType;
import futurepack.api.Constants;
import futurepack.api.EnumStateSpaceship;
import futurepack.api.interfaces.IPlanet;
import futurepack.api.interfaces.tilentity.ITileBoardComputer;
import futurepack.common.block.inventory.TileEntityBoardComputer;
import futurepack.common.spaceships.FPPlanetRegistry;
import net.minecraft.core.BlockPos;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.level.block.entity.BlockEntity;

public class BoardcomputerMethods implements GenericPeripheral 
{
	@Nonnull
	@Override
	public PeripheralType getType() 
	{
		return PeripheralType.ofAdditional("boardcomputer");
	}

	@Nonnull
	@Override
	public ResourceLocation id() 
	{
		return new ResourceLocation(Constants.MOD_ID, "boardcomputer");
	}
	
	public static Map<String, ?> fillInfo(IPlanet planet)
	{
		Map<String, Object> map = new TreeMap<>();
		
		map.put("id", planet.getDimenionId().toString());
		map.put("name", planet.getName());
		map.put("needed_upgrades", planet.getRequiredShipUpgrades());
		map.put("has_breathable_atmosphere", planet.hasBreathableAtmosphere());
		
		
		return map;
	}
	
	@LuaFunction
	public static Map<String, ?> getPlanetInfo(ITileBoardComputer computer)
	{
		if(computer instanceof BlockEntity te)
		{
			return fillInfo(FPPlanetRegistry.instance.getPlanetSafe(te.getLevel()));
		}
		return new TreeMap<>();
	}
	
	@LuaFunction
	public static Map<String, ?> getShipState(ITileBoardComputer computer)
	{
		Map<String, Boolean> map = new TreeMap<>();
		
		for(EnumStateSpaceship state : EnumStateSpaceship.values())
		{
			map.put(state.name(), computer.getState(state));
		}
		
		return map;
	}
	
	@LuaFunction
	public static boolean isAdvancendBoardcomputer(ITileBoardComputer computer)
	{
		return computer.isAdvancedBoardComputer();
	}
	
	
	@LuaFunction(mainThread = true)
	public static Map<String, ?> getShipStats(ITileBoardComputer computer)
	{
		Map<String, Object> map = new TreeMap<>();
		
		map.put("thruster_count", computer.getThrusterCount());
		map.put("fuel_count", computer.getFuelCount());
		map.put("block_count", computer.getBlockCount());
		map.put("transporting_block_count", computer.getTransportingBlockCount());
		
		return map;
	}
	
	@LuaFunction(mainThread = true)
	public static float getJumpProgress(ITileBoardComputer computer)
	{
		return computer.getJumpProgress();
	}
	
	@LuaFunction(mainThread = true)
	public static  boolean canJump(ITileBoardComputer computer)
	{
		return computer.canJump();
	}
	
	@LuaFunction(mainThread = true)
	public static void searchForShip(ITileBoardComputer computer)
	{
		computer.searchForShip();
	}
	
	@LuaFunction(mainThread = true)
	public static Map<String, ?> getJumpTargetPosition(ITileBoardComputer computer)
	{
		BlockPos p = computer.getJumpTargetPosition();
		if(p==null)
			return null;
		else
		{
			Map<String, Object> map = new TreeMap<>();
			map.put("x", p.getX());
			map.put("y", p.getY());
			map.put("z", p.getZ());
			return map;
		}
	}
	
	@LuaFunction(mainThread = true)
	public static Map<String, ?> getComputerPosition(ITileBoardComputer computer)
	{
		BlockPos p = computer.getComputerPosition();
		if(p==null)
			return null;
		else
		{
			Map<String, Object> map = new TreeMap<>();
			map.put("x", p.getX());
			map.put("y", p.getY());
			map.put("z", p.getZ());
			return map;
		}
	}
	
	@LuaFunction(mainThread = true)
	public static void prepareJump(ITileBoardComputer computer, String planet, int x, int y, int z) throws LuaException
	{
		IPlanet pl = FPPlanetRegistry.instance.getPlanetByName(planet);
		if(pl == null)
		{
			throw new LuaException("unknown dimension '" + planet + "'");
		}
		computer.prepareJump(pl, new BlockPos(x, y, z));
	}
}
