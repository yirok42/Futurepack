package futurepack.extensions.computercraft;

import java.util.Map;
import java.util.function.BooleanSupplier;

import dan200.computercraft.api.filesystem.IMount;
import dan200.computercraft.api.filesystem.IWritableMount;
import dan200.computercraft.api.peripheral.IComputerAccess;
import dan200.computercraft.api.peripheral.IPeripheral;
import dan200.computercraft.api.peripheral.IWorkMonitor;
import dan200.computercraft.api.peripheral.NotAttachedException;

public record DelegateComputerAccess(IComputerAccess computer, BooleanSupplier attached) implements IComputerAccess
{
	
	public String mount(String desiredLocation, IMount mount) {
		if( !attached.getAsBoolean() ) throw new NotAttachedException();
		return computer.mount(desiredLocation, mount);
	}

	public String mount(String desiredLocation, IMount mount, String driveName) {
		if( !attached.getAsBoolean() ) throw new NotAttachedException();
		return computer.mount(desiredLocation, mount, driveName);
	}

	public String mountWritable(String desiredLocation, IWritableMount mount) {
		if( !attached.getAsBoolean() ) throw new NotAttachedException();
		return computer.mountWritable(desiredLocation, mount);
	}

	public String mountWritable(String desiredLocation, IWritableMount mount, String driveName) {
		if( !attached.getAsBoolean() ) throw new NotAttachedException();
		return computer.mountWritable(desiredLocation, mount, driveName);
	}

	public void unmount(String location) {
		if( !attached.getAsBoolean() ) throw new NotAttachedException();
		computer.unmount(location);
	}

	public int getID() {
		if( !attached.getAsBoolean() ) throw new NotAttachedException();
		return computer.getID();
	}

	public void queueEvent(String event, Object... arguments) {
		if( !attached.getAsBoolean() ) throw new NotAttachedException();
		computer.queueEvent(event, arguments);
	}

	public String getAttachmentName() {
		if( !attached.getAsBoolean() ) throw new NotAttachedException();
		return computer.getAttachmentName();
	}

	public Map<String, IPeripheral> getAvailablePeripherals() {
		if( !attached.getAsBoolean() ) throw new NotAttachedException();
		return computer.getAvailablePeripherals();
	}

	public IPeripheral getAvailablePeripheral(String name) {
		if( !attached.getAsBoolean() ) throw new NotAttachedException();
		return computer.getAvailablePeripheral(name);
	}

	public IWorkMonitor getMainThreadMonitor() {
		if( !attached.getAsBoolean() ) throw new NotAttachedException();
		return computer.getMainThreadMonitor();
	}
}
