package futurepack.extensions.computercraft;

import net.minecraft.world.item.ItemStack;

public interface IFilterAccess 
{
	String[] getFilterGroups();
	
	int getSlots(String group);
	
	ItemStack getItem(String group, int slot);
	
	void setItem(String group, int slot, ItemStack stack);
}
