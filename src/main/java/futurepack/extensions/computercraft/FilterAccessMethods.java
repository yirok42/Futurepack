package futurepack.extensions.computercraft;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Nonnull;

import dan200.computercraft.api.lua.LuaFunction;
import dan200.computercraft.api.peripheral.GenericPeripheral;
import dan200.computercraft.api.peripheral.PeripheralType;
import dan200.computercraft.shared.peripheral.generic.data.ItemData;
import futurepack.api.Constants;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.registries.ForgeRegistries;

public class FilterAccessMethods implements GenericPeripheral
{
	@Nonnull
	@Override
	public PeripheralType getType() 
	{
		return PeripheralType.ofAdditional("filter_access");
	}

	@Nonnull
	@Override
	public ResourceLocation id() 
	{
		return new ResourceLocation(Constants.MOD_ID, "filter_access");
	}
	
	@LuaFunction( mainThread = true )
	public static String[] getFilterGroups(IFilterAccess access)
	{
		return access.getFilterGroups();
	}
	
	@LuaFunction( mainThread = true )
	public static int getSlots(IFilterAccess access, String group)
	{
		return access.getSlots(group);
	}
	
	@LuaFunction( mainThread = true )
	public static Map<String, ?> getItemDetail(IFilterAccess access, String group, int slot)
	{
		return ItemData.fill( new HashMap<>(), access.getItem(group, slot) );
	}
	
	@LuaFunction( mainThread = true )
	public static void setItem(IFilterAccess access, String group, int slot, String itemName)
	{
		access.setItem(group, slot, new ItemStack(ForgeRegistries.ITEMS.getValue(new ResourceLocation(itemName))));
	}
}
