package futurepack.extensions.crafttweaker.impl;

import java.util.Arrays;

import org.openzen.zencode.java.ZenCodeType;

import com.blamejared.crafttweaker.api.annotation.ZenRegister;
import com.blamejared.crafttweaker.api.ingredient.IIngredient;
import com.blamejared.crafttweaker.api.item.IItemStack;
import com.blamejared.crafttweaker.api.item.MCItemStack;
import com.blamejared.crafttweaker_annotations.annotations.Document;

import futurepack.common.research.Research;
import futurepack.common.research.ResearchManager;
import futurepack.depend.api.EnumAspects;
import futurepack.extensions.crafttweaker.CrafttweakerExtension;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.ItemStack;

@ZenRegister
@ZenCodeType.Name("mods.futurepack.ResearchWrapper")
@Document(value="futurepack/extensions/crafttweaker/impl/ResearchWrapper")
public class ResearchWrapper
{
	final Research r;

	public ResearchWrapper(Research r)
	{
		this.r = r;
	}

	@ZenCodeType.Method
	public void setParents(String[] p)
	{
		Research[] parents = Arrays.stream(p).map(ResearchManager::getResearch).toArray(Research[]::new);
		r.setParents(parents);
	}
	@ZenCodeType.Method
	public void setGrandparents(String[] p)
	{
		ResourceLocation[] gp = Arrays.stream(p).map(ResourceLocation::new).toArray(ResourceLocation[]::new);
		r.setGrandparents(gp);
	}
	@ZenCodeType.Method
	public void setEnabled(IItemStack[] it)
	{
		r.setEnabled(CrafttweakerExtension.convert(it));
	}
	@ZenCodeType.Method
	public void setNeeded(IIngredient[] it)
	{
		r.setNeeded(CrafttweakerExtension.convert(it));
	}
	
	@ZenCodeType.Method
	public String[] getParents()
	{
		return Arrays.stream(r.getParents()).map(Research::toString).toArray(String[]::new);
	}
	@ZenCodeType.Method
	public String[] getGrandparents()
	{
		return Arrays.stream(r.getGrandparents()).map(ResourceLocation::toString).toArray(String[]::new);
	}
	@ZenCodeType.Method
	public String getTranslationKey()
	{
		return r.getTranslationKey();
	}
	@ZenCodeType.Method
	public String getName()
	{
		return r.getName();
	}
	@ZenCodeType.Method
	public int getX()
	{
		return r.getX();
	}
	@ZenCodeType.Method
	public int getY()
	{
		return r.getY();
	}

	@ZenCodeType.Method
	public IItemStack[] getEnables()
	{
		return Arrays.stream(r.getEnables()).map(MCItemStack::new).toArray(IItemStack[]::new);
	}
//	@ZenCodeType.Method
//	public ItemPredicateBase[] getNeeded()
//	{
//		return r.getNeeded();
//	}

	@ZenCodeType.Method
	public int getExpLvl()
	{
		return r.getExpLvl();
	}
	@ZenCodeType.Method
	public int getSupport()
	{
		return r.getSupport();
	}
	@ZenCodeType.Method
	public int getNeonenergie()
	{
		return r.getNeonenergie();
	}
	@ZenCodeType.Method
	public int getTime()
	{
		return r.getTime();
	}
	@ZenCodeType.Method
	public int getTecLevel()
	{
		return r.getTecLevel();
	}
	@ZenCodeType.Method
	public String getBackground()
	{
		return r.getBackground().toString();
	}
	@ZenCodeType.Method
	public boolean isVisible(boolean hasRes, boolean canRes)
	{
		return r.isVisible(hasRes, canRes);
	}
	@ZenCodeType.Method
	public String getDomain()
	{
		return r.getDomain();
	}

	@ZenCodeType.Method
	public String toString()
	{
		return r.toString();
	}
	@ZenCodeType.Method
	public String toFullString()
	{
		return r.toFullString();
	}
	@ZenCodeType.Method
	public void setRewards(IItemStack[] it)
	{
		r.setRewards(CrafttweakerExtension.convert(it));
	}
	@ZenCodeType.Method
	public ItemStack[] getRewards()
	{
		return r.getRewards();
	}

	
	@ZenCodeType.Method
	public void setExpLvl(int xp)
	{
		r.setExpLvl(xp);
	}
	@ZenCodeType.Method
	public void setSupport(int sp)
	{
		r.setSupport(sp);
	}
	@ZenCodeType.Method
	public void setNeonenergie(int ne)
	{
		r.setNeonenergie(ne);
	}
	@ZenCodeType.Method
	public void setTime(int ticks)
	{
		r.setTime(ticks);
	}
	@ZenCodeType.Method
	public void setTecLevel(int lvl)
	{
		r.setTecLevel(lvl);
	}
	
	@ZenCodeType.Method
	public String[] getAspects()
	{
		return Arrays.stream(r.getAspects()).map(EnumAspects::name).map(String::toLowerCase).toArray(String[]::new);
	}
	@ZenCodeType.Method
	public void setAspects(String[] aspects)
	{
		r.setAspects(Arrays.stream(aspects).map(String::toUpperCase).map(EnumAspects::valueOf).toArray(EnumAspects[]::new));
	}
}
