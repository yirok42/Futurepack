package futurepack.extensions.crafttweaker.impl;

import org.openzen.zencode.java.ZenCodeType;

import com.blamejared.crafttweaker.api.CraftTweakerAPI;
import com.blamejared.crafttweaker.api.annotation.ZenRegister;
import com.blamejared.crafttweaker.api.ingredient.IIngredient;
import com.blamejared.crafttweaker.api.item.IItemStack;

import futurepack.common.recipes.crushing.CrushingRecipe;
import futurepack.common.recipes.crushing.FPCrushingManager;
import futurepack.extensions.crafttweaker.CrafttweakerExtension;
import futurepack.extensions.crafttweaker.RecipeActionBase;

@ZenRegister
@ZenCodeType.Name("mods.futurepack.crushing")
public class CTCrushing
{

	static class CrusherRecipeAddAction extends RecipeActionBase<CrushingRecipe>
	{
		private CrushingRecipe r;

		public CrusherRecipeAddAction(IItemStack output, IIngredient input)
		{
			super(Type.ADD, () -> FPCrushingManager.instance);

			r = new CrushingRecipe(CrafttweakerExtension.convert(input), output.getInternal());
		}

		@Override
		public CrushingRecipe createRecipe()
		{
			return r;
		}

	}

	static class CrusherRecipeRemoveAction extends RecipeActionBase<CrushingRecipe>
	{
		private IItemStack input;

		public CrusherRecipeRemoveAction(IItemStack input)
		{
			super(Type.REMOVE, () -> FPCrushingManager.instance);

			this.input = input;
		}

		@Override
		public CrushingRecipe createRecipe()
		{
			return ((FPCrushingManager)recipeManager.get()).getRecipe(input.getInternal());
		}

	}

	@ZenCodeType.Method
	public static void add(IItemStack output, IIngredient input)
	{
		CraftTweakerAPI.apply(new CrusherRecipeAddAction(output, input));
	}

	@ZenCodeType.Method
	public static void remove(IItemStack input)
	{
		CraftTweakerAPI.apply(new CrusherRecipeRemoveAction(input));
	}
}
