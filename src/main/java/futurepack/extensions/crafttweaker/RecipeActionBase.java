package futurepack.extensions.crafttweaker;

import java.util.function.Supplier;

import com.blamejared.crafttweaker.api.action.base.IUndoableAction;

import futurepack.common.recipes.ISyncedRecipeManager;

public abstract class RecipeActionBase<T> implements IUndoableAction
{
	protected final Supplier<ISyncedRecipeManager<T>> recipeManager;
	protected final Type type;
	protected T createdRecipe = null;
	
	public RecipeActionBase(Type type, Supplier<ISyncedRecipeManager<T>> recipeManager)
	{
		this.recipeManager = recipeManager;
		this.type = type;
	}
	
	public abstract T createRecipe();

	public T getRecipe()
	{
		if(createdRecipe==null)
			createdRecipe = createRecipe();
		
		return createdRecipe;
	}
	
	@Override
	public final void apply()
	{
		if(type == Type.ADD) 
			add();
		else
			remove();
	}
	
	@Override
	public final void undo()
	{
		if(type == Type.REMOVE) 
			add();
		else
			remove();
	}
	
	@Override
	public final String describe()
	{
		return (type == Type.ADD) ? describeAdd() : describeRemove();
	}
	
	@Override
	public final String describeUndo()
	{
		return (type == Type.ADD) ? describeRemove() : describeAdd();
	}

	public void add()
	{
		recipeManager.get().addRecipe(getRecipe());
	}
	
	public void remove()
	{
		recipeManager.get().getRecipes().remove(getRecipe());
	}
	
	public String describeAdd()
	{
		return "Add recipe " + getRecipe() + " to " + recipeManager.get().getName();
	}
	
	public String describeRemove()
	{
		return "Removes recipe " + getRecipe() + " to " + recipeManager.get().getName();
	}
	
	public static enum Type
	{
		ADD, REMOVE;
	}
}
