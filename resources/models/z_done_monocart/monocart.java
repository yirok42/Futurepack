// Made with Blockbench 4.0.0-beta.3
// Exported for Minecraft version 1.17 with Mojang mappings
// Paste this class into your mod and generate all required imports


public class monocart<T extends Entity> extends EntityModel<T> {
	// This layer location should be baked with EntityRendererProvider.Context in the entity renderer and passed into this model's constructor
	public static final ModelLayerLocation LAYER_LOCATION = new ModelLayerLocation(new ResourceLocation("modid", "monocart"), "main");
	private final ModelPart root;

	public monocart(ModelPart root) {
		this.root = root.getChild("root");
	}

	public static LayerDefinition createBodyLayer() {
		MeshDefinition meshdefinition = new MeshDefinition();
		PartDefinition partdefinition = meshdefinition.getRoot();

		PartDefinition root = partdefinition.addOrReplaceChild("root", CubeListBuilder.create(), PartPose.offset(0.0F, 19.0F, 0.0F));

		PartDefinition Streifenleiste1 = root.addOrReplaceChild("Streifenleiste1", CubeListBuilder.create().texOffs(0, 30).addBox(0.0F, 0.0F, -2.0F, 1.0F, 1.0F, 4.0F, new CubeDeformation(0.0F)), PartPose.offset(7.0F, 2.0F, 0.0F));

		PartDefinition Streifenleiste2 = root.addOrReplaceChild("Streifenleiste2", CubeListBuilder.create().texOffs(0, 30).addBox(-1.0F, 0.0F, -2.0F, 1.0F, 1.0F, 4.0F, new CubeDeformation(0.0F)), PartPose.offset(-7.0F, 2.0F, 0.0F));

		PartDefinition Boden2 = root.addOrReplaceChild("Boden2", CubeListBuilder.create().texOffs(0, 22).addBox(-8.0F, 0.0F, -6.0F, 16.0F, 1.0F, 7.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 3.0F, -2.0F));

		PartDefinition Fahrleiste = root.addOrReplaceChild("Fahrleiste", CubeListBuilder.create().texOffs(10, 30).addBox(-7.0F, 0.0F, -1.0F, 14.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 3.0F, 0.0F));

		PartDefinition Boden1 = root.addOrReplaceChild("Boden1", CubeListBuilder.create().texOffs(0, 22).addBox(-8.0F, 0.0F, -6.0F, 16.0F, 1.0F, 7.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 3.0F, 2.0F, 0.0F, -3.1416F, 0.0F));

		PartDefinition Cargo = root.addOrReplaceChild("Cargo", CubeListBuilder.create().texOffs(0, 0).addBox(-7.0F, 0.0F, -7.0F, 14.0F, 8.0F, 14.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, -5.0F, 0.0F));

		PartDefinition Halter1 = root.addOrReplaceChild("Halter1", CubeListBuilder.create().texOffs(46, 22).addBox(-1.0F, -7.0F, -2.0F, 1.0F, 7.0F, 4.0F, new CubeDeformation(0.0F)), PartPose.offset(8.0F, 3.0F, 4.0F));

		PartDefinition Halter2 = root.addOrReplaceChild("Halter2", CubeListBuilder.create().texOffs(46, 22).addBox(-1.0F, -7.0F, -2.0F, 1.0F, 7.0F, 4.0F, new CubeDeformation(0.0F)), PartPose.offset(8.0F, 3.0F, -4.0F));

		PartDefinition Halter3 = root.addOrReplaceChild("Halter3", CubeListBuilder.create().texOffs(46, 22).addBox(-1.0F, -7.0F, -2.0F, 1.0F, 7.0F, 4.0F, new CubeDeformation(0.0F)), PartPose.offset(-7.0F, 3.0F, -4.0F));

		PartDefinition Halter4 = root.addOrReplaceChild("Halter4", CubeListBuilder.create().texOffs(46, 22).addBox(-1.0F, -7.0F, -2.0F, 1.0F, 7.0F, 4.0F, new CubeDeformation(0.0F)), PartPose.offset(-7.0F, 3.0F, 4.0F));

		return LayerDefinition.create(meshdefinition, 64, 64);
	}

	@Override
	public void setupAnim(T entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch) {

	}

	@Override
	public void renderToBuffer(PoseStack poseStack, VertexConsumer buffer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha) {
		root.render(poseStack, buffer, packedLight, packedOverlay);
	}
}