// Made with Blockbench 4.0.0-beta.3
// Exported for Minecraft version 1.17 with Mojang mappings
// Paste this class into your mod and generate all required imports


public class pump<T extends Entity> extends EntityModel<T> {
	// This layer location should be baked with EntityRendererProvider.Context in the entity renderer and passed into this model's constructor
	public static final ModelLayerLocation LAYER_LOCATION = new ModelLayerLocation(new ResourceLocation("modid", "pump"), "main");
	private final ModelPart root;

	public pump(ModelPart root) {
		this.root = root.getChild("root");
	}

	public static LayerDefinition createBodyLayer() {
		MeshDefinition meshdefinition = new MeshDefinition();
		PartDefinition partdefinition = meshdefinition.getRoot();

		PartDefinition root = partdefinition.addOrReplaceChild("root", CubeListBuilder.create(), PartPose.offset(0.0F, 16.0F, 0.0F));

		PartDefinition Anschluss2 = root.addOrReplaceChild("Anschluss2", CubeListBuilder.create().texOffs(44, 10).addBox(6.0F, -2.0F, -2.0F, 1.0F, 4.0F, 4.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition Anschluss1 = root.addOrReplaceChild("Anschluss1", CubeListBuilder.create().texOffs(44, 0).addBox(-7.0F, -2.0F, -2.0F, 1.0F, 4.0F, 4.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition Ausen4 = root.addOrReplaceChild("Ausen4", CubeListBuilder.create().texOffs(0, 24).addBox(-6.0F, -4.0F, -3.0F, 1.0F, 8.0F, 10.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 1.5708F));

		PartDefinition Ausen3 = root.addOrReplaceChild("Ausen3", CubeListBuilder.create().texOffs(0, 24).addBox(-6.0F, -4.0F, -3.0F, 1.0F, 8.0F, 10.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 3.1416F));

		PartDefinition Ausen2 = root.addOrReplaceChild("Ausen2", CubeListBuilder.create().texOffs(0, 24).addBox(-6.0F, -4.0F, -3.0F, 1.0F, 8.0F, 10.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 0.0F, -1.5708F));

		PartDefinition Ausen1 = root.addOrReplaceChild("Ausen1", CubeListBuilder.create().texOffs(0, 24).addBox(-6.0F, -4.0F, -3.0F, 1.0F, 8.0F, 10.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition MotorMitte = root.addOrReplaceChild("MotorMitte", CubeListBuilder.create().texOffs(0, 0).addBox(-5.0F, -5.0F, -3.0F, 10.0F, 10.0F, 11.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition Steuergeeinhait = root.addOrReplaceChild("Steuergeeinhait", CubeListBuilder.create().texOffs(0, 44).addBox(-3.0F, -8.0F, -8.0F, 9.0F, 16.0F, 5.0F, new CubeDeformation(0.0F)), PartPose.offset(1.0F, 0.0F, 0.0F));

		PartDefinition MessStab = root.addOrReplaceChild("MessStab", CubeListBuilder.create().texOffs(24, 25).addBox(-6.01F, -6.0F, -7.01F, 4.0F, 12.0F, 4.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition Fixierleiste = root.addOrReplaceChild("Fixierleiste", CubeListBuilder.create().texOffs(56, 32).addBox(-4.0F, 6.0F, -1.0F, 8.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition Stellleiste1 = root.addOrReplaceChild("Stellleiste1", CubeListBuilder.create().texOffs(56, 0).addBox(4.0F, 6.0F, -3.0F, 2.0F, 2.0F, 11.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition Stelleiste2 = root.addOrReplaceChild("Stelleiste2", CubeListBuilder.create().texOffs(56, 15).addBox(-6.0F, 6.0F, -5.0F, 2.0F, 2.0F, 13.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		return LayerDefinition.create(meshdefinition, 128, 128);
	}

	@Override
	public void setupAnim(T entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch) {

	}

	@Override
	public void renderToBuffer(PoseStack poseStack, VertexConsumer buffer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha) {
		root.render(poseStack, buffer, packedLight, packedOverlay);
	}
}