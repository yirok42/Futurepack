// Made with Blockbench 4.0.0-beta.3
// Exported for Minecraft version 1.17 with Mojang mappings
// Paste this class into your mod and generate all required imports


public class gehuf<T extends Entity> extends EntityModel<T> {
	// This layer location should be baked with EntityRendererProvider.Context in the entity renderer and passed into this model's constructor
	public static final ModelLayerLocation LAYER_LOCATION = new ModelLayerLocation(new ResourceLocation("modid", "gehuf"), "main");
	private final ModelPart gehuf;

	public gehuf(ModelPart root) {
		this.gehuf = root.getChild("gehuf");
	}

	public static LayerDefinition createBodyLayer() {
		MeshDefinition meshdefinition = new MeshDefinition();
		PartDefinition partdefinition = meshdefinition.getRoot();

		PartDefinition gehuf = partdefinition.addOrReplaceChild("gehuf", CubeListBuilder.create().texOffs(0, 2).addBox(-3.0F, -15.5F, -3.0F, 6.0F, 6.0F, 6.0F, new CubeDeformation(0.0F))
		.texOffs(24, 3).addBox(-9.0F, -15.0F, -3.0F, 6.0F, 5.0F, 6.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 24.0F, -3.0F, 0.0F, 1.5708F, 0.0F));

		PartDefinition leg_f_r = gehuf.addOrReplaceChild("leg_f_r", CubeListBuilder.create().texOffs(0, 20).addBox(-1.0F, 0.0F, -0.99F, 2.0F, 10.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(1.0F, -10.0F, -2.0F));

		PartDefinition leg_f_l = gehuf.addOrReplaceChild("leg_f_l", CubeListBuilder.create().texOffs(8, 20).addBox(-1.0F, 0.0F, -1.01F, 2.0F, 10.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(1.0F, -10.0F, 2.0F));

		PartDefinition leg_b_l = gehuf.addOrReplaceChild("leg_b_l", CubeListBuilder.create().texOffs(8, 20).addBox(-1.0F, 0.0F, -1.0F, 2.0F, 10.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(-7.0F, -10.0F, 2.0F));

		PartDefinition leg_b_r = gehuf.addOrReplaceChild("leg_b_r", CubeListBuilder.create().texOffs(0, 20).addBox(-1.0F, 0.0F, -1.0F, 2.0F, 10.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(-7.0F, -10.0F, -2.0F));

		PartDefinition tail = gehuf.addOrReplaceChild("tail", CubeListBuilder.create().texOffs(16, 20).addBox(-1.0F, 0.0F, -0.5F, 1.0F, 6.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(-9.0F, -13.0F, 0.0F));

		PartDefinition neck = gehuf.addOrReplaceChild("neck", CubeListBuilder.create().texOffs(0, 14).addBox(0.0F, -1.5F, -1.5F, 6.0F, 3.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offset(2.0F, -12.5F, 0.0F));

		PartDefinition head = neck.addOrReplaceChild("head", CubeListBuilder.create().texOffs(26, 14).addBox(0.0F, -2.5F, -2.5F, 5.0F, 5.0F, 5.0F, new CubeDeformation(0.0F))
		.texOffs(18, 14).addBox(5.0F, -0.5F, -1.5F, 1.0F, 2.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offset(4.5F, 0.0F, 0.0F));

		return LayerDefinition.create(meshdefinition, 64, 32);
	}

	@Override
	public void setupAnim(T entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch) {

	}

	@Override
	public void renderToBuffer(PoseStack poseStack, VertexConsumer buffer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha) {
		gehuf.render(poseStack, buffer, packedLight, packedOverlay);
	}
}