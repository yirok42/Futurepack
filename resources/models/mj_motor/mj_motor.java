// Made with Blockbench 4.0.0-beta.3
// Exported for Minecraft version 1.17 with Mojang mappings
// Paste this class into your mod and generate all required imports


public class mj_motor<T extends Entity> extends EntityModel<T> {
	// This layer location should be baked with EntityRendererProvider.Context in the entity renderer and passed into this model's constructor
	public static final ModelLayerLocation LAYER_LOCATION = new ModelLayerLocation(new ResourceLocation("modid", "mj_motor"), "main");
	private final ModelPart root;

	public mj_motor(ModelPart root) {
		this.root = root.getChild("root");
	}

	public static LayerDefinition createBodyLayer() {
		MeshDefinition meshdefinition = new MeshDefinition();
		PartDefinition partdefinition = meshdefinition.getRoot();

		PartDefinition root = partdefinition.addOrReplaceChild("root", CubeListBuilder.create(), PartPose.offset(0.0F, 16.0F, 0.0F));

		PartDefinition Shape1 = root.addOrReplaceChild("Shape1", CubeListBuilder.create().texOffs(0, 32).addBox(-8.0F, -1.0F, -1.0F, 16.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition Shape2 = root.addOrReplaceChild("Shape2", CubeListBuilder.create().texOffs(0, 36).addBox(-5.0F, -7.0F, -7.0F, 10.0F, 14.0F, 14.0F, new CubeDeformation(0.0F)), PartPose.offset(-1.0F, 0.0F, 0.0F));

		PartDefinition Shape3 = root.addOrReplaceChild("Shape3", CubeListBuilder.create().texOffs(0, 0).addBox(-1.0F, -8.0F, -8.0F, 1.0F, 16.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offset(-6.0F, 0.0F, 0.0F));

		PartDefinition Shape3 = root.addOrReplaceChild("Shape3", CubeListBuilder.create().texOffs(0, 0).addBox(-1.0F, -8.0F, -8.0F, 1.0F, 16.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offset(5.0F, 0.0F, 0.0F));

		PartDefinition Shape4 = root.addOrReplaceChild("Shape4", CubeListBuilder.create().texOffs(20, 0).addBox(-5.0F, 0.0F, -3.0F, 10.0F, 1.0F, 6.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-1.0F, 0.0F, 8.0F, -1.5708F, 0.0F, 0.0F));

		PartDefinition Shape4 = root.addOrReplaceChild("Shape4", CubeListBuilder.create().texOffs(36, 34).addBox(-5.0F, 0.0F, 0.0F, 10.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(-1.0F, 0.0F, -8.0F));

		PartDefinition Shape5 = root.addOrReplaceChild("Shape5", CubeListBuilder.create().texOffs(36, 25).addBox(-4.0F, -3.0F, 0.0F, 8.0F, 6.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(-1.0F, -3.0F, -8.0F));

		PartDefinition Shape4 = root.addOrReplaceChild("Shape4", CubeListBuilder.create().texOffs(20, 0).addBox(-5.0F, 0.0F, -3.0F, 10.0F, 1.0F, 6.0F, new CubeDeformation(0.0F)), PartPose.offset(-1.0F, -8.0F, 0.0F));

		return LayerDefinition.create(meshdefinition, 64, 64);
	}

	@Override
	public void setupAnim(T entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch) {

	}

	@Override
	public void renderToBuffer(PoseStack poseStack, VertexConsumer buffer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha) {
		root.render(poseStack, buffer, packedLight, packedOverlay);
	}
}