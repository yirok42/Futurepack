// Made with Blockbench 4.0.0-beta.3
// Exported for Minecraft version 1.17 with Mojang mappings
// Paste this class into your mod and generate all required imports


public class neon_engine<T extends Entity> extends EntityModel<T> {
	// This layer location should be baked with EntityRendererProvider.Context in the entity renderer and passed into this model's constructor
	public static final ModelLayerLocation LAYER_LOCATION = new ModelLayerLocation(new ResourceLocation("modid", "neon_engine"), "main");
	private final ModelPart root;

	public neon_engine(ModelPart root) {
		this.root = root.getChild("root");
	}

	public static LayerDefinition createBodyLayer() {
		MeshDefinition meshdefinition = new MeshDefinition();
		PartDefinition partdefinition = meshdefinition.getRoot();

		PartDefinition root = partdefinition.addOrReplaceChild("root", CubeListBuilder.create(), PartPose.offset(0.0F, 16.0F, 0.0F));

		PartDefinition Energieleiter = root.addOrReplaceChild("Energieleiter", CubeListBuilder.create().texOffs(0, 40).addBox(-4.0F, -4.0F, -8.0F, 8.0F, 8.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition HalterH = root.addOrReplaceChild("HalterH", CubeListBuilder.create().texOffs(0, 21).addBox(-8.0F, -8.0F, 4.0F, 16.0F, 16.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition Spulle1 = root.addOrReplaceChild("Spulle1", CubeListBuilder.create().texOffs(0, 0).addBox(-2.0F, -7.0F, -0.1F, 4.0F, 4.0F, 8.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.7854F));

		PartDefinition Spulle2 = root.addOrReplaceChild("Spulle2", CubeListBuilder.create().texOffs(0, 0).addBox(-2.0F, -7.0F, -0.1F, 4.0F, 4.0F, 8.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 0.0F, -0.7854F));

		PartDefinition Spulle3 = root.addOrReplaceChild("Spulle3", CubeListBuilder.create().texOffs(0, 0).addBox(-2.0F, -7.0F, -0.1F, 4.0F, 4.0F, 8.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 0.0F, -2.3562F));

		PartDefinition Spulle4 = root.addOrReplaceChild("Spulle4", CubeListBuilder.create().texOffs(0, 0).addBox(-2.0F, -7.0F, -0.1F, 4.0F, 4.0F, 8.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 2.3562F));

		PartDefinition HalterV = root.addOrReplaceChild("HalterV", CubeListBuilder.create().texOffs(0, 21).addBox(-8.0F, -8.0F, -3.0F, 16.0F, 16.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition Eisenkern1 = root.addOrReplaceChild("Eisenkern1", CubeListBuilder.create().texOffs(17, 0).addBox(-1.0F, -10.0F, 1.0F, 2.0F, 4.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.7854F));

		PartDefinition Eisenkern2 = root.addOrReplaceChild("Eisenkern2", CubeListBuilder.create().texOffs(17, 0).addBox(-1.0F, -10.0F, 1.0F, 2.0F, 4.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 2.3562F));

		PartDefinition Eisenkern3 = root.addOrReplaceChild("Eisenkern3", CubeListBuilder.create().texOffs(17, 0).addBox(-1.0F, -10.0F, 1.0F, 2.0F, 4.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 0.0F, -2.3562F));

		PartDefinition Eisenkern4 = root.addOrReplaceChild("Eisenkern4", CubeListBuilder.create().texOffs(17, 0).addBox(-1.0F, -10.0F, 1.0F, 2.0F, 4.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 0.0F, -0.7854F));

		PartDefinition Fixirung = root.addOrReplaceChild("Fixirung", CubeListBuilder.create().texOffs(26, 0).addBox(-5.0F, -5.0F, -4.0F, 10.0F, 10.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		return LayerDefinition.create(meshdefinition, 64, 64);
	}

	@Override
	public void setupAnim(T entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch) {

	}

	@Override
	public void renderToBuffer(PoseStack poseStack, VertexConsumer buffer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha) {
		root.render(poseStack, buffer, packedLight, packedOverlay);
	}
}