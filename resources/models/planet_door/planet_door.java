// Made with Blockbench 4.0.0-beta.3
// Exported for Minecraft version 1.17 with Mojang mappings
// Paste this class into your mod and generate all required imports


public class planet_door<T extends Entity> extends EntityModel<T> {
	// This layer location should be baked with EntityRendererProvider.Context in the entity renderer and passed into this model's constructor
	public static final ModelLayerLocation LAYER_LOCATION = new ModelLayerLocation(new ResourceLocation("modid", "planet_door"), "main");
	private final ModelPart root;

	public planet_door(ModelPart root) {
		this.root = root.getChild("root");
	}

	public static LayerDefinition createBodyLayer() {
		MeshDefinition meshdefinition = new MeshDefinition();
		PartDefinition partdefinition = meshdefinition.getRoot();

		PartDefinition root = partdefinition.addOrReplaceChild("root", CubeListBuilder.create(), PartPose.offset(0.0F, 24.0F, 0.0F));

		PartDefinition Standfuss1 = root.addOrReplaceChild("Standfuss1", CubeListBuilder.create().texOffs(0, 0).addBox(-8.0F, -8.0F, -8.0F, 16.0F, 9.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, -1.0F, 0.0F));

		PartDefinition SockelFuss1 = root.addOrReplaceChild("SockelFuss1", CubeListBuilder.create().texOffs(0, 32).addBox(-3.0F, -3.0F, 0.0F, 6.0F, 6.0F, 8.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, -5.0F, 8.0F));

		PartDefinition SockelFuss2 = root.addOrReplaceChild("SockelFuss2", CubeListBuilder.create().texOffs(0, 32).addBox(-3.0F, -3.0F, 0.0F, 6.0F, 6.0F, 8.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, -5.0F, -8.0F, 0.0F, -3.1416F, 0.0F));

		PartDefinition TorSockel4 = root.addOrReplaceChild("TorSockel4", CubeListBuilder.create().texOffs(32, 44).addBox(-2.0F, -2.0F, 0.0F, 4.0F, 4.0F, 8.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, -6.0F, 12.0F, 0.7854F, 0.0F, 0.0F));

		PartDefinition TorSockel3 = root.addOrReplaceChild("TorSockel3", CubeListBuilder.create().texOffs(32, 44).addBox(-2.0F, -2.0F, 0.0F, 4.0F, 4.0F, 8.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, -6.0F, -12.0F, 0.7854F, -3.1416F, 0.0F));

		PartDefinition TorSockel2 = root.addOrReplaceChild("TorSockel2", CubeListBuilder.create().texOffs(32, 30).addBox(-2.0F, -2.0F, 0.0F, 4.0F, 4.0F, 8.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, -11.0F, -17.2F, 1.117F, -3.1416F, 0.0F));

		PartDefinition TorSockel1 = root.addOrReplaceChild("TorSockel1", CubeListBuilder.create().texOffs(32, 30).addBox(-2.0F, -2.0F, 0.0F, 4.0F, 4.0F, 8.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, -11.0F, 17.2F, 1.117F, 0.0F, 0.0F));

		PartDefinition TorTeil2 = root.addOrReplaceChild("TorTeil2", CubeListBuilder.create().texOffs(60, 30).addBox(-2.0F, -27.0F, -2.0F, 4.0F, 27.0F, 4.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, -17.4F, -20.4F, 0.0F, -3.1416F, 0.0F));

		PartDefinition TorTeil1 = root.addOrReplaceChild("TorTeil1", CubeListBuilder.create().texOffs(60, 30).addBox(-2.0F, -27.0F, -2.0F, 4.0F, 27.0F, 4.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, -17.4F, 20.4F, 0.0F, 0.0F, 0.0F));

		PartDefinition EnergieStrahl2 = root.addOrReplaceChild("EnergieStrahl2", CubeListBuilder.create().texOffs(68, 0).addBox(-1.0F, -1.0F, -2.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, -20.0F, 18.4F));

		PartDefinition EnergieStrahl3 = root.addOrReplaceChild("EnergieStrahl3", CubeListBuilder.create().texOffs(68, 0).addBox(-1.0F, -1.0F, -2.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, -31.0F, 18.4F));

		PartDefinition EnergieStrahl4 = root.addOrReplaceChild("EnergieStrahl4", CubeListBuilder.create().texOffs(68, 0).addBox(-1.0F, -1.0F, -2.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, -20.0F, -18.4F, 3.1416F, 0.0F, 0.0F));

		PartDefinition EnergieStrahl5 = root.addOrReplaceChild("EnergieStrahl5", CubeListBuilder.create().texOffs(68, 0).addBox(-1.0F, -1.0F, -2.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, -31.0F, -18.4F, 3.1416F, 0.0F, 0.0F));

		PartDefinition EnergieStrahl6 = root.addOrReplaceChild("EnergieStrahl6", CubeListBuilder.create().texOffs(68, 0).addBox(-1.0F, -1.0F, -2.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, -42.0F, -18.4F, 3.1416F, 0.0F, 0.0F));

		PartDefinition EnergieStrahl1 = root.addOrReplaceChild("EnergieStrahl1", CubeListBuilder.create().texOffs(68, 0).addBox(-1.0F, -1.0F, -2.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, -42.0F, 18.4F));

		PartDefinition EreignisHorizont = root.addOrReplaceChild("EreignisHorizont", CubeListBuilder.create().texOffs(80, 0).addBox(0.0F, -17.0F, -17.0F, 0.0F, 34.0F, 34.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, -31.0F, 0.0F));

		return LayerDefinition.create(meshdefinition, 256, 256);
	}

	@Override
	public void setupAnim(T entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch) {

	}

	@Override
	public void renderToBuffer(PoseStack poseStack, VertexConsumer buffer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha) {
		root.render(poseStack, buffer, packedLight, packedOverlay);
	}
}