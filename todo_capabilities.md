Inventory Handler:


| File | Status | Tested | Logistik Editor |
| ---- | ------ | ------ | ------ |
| TileEntityCrusher.java				| dirty impl			| OK | OK |
| TileEntityEFurnace.java				| dirty impl			| OK | OK (loose item-conf on upgrade) |
| TileEntityIndustrialNeonFurnace.java	| dirty impl			| OK | OK (loose item-conf on upgrade) |
| TileEntityIonCollector.java			| dirty impl			| ~ OK | loose all configuration |
| TileEntityMachineBase.java			| ... 					| ... | ... |
| TileEntityOptiBenchBase.java			| dirty impl			| OK | OK |
| TileEntityPlasmaGenerator.java		| dirty impl			| OK | OK |
| TileEntitySorter.java					| dirty impl			| fixed? | ... |
| TileEntityZentrifuge.java				| dirty impl			| fixed?  | OK |
| TileEntityMonorailStation.java		| full impleemneted		| fixed? | ... |
| TileEntityAssemblyTable.java			| dirty impl			| fixed? | loose all configuration; remove item options |
| TileEntityBaterieBox.java				| dirty impl			| ~ | loose all configuration ! |
| TileEntityBlockBreaker.java			| full implemented		| OK | ... |
| TileEntityBoardComputer.java			| dont need it			| ... | ... |
| TileEntityBrennstoffGenerator.java	| full implemented		| fixed? (take Items) | loose all configuration & not needed |
| TileEntityDroneStation.java			| full implemented		| fixed? - everything |
| TileEntityFlashServer.java			| dirty impl			| ~ | loose all configuration ! |
| TileEntityForscher.java				| dont need it			| ... | ... |
| TileEntityFuelCell.java				| dirty impl			| OK | ... |
| TileEntityIndustryalFurnace.java		| inv=dirty, fluid full | fixed? - no insert by pipe; items puttable to obsidian slot | ... |
| TileEntityInventoryBase.java			| ...					| ... | ... |
| TileEntityModulT1.java				| full impl				| fixed? (take Items) | ... |
| TileEntityModulT2.java				| full impl				| fixed? (take Items) | ... |
| TileEntityPartPress.java				| ...					| ... | ... |
| TileEntityPipe.java					| dirt impl				| OK | ... |
| TileEntityWaterCooler.java			| full impl				| fixed? (take Items) | loose all configuration |
| TileEntityPusher						| full implemented		| fixed? | ... |
| TileEntityTechTable					| dont need it			| ... | ... |
| TileEntityBlockPlacer					| dirty/simple impl		| ... | ... |


... = old IInventory

~ = Insert/Extract Logik sollte überarbeitet werden
